# メホールシステム説明

## メホールとは
 - 画像を見ながら入力できるプログラム
 - 入力したデータを検索/出力できる
## メホールシステム概要
1. 全体図
	- 以前![](./img/mejor/old.png)
	- 現在![](./img/mejor/curr.jpg)
1. 付随プログラムに関して
	- BackgroundOCR![](./img/ocr/easy.jpg)
## 業務におけるメホールの使用
1. 基本的な業務フロー（柔整申請書電子化業務）
	- 保険者からの流れ![](./img/flow/all.jpg)
	- メホール入力だけの場合のフロー![](./img/flow/inputonly.jpg)
	- 保険者からの提供データが存在する場合のフロー![](./img/flow/inputanddata.jpg)
1. 業務におけるファイルの流れ
	- ファイルフロー![](./img/fileflow.jpg)
## 代表的な作業
 - 画像登録
 -　提供データインポート
 - 入力画面
　	- 入力1回目表示時![](./img/inputflow/inputflow11.jpg)
　	- 入力1回目登録時![](./img/inputflow/inputflow12.jpg)
　	- 入力2回目表示時![](./img/inputflow/inputflow21.jpg)
　	- 入力2回目登録時![](./img/inputflow/inputflow22.jpg) 
　- リスト作成画面![](./img/listcreate_flow.jpg)
 - データ出力
## 管理体系
 - スキャンID/スキャングループ/AIDとは![](./img/manage/manage1.jpg)
 - 管理イメージ![](./img/manage/manage2.jpg)
---
# メホールの構成
1. 基本DB
   - 保険者用DB 構成（redmineのやつ)
   - メホール管理用DB（入力ログ、リスト作成用設定、傷病名マスタ、ユーザ管理等） 構成　資料つくる→完了
   - 共通データ用DB（往療距離算出用） 構成　スクショでいい→一応できたが、ホントにスクショのみ

1. 管理者用画面説明 全部スクショと何をしているか →スクショとった
   - バージョン管理
   - メホール全体設定
   - 保険者設定
   - メディベリー設定
   - 各マスタ更新
   - ログ集計
### メホールにおける申請書データの扱い
1. テーブル仕様
   - Application　各項目がある設計書、AIDについて
   - Refrece　保険者からの提供データ
   - scan　1スキャン（100申請書）ごとのID
   - scangroup　１スキャングループ（８００申請書）ごとのid
### 管理体系
 - スキャンID/スキャングループ/AIDとは![](./img/manage/manage1.jpg)
 - 管理イメージ![](./img/manage/manage2.jpg)	
### メホールのソース構造
1. ソリューション　→スクショ
1. 一保険者の基本構成
1. 入力画面の構成