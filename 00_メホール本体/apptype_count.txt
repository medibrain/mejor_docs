  ?column?   | aapptype | count 
-------------+----------+-------
 aichi_toshi |        7 |  3271
 aichi_toshi |        6 | 81231
 aichi_toshi |        0 |  1138
 aichi_toshi |       -3 |   152
 aichi_toshi |       -4 |   533
(5 行)

     ?column?     | aapptype | count  
------------------+----------+--------
 amagasaki_kokuho |        6 | 235833
 amagasaki_kokuho |       -3 |   1147
 amagasaki_kokuho |       -4 |   2991
(3 行)

 ?column?  | aapptype | count 
-----------+----------+-------
 arakawaku |        8 |    60
 arakawaku |        7 |   102
 arakawaku |        6 |  7746
 arakawaku |        0 |  6139
 arakawaku |       -3 |   256
 arakawaku |       -4 |    72
 arakawaku |       -7 |  2321
(7 行)

    ?column?    | aapptype | count 
----------------+----------+-------
 asakura_kokuho |        6 |  7240
 asakura_kokuho |       -3 |   115
 asakura_kokuho |       -4 |    61
(3 行)

    ?column?     | aapptype | count 
-----------------+----------+-------
 beppushi_kokuho |        6 | 10110
 beppushi_kokuho |       -4 |     2
(2 行)

    ?column?     | aapptype | count 
-----------------+----------+-------
 chibashi_kokuho |        8 |  3050
 chibashi_kokuho |        7 |  2801
 chibashi_kokuho |        6 | 54165
 chibashi_kokuho |        0 |  1746
 chibashi_kokuho |       -3 |  3798
 chibashi_kokuho |       -4 |   289
 chibashi_kokuho |       -5 |  1763
 chibashi_kokuho |      -11 |   509
 chibashi_kokuho |      -13 |   103
 chibashi_kokuho |      -14 |  1314
(10 行)

  ?column?  | aapptype | count 
------------+----------+-------
 chuo_radio |        6 | 21993
 chuo_radio |        0 |   741
 chuo_radio |       -3 |    42
 chuo_radio |       -4 |    20
(4 行)

   ?column?   | aapptype | count 
--------------+----------+-------
 denki_garasu |        6 |  2817
 denki_garasu |        0 |    57
 denki_garasu |       -3 |    40
 denki_garasu |       -4 |     1
(4 行)

 ?column? | aapptype | count 
----------+----------+-------
 futaba   |        8 |    21
 futaba   |        7 |   214
 futaba   |        6 |  8607
 futaba   |        0 |   220
 futaba   |       -3 |   337
 futaba   |       -4 |    15
(6 行)

      ?column?      | aapptype | count 
--------------------+----------+-------
 gaimusho_ryouyouhi |        0 |  2598
 gaimusho_ryouyouhi |       -4 |     6
(2 行)

     ?column?     | aapptype | count  
------------------+----------+--------
 gakko_01hokkaido |        7 |   8831
 gakko_01hokkaido |        6 | 107174
 gakko_01hokkaido |        0 |   3609
 gakko_01hokkaido |       -3 |   5090
 gakko_01hokkaido |       -4 |   1507
 gakko_01hokkaido |       -7 |  10931
(6 行)

    ?column?    | aapptype | count 
----------------+----------+-------
 gakko_02aomori |        7 |    87
 gakko_02aomori |        6 | 24974
 gakko_02aomori |        0 |   870
 gakko_02aomori |       -3 |   152
 gakko_02aomori |       -7 |  1583
(5 行)

   ?column?    | aapptype | count 
---------------+----------+-------
 gakko_03iwate |        7 |     1
 gakko_03iwate |        6 | 32312
 gakko_03iwate |       -3 |   392
 gakko_03iwate |       -4 |     1
 gakko_03iwate |       -7 |  2283
(5 行)

    ?column?    | aapptype | count 
----------------+----------+-------
 gakko_04miyagi |        6 | 65495
 gakko_04miyagi |        0 |  3074
 gakko_04miyagi |       -3 |   718
 gakko_04miyagi |       -4 |  1455
 gakko_04miyagi |       -7 |  3254
(5 行)

   ?column?    | aapptype | count 
---------------+----------+-------
 gakko_05akita |        7 |   112
 gakko_05akita |        6 | 21262
 gakko_05akita |       -3 |    44
 gakko_05akita |       -7 |  1998
(4 行)

     ?column?     | aapptype | count 
------------------+----------+-------
 gakko_06yamagata |        7 |   290
 gakko_06yamagata |        6 | 20448
 gakko_06yamagata |       -3 |   172
 gakko_06yamagata |       -4 |    50
 gakko_06yamagata |       -7 |  1562
(5 行)

     ?column?      | aapptype | count 
-------------------+----------+-------
 gakko_07fukushima |        7 |   847
 gakko_07fukushima |        6 | 45100
 gakko_07fukushima |       -3 |   561
 gakko_07fukushima |       -4 |    12
 gakko_07fukushima |       -7 |  4680
(5 行)

    ?column?     | aapptype | count 
-----------------+----------+-------
 gakko_08ibaragi |        8 |    15
 gakko_08ibaragi |        7 |   925
 gakko_08ibaragi |        6 | 56955
 gakko_08ibaragi |        0 |   914
 gakko_08ibaragi |       -3 |  1033
 gakko_08ibaragi |       -4 |     1
 gakko_08ibaragi |       -7 |  3736
(7 行)

    ?column?     | aapptype | count 
-----------------+----------+-------
 gakko_09tochigi |        7 |   312
 gakko_09tochigi |        6 | 40074
 gakko_09tochigi |       -3 |   794
 gakko_09tochigi |       -4 |    99
 gakko_09tochigi |       -7 |  2439
(5 行)

   ?column?    | aapptype | count 
---------------+----------+-------
 gakko_10gunma |        7 |   383
 gakko_10gunma |        6 | 43032
 gakko_10gunma |        0 |    20
 gakko_10gunma |       -3 |   629
 gakko_10gunma |       -4 |     9
 gakko_10gunma |       -7 |  2463
(6 行)

    ?column?     | aapptype | count  
-----------------+----------+--------
 gakko_11saitama |        7 |   2033
 gakko_11saitama |        6 | 117583
 gakko_11saitama |       -3 |   2554
 gakko_11saitama |       -4 |     11
 gakko_11saitama |       -7 |   8444
(5 行)

   ?column?    | aapptype | count 
---------------+----------+-------
 gakko_12chiba |        7 |  2352
 gakko_12chiba |        6 | 95571
 gakko_12chiba |       -3 |  2212
 gakko_12chiba |       -4 |     3
 gakko_12chiba |       -7 |  5913
(5 行)

   ?column?    | aapptype | count  
---------------+----------+--------
 gakko_13tokyo |        7 |   5342
 gakko_13tokyo |        6 | 177156
 gakko_13tokyo |       -3 |   4286
 gakko_13tokyo |       -4 |     20
 gakko_13tokyo |       -7 |  13275
(5 行)

     ?column?     | aapptype | count  
------------------+----------+--------
 gakko_14kanagawa |        7 |   4398
 gakko_14kanagawa |        6 | 127318
 gakko_14kanagawa |        0 |   3429
 gakko_14kanagawa |       -3 |   4306
 gakko_14kanagawa |       -4 |     14
 gakko_14kanagawa |       -7 |   9396
(6 行)

    ?column?    | aapptype | count 
----------------+----------+-------
 gakko_15nigata |        7 |   672
 gakko_15nigata |        6 | 32172
 gakko_15nigata |       -3 |   672
 gakko_15nigata |       -4 |    33
 gakko_15nigata |       -7 |  3645
(5 行)

    ?column?    | aapptype | count 
----------------+----------+-------
 gakko_16toyama |        7 |  1148
 gakko_16toyama |        6 | 20007
 gakko_16toyama |       -3 |   908
 gakko_16toyama |       -4 |     2
 gakko_16toyama |       -7 |  1347
(5 行)

     ?column?     | aapptype | count 
------------------+----------+-------
 gakko_17ishikawa |        7 |  1078
 gakko_17ishikawa |        6 | 19490
 gakko_17ishikawa |       -3 |   264
 gakko_17ishikawa |       -7 |  1978
(4 行)

   ?column?    | aapptype | count 
---------------+----------+-------
 gakko_18fukui |        7 |  1092
 gakko_18fukui |        6 | 15206
 gakko_18fukui |        0 |   476
 gakko_18fukui |       -3 |   278
 gakko_18fukui |       -4 |   203
 gakko_18fukui |       -7 |  2002
(6 行)

     ?column?      | aapptype | count 
-------------------+----------+-------
 gakko_19yamanashi |        7 |   283
 gakko_19yamanashi |        6 | 18209
 gakko_19yamanashi |       -3 |   581
 gakko_19yamanashi |       -7 |  1933
(4 行)

    ?column?    | aapptype | count 
----------------+----------+-------
 gakko_20nagano |        6 | 44277
 gakko_20nagano |       -3 |   833
 gakko_20nagano |       -4 |     3
 gakko_20nagano |       -7 |  3065
(4 行)

   ?column?   | aapptype | count 
--------------+----------+-------
 gakko_21gifu |        7 |   995
 gakko_21gifu |        6 | 43703
 gakko_21gifu |       -3 |   854
 gakko_21gifu |       -4 |     5
 gakko_21gifu |       -7 |  6887
(5 行)

     ?column?     | aapptype | count 
------------------+----------+-------
 gakko_22shizuoka |        7 |   648
 gakko_22shizuoka |        6 | 57805
 gakko_22shizuoka |       -3 |  1070
 gakko_22shizuoka |       -4 |    14
 gakko_22shizuoka |       -7 |  6196
(5 行)

 ?column? | aapptype | count 
----------+----------+-------
(0 行)

  ?column?   | aapptype | count 
-------------+----------+-------
 gakko_24mie |        7 |  1381
 gakko_24mie |        6 | 31876
 gakko_24mie |       -3 |   798
 gakko_24mie |       -7 |  5044
(4 行)

   ?column?    | aapptype | count 
---------------+----------+-------
 gakko_25shiga |        7 |   532
 gakko_25shiga |        6 | 29690
 gakko_25shiga |        0 |  1051
 gakko_25shiga |       -3 |   499
 gakko_25shiga |       -4 |    28
 gakko_25shiga |       -7 |  4491
(6 行)

   ?column?    | aapptype | count 
---------------+----------+-------
 gakko_26kyoto |        7 |  1542
 gakko_26kyoto |        6 | 73238
 gakko_26kyoto |       -3 |  1882
 gakko_26kyoto |       -4 |     1
 gakko_26kyoto |       -7 | 10344
(5 行)

   ?column?    | aapptype | count  
---------------+----------+--------
 gakko_27osaka |        7 |  13697
 gakko_27osaka |        6 | 304009
 gakko_27osaka |        0 |   1665
 gakko_27osaka |       -1 |  16790
 gakko_27osaka |       -3 |  12287
 gakko_27osaka |       -4 |     24
 gakko_27osaka |       -7 |  26522
 gakko_27osaka |       -8 |  16791
(8 行)

   ?column?    | aapptype | count  
---------------+----------+--------
 gakko_28hyogo |        7 |    902
 gakko_28hyogo |        6 | 126887
 gakko_28hyogo |       -3 |   1643
 gakko_28hyogo |       -4 |      4
 gakko_28hyogo |       -7 |  16087
(5 行)

   ?column?   | aapptype | count 
--------------+----------+-------
 gakko_29nara |        7 |   766
 gakko_29nara |        6 | 37599
 gakko_29nara |       -3 |   849
 gakko_29nara |       -7 |  6058
(4 行)

     ?column?     | aapptype | count 
------------------+----------+-------
 gakko_30wakayama |        7 |   969
 gakko_30wakayama |        6 | 40486
 gakko_30wakayama |       -3 |   594
 gakko_30wakayama |       -4 |    11
 gakko_30wakayama |       -7 |  6647
(5 行)

    ?column?     | aapptype | count 
-----------------+----------+-------
 gakko_31tottori |        7 |   113
 gakko_31tottori |        6 |  6767
 gakko_31tottori |       -3 |   217
 gakko_31tottori |       -4 |     4
 gakko_31tottori |       -7 |  1365
(5 行)

    ?column?     | aapptype | count 
-----------------+----------+-------
 gakko_32shimane |        7 |   113
 gakko_32shimane |        6 |  8363
 gakko_32shimane |        0 |   235
 gakko_32shimane |       -3 |    29
 gakko_32shimane |       -7 |  1397
(5 行)

    ?column?     | aapptype | count 
-----------------+----------+-------
 gakko_33okayama |        7 |   945
 gakko_33okayama |        6 | 33933
 gakko_33okayama |       -3 |   158
 gakko_33okayama |       -4 |     2
 gakko_33okayama |       -7 |  2865
(5 行)

     ?column?      | aapptype | count 
-------------------+----------+-------
 gakko_34hiroshima |        7 |  2367
 gakko_34hiroshima |        6 | 37199
 gakko_34hiroshima |        0 |  1011
 gakko_34hiroshima |       -3 |   407
 gakko_34hiroshima |       -4 |     2
 gakko_34hiroshima |       -7 |  4190
(6 行)

      ?column?      | aapptype | count 
--------------------+----------+-------
 gakko_35yamamguchi |        7 |   800
 gakko_35yamamguchi |        6 | 26404
 gakko_35yamamguchi |       -3 |   540
 gakko_35yamamguchi |       -4 |     4
 gakko_35yamamguchi |       -7 |  2628
(5 行)

     ?column?      | aapptype | count 
-------------------+----------+-------
 gakko_36tokushima |        7 |   867
 gakko_36tokushima |        6 | 34471
 gakko_36tokushima |       -3 |   372
 gakko_36tokushima |       -7 |  2707
(4 行)

    ?column?    | aapptype | count 
----------------+----------+-------
 gakko_37kagawa |        7 |   414
 gakko_37kagawa |        6 | 25867
 gakko_37kagawa |       -3 |   615
 gakko_37kagawa |       -4 |     1
 gakko_37kagawa |       -7 |  3206
(5 行)

   ?column?    | aapptype | count 
---------------+----------+-------
 gakko_38ehime |        6 | 31477
 gakko_38ehime |       -3 |   135
 gakko_38ehime |       -7 |  3803
(3 行)

   ?column?    | aapptype | count 
---------------+----------+-------
 gakko_39kochi |        7 |    60
 gakko_39kochi |        6 | 18576
 gakko_39kochi |       -3 |    88
 gakko_39kochi |       -7 |  2211
(4 行)

    ?column?     | aapptype | count  
-----------------+----------+--------
 gakko_40fukuoka |        7 |   3863
 gakko_40fukuoka |        6 | 147568
 gakko_40fukuoka |        0 |   3948
 gakko_40fukuoka |       -3 |   3347
 gakko_40fukuoka |       -4 |      2
 gakko_40fukuoka |       -7 |   8125
(6 行)

   ?column?   | aapptype | count 
--------------+----------+-------
 gakko_41saga |        7 |   223
 gakko_41saga |        6 | 28394
 gakko_41saga |       -3 |   307
 gakko_41saga |       -7 |  2294
(4 行)

     ?column?     | aapptype | count 
------------------+----------+-------
 gakko_42nagasaki |        7 |  1795
 gakko_42nagasaki |        6 | 69810
 gakko_42nagasaki |        0 |  1349
 gakko_42nagasaki |       -3 |   953
 gakko_42nagasaki |       -4 |    12
 gakko_42nagasaki |       -7 |  4510
(6 行)

     ?column?     | aapptype | count 
------------------+----------+-------
 gakko_43kumamoto |        7 |   911
 gakko_43kumamoto |        6 | 37936
 gakko_43kumamoto |        0 |  1090
 gakko_43kumamoto |       -3 |   482
 gakko_43kumamoto |       -7 |  3421
(5 行)

   ?column?   | aapptype | count 
--------------+----------+-------
 gakko_44oita |        7 |   428
 gakko_44oita |        6 | 34923
 gakko_44oita |        0 |   869
 gakko_44oita |       -3 |   518
 gakko_44oita |       -4 |   313
 gakko_44oita |       -7 |  2440
(6 行)

     ?column?     | aapptype | count 
------------------+----------+-------
 gakko_45miyazaki |        7 |  1068
 gakko_45miyazaki |        6 | 29593
 gakko_45miyazaki |        0 |   798
 gakko_45miyazaki |       -3 |   586
 gakko_45miyazaki |       -7 |  2544
(5 行)

     ?column?      | aapptype | count 
-------------------+----------+-------
 gakko_46kagoshima |        7 |   621
 gakko_46kagoshima |        6 | 64217
 gakko_46kagoshima |        0 |  1669
 gakko_46kagoshima |       -3 |   945
 gakko_46kagoshima |       -4 |     3
 gakko_46kagoshima |       -7 |  5008
(6 行)

    ?column?     | aapptype | count 
-----------------+----------+-------
 gakko_47okinawa |        7 |   519
 gakko_47okinawa |        6 | 41328
 gakko_47okinawa |        0 |  1123
 gakko_47okinawa |       -3 |   703
 gakko_47okinawa |       -4 |     6
 gakko_47okinawa |       -7 |  3422
(6 行)

   ?column?   | aapptype | count 
--------------+----------+-------
 gakko_99test |        7 |   904
 gakko_99test |        6 |  3375
 gakko_99test |        0 |   678
 gakko_99test |       -3 |   316
 gakko_99test |       -4 |     6
 gakko_99test |       -7 |   670
(6 行)

        ?column?        | aapptype | count 
------------------------+----------+-------
 higashimurayama_kokuho |        6 | 58164
 higashimurayama_kokuho |        0 |   434
 higashimurayama_kokuho |       -3 |  1676
 higashimurayama_kokuho |       -4 |   385
(4 行)

 ?column? | aapptype | count 
----------+----------+-------
 kagome   |        6 |  5383
 kagome   |        0 |   121
 kagome   |       -3 |   389
 kagome   |       -4 |     7
(4 行)

 ?column? | aapptype | count 
----------+----------+-------
(0 行)

   ?column?   | aapptype | count 
--------------+----------+-------
 kk_hiroshima |        6 |  7997
(1 行)

  ?column?  | aapptype | count 
------------+----------+-------
 kk_saitama |        6 |     1
 kk_saitama |        0 |  4308
(2 行)

  ?column?   | aapptype | count  
-------------+----------+--------
 kyoto_koiki |        8 | 121620
 kyoto_koiki |        7 |  52633
 kyoto_koiki |        0 |    996
 kyoto_koiki |       -3 | 152397
 kyoto_koiki |       -4 |  24528
 kyoto_koiki |       -5 |  92038
 kyoto_koiki |       -6 |  16728
 kyoto_koiki |       -9 |   2321
(8 行)

   ?column?   | aapptype | count 
--------------+----------+-------
 miyagi_koiki |        8 | 28964
 miyagi_koiki |        7 |  6128
 miyagi_koiki |        6 | 92933
 miyagi_koiki |       -3 | 72917
 miyagi_koiki |       -4 | 11995
 miyagi_koiki |     -999 |     3
(6 行)

   ?column?    | aapptype | count 
---------------+----------+-------
 miyagi_kokuho |        6 | 57006
 miyagi_kokuho |        0 |  1012
(2 行)

    ?column?    | aapptype | count  
----------------+----------+--------
 miyazaki_koiki |        6 | 278289
 miyazaki_koiki |        0 |  13932
 miyazaki_koiki |       -3 |    649
 miyazaki_koiki |       -4 |  19071
(4 行)

   ?column?    | aapptype | count 
---------------+----------+-------
 nagoya_kouwan |        8 |    61
 nagoya_kouwan |        7 |   489
 nagoya_kouwan |        6 | 11699
 nagoya_kouwan |        0 |   556
 nagoya_kouwan |       -3 |   349
 nagoya_kouwan |       -4 |    19
(6 行)

     ?column?     | aapptype | count 
------------------+----------+-------
 nagoyashi_kokuho |        6 |  6495
 nagoyashi_kokuho |       -3 |   112
 nagoyashi_kokuho |       -4 |   517
(3 行)

    ?column?    | aapptype | count 
----------------+----------+-------
 nisshin_kokuho |        6 |    10
 nisshin_kokuho |        0 |     6
 nisshin_kokuho |       -3 |    12
(3 行)

  ?column?   | aapptype |  count  
-------------+----------+---------
 osaka_koiki |        8 |  377868
 osaka_koiki |        7 | 1001685
 osaka_koiki |        6 | 3317903
 osaka_koiki |        0 |   86230
 osaka_koiki |       -3 |  666292
 osaka_koiki |       -4 |  161544
 osaka_koiki |       -5 |  225518
 osaka_koiki |       -9 |   29935
 osaka_koiki |      -11 |   85538
 osaka_koiki |      -13 |   80457
 osaka_koiki |      -14 |   67102
 osaka_koiki |     -999 |       1
(12 行)

     ?column?     | aapptype | count 
------------------+----------+-------
 sakurashi_kokuho |        6 |  7994
 sakurashi_kokuho |        0 |  8436
 sakurashi_kokuho |       -3 |   152
 sakurashi_kokuho |       -4 |   739
 sakurashi_kokuho |       -7 |    41
(5 行)

  ?column?   | aapptype | count 
-------------+----------+-------
 sharp_kenpo |        6 | 30147
 sharp_kenpo |       -3 |   716
 sharp_kenpo |       -4 |   176
(3 行)

   ?column?    | aapptype | count 
---------------+----------+-------
 shimane_koiki |        8 |  4192
 shimane_koiki |        7 |  3269
 shimane_koiki |        6 | 24831
 shimane_koiki |        0 |  3454
 shimane_koiki |       -3 | 12814
 shimane_koiki |       -4 |  6790
(6 行)

    ?column?    | aapptype | count  
----------------+----------+--------
 shizuoka_koiki |        8 | 103048
 shizuoka_koiki |        7 |  20727
 shizuoka_koiki |        0 |  89808
 shizuoka_koiki |       -3 | 142183
 shizuoka_koiki |       -4 |   5569
(5 行)

     ?column?      | aapptype | count 
-------------------+----------+-------
 shizuokashi_ahaki |        8 |  2070
 shizuokashi_ahaki |        7 |   766
 shizuokashi_ahaki |       -3 |  3996
 shizuokashi_ahaki |       -4 |    59
 shizuokashi_ahaki |       -5 |   728
 shizuokashi_ahaki |       -7 |     2
 shizuokashi_ahaki |      -11 |   310
 shizuokashi_ahaki |      -13 |    18
 shizuokashi_ahaki |      -14 |   558
(9 行)

    ?column?     | aapptype | count  
-----------------+----------+--------
 tokushima_koiki |        8 |  35485
 tokushima_koiki |        7 |  20714
 tokushima_koiki |        6 | 247573
 tokushima_koiki |        0 | 105700
 tokushima_koiki |       -4 |      2
(5 行)

    ?column?    | aapptype | count 
----------------+----------+-------
 yamazaki_mazak |        6 |  5972
 yamazaki_mazak |        0 |   252
 yamazaki_mazak |       -3 |   317
 yamazaki_mazak |       -4 |     9
(4 行)

  ?column?   | aapptype | count 
-------------+----------+-------
 shinagawaku |        6 |  4187
 shinagawaku |       -3 |    37
(2 行)

  ?column?   | aapptype | count 
-------------+----------+-------
 ichiharashi |        6 |     7
(1 行)

  ?column?  | aapptype | count 
------------+----------+-------
 kashiwashi |        6 |    10
(1 行)

 ?column? | aapptype | count 
----------+----------+-------
 sakaishi |        8 |   181
 sakaishi |        7 |  1428
 sakaishi |        6 |     4
 sakaishi |        0 |   851
(4 行)

  ?column?  | aapptype | count 
------------+----------+-------
 ibarakishi |        8 |   170
 ibarakishi |        7 |   671
 ibarakishi |        6 |  6331
 ibarakishi |       -3 |   694
 ibarakishi |       -4 |   416
(5 行)

  ?column?  | aapptype | count 
------------+----------+-------
 kodairashi |        6 |     3
 kodairashi |       -3 |     3
(2 行)

  ?column?  | aapptype | count 
------------+----------+-------
 matsudoshi |        8 |   140
 matsudoshi |        7 |   102
 matsudoshi |        6 | 13104
 matsudoshi |        0 |   493
 matsudoshi |       -3 |   328
 matsudoshi |       -4 |     1
(6 行)

 ?column? | aapptype | count 
----------+----------+-------
 fuchushi |        8 |     4
 fuchushi |        7 |     4
 fuchushi |        6 |    11
 fuchushi |        0 |    39
(4 行)

      ?column?       | aapptype | count 
---------------------+----------+-------
 koshigayashi_kokuho |        6 |     4
 koshigayashi_kokuho |        0 |  1527
 koshigayashi_kokuho |       -3 |     1
(3 行)

