﻿--inputlogから入力時刻を取得して現在入力されている保険者を確認する

select i.insurername,l.insurerid,l.apptype,l.uid,u.name,date_trunc('minute',l.inputdt) 
from inputlog l inner join insurer i on 
l.insurerid=i.insurerid 
inner join users u on
l.uid=u.userid
where l.inputdt>=now()- interval '30 minute'

order by inputdt desc