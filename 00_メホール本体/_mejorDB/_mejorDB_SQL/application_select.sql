﻿select
aid
,ayear   as 施術年
,amonth  as 施術月
,hnum    as 被保険者番号
,psex     as 性別
,pbirthday 生年月日
,acounteddays 診療実日数
,fchargetype 新規継続
,aratio 給付割合
,atotal 合計金額
,acharge 請求金額
,aapptype 申請書種類
,fdistance 距離
,fvisitadd 往料加算
,statusflags ステータス
,cym 処理年月
,ym 診療年月
,ifirstdate1 初検日
,istartdate1 開始日
,ifinishdate1 終了日
,iname1 負傷名1
,iname2 負傷名2
,iname3 負傷名3
,iname4 負傷名4
,iname5 負傷名5
,taggeddatas

from application 
where cym=201908