--
-- PostgreSQL database dump
--

-- Dumped from database version 11.6
-- Dumped by pg_dump version 14.1

-- Started on 2022-02-23 13:29:19

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 208 (class 1255 OID 66615)
-- Name: addmonth(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.addmonth(ym integer, addm integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    res integer := ym;
    ay integer := addm/12;
    am integer := addm%12;
    m integer :=ym%100;
BEGIN
    res=res+(ay*100);
    res=res+am;
    IF 12<m+am THEN
        res=res+88;
    ELSIF m+am<1 THEN
        res=res-88;
    END IF;
    RETURN res;
END;
$$;


ALTER FUNCTION public.addmonth(ym integer, addm integer) OWNER TO postgres;

--
-- TOC entry 209 (class 1255 OID 66616)
-- Name: appcounter_update(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.appcounter_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'INSERT') THEN
  IF (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
    UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
  ELSE
    INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
  END IF;
  RETURN NEW;
ELSIF (TG_OP = 'UPDATE') THEN
  IF NEW.cym=OLD.cym THEN
    RETURN NEW;
  ELSE
    UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
    SELECT cym FROM appcounter WHERE cym=NEW.cym;
    IF  (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
      UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
    ELSE
      INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
    END IF;
  END IF;
ELSIF (TG_OP = 'DELETE') THEN
  UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
  RETURN OLD;
END IF;
END;
$$;


ALTER FUNCTION public.appcounter_update() OWNER TO postgres;

SET default_tablespace = '';

--
-- TOC entry 196 (class 1259 OID 66617)
-- Name: appcounter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.appcounter (
    cym integer NOT NULL,
    counter integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.appcounter OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 66621)
-- Name: application; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.application (
    aid integer NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    ayear integer DEFAULT 0 NOT NULL,
    amonth integer DEFAULT 0 NOT NULL,
    inum text DEFAULT ''::text NOT NULL,
    hnum text DEFAULT ''::text NOT NULL,
    hpref integer DEFAULT 0 NOT NULL,
    htype integer DEFAULT 0 NOT NULL,
    hname text DEFAULT ''::text NOT NULL,
    hzip text DEFAULT ''::text NOT NULL,
    haddress text DEFAULT ''::text NOT NULL,
    pname text DEFAULT ''::text NOT NULL,
    psex integer DEFAULT 0 NOT NULL,
    pbirthday date DEFAULT '0001-01-01'::date NOT NULL,
    asingle integer DEFAULT 0 NOT NULL,
    afamily integer DEFAULT 0 NOT NULL,
    aratio integer DEFAULT 0 NOT NULL,
    publcexpense text DEFAULT ''::text NOT NULL,
    emptytext1 text DEFAULT ''::text NOT NULL,
    emptyint1 integer DEFAULT 0 NOT NULL,
    emptytext2 text DEFAULT ''::text NOT NULL,
    ainspectdate date DEFAULT '0001-01-01'::date NOT NULL,
    emptyint2 integer DEFAULT 0 NOT NULL,
    emptyint3 integer DEFAULT 0 NOT NULL,
    aimagefile text DEFAULT ''::text NOT NULL,
    emptytext3 text DEFAULT ''::text NOT NULL,
    achargeyear integer DEFAULT 0 NOT NULL,
    achargemonth integer DEFAULT 0 NOT NULL,
    iname1 text DEFAULT ''::text NOT NULL,
    idate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    idays1 integer DEFAULT 0 NOT NULL,
    icourse1 integer DEFAULT 0 NOT NULL,
    ifee1 integer DEFAULT 0 NOT NULL,
    iname2 text DEFAULT ''::text NOT NULL,
    idate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    idays2 integer DEFAULT 0 NOT NULL,
    icourse2 integer DEFAULT 0 NOT NULL,
    ifee2 integer DEFAULT 0 NOT NULL,
    iname3 text DEFAULT ''::text NOT NULL,
    idate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    idays3 integer DEFAULT 0 NOT NULL,
    icourse3 integer DEFAULT 0 NOT NULL,
    ifee3 integer DEFAULT 0 NOT NULL,
    iname4 text DEFAULT ''::text NOT NULL,
    idate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    idays4 integer DEFAULT 0 NOT NULL,
    icourse4 integer DEFAULT 0 NOT NULL,
    ifee4 integer DEFAULT 0 NOT NULL,
    iname5 text DEFAULT ''::text NOT NULL,
    idate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    idays5 integer DEFAULT 0 NOT NULL,
    icourse5 integer DEFAULT 0 NOT NULL,
    ifee5 integer DEFAULT 0 NOT NULL,
    fchargetype integer DEFAULT 0 NOT NULL,
    fdistance integer DEFAULT 0 NOT NULL,
    fvisittimes integer DEFAULT 0 NOT NULL,
    fvisitfee integer DEFAULT 0 NOT NULL,
    fvisitadd integer DEFAULT 0 NOT NULL,
    sid text DEFAULT ''::text NOT NULL,
    sregnumber text DEFAULT ''::text NOT NULL,
    szip text DEFAULT ''::text NOT NULL,
    saddress text DEFAULT ''::text NOT NULL,
    sname text DEFAULT ''::text NOT NULL,
    stel text DEFAULT ''::text NOT NULL,
    sdoctor text DEFAULT ''::text NOT NULL,
    skana text DEFAULT ''::text NOT NULL,
    bacctype integer DEFAULT 0 NOT NULL,
    bname text DEFAULT ''::text NOT NULL,
    btype integer DEFAULT 0 NOT NULL,
    bbranch text DEFAULT ''::text NOT NULL,
    bbranchtype integer DEFAULT 0 NOT NULL,
    baccname text DEFAULT ''::text NOT NULL,
    bkana text DEFAULT ''::text NOT NULL,
    baccnumber text DEFAULT ''::text NOT NULL,
    atotal integer DEFAULT 0 NOT NULL,
    apartial integer DEFAULT 0 NOT NULL,
    acharge integer DEFAULT 0 NOT NULL,
    acounteddays integer DEFAULT 0 NOT NULL,
    numbering text DEFAULT ''::text NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    ufirst integer DEFAULT 0 NOT NULL,
    usecond integer DEFAULT 0 NOT NULL,
    uinquiry integer DEFAULT 0 NOT NULL,
    bui integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    statusflags integer DEFAULT 0 NOT NULL,
    shokaireason integer DEFAULT 0 NOT NULL,
    rrid integer DEFAULT 0 NOT NULL,
    inspectreasons integer DEFAULT 0 NOT NULL,
    additionaluid1 integer DEFAULT 0 NOT NULL,
    additionaluid2 integer DEFAULT 0 NOT NULL,
    memo_shokai text DEFAULT ''::text NOT NULL,
    memo_inspect text DEFAULT ''::text NOT NULL,
    memo text DEFAULT ''::text NOT NULL,
    paycode text DEFAULT ''::text NOT NULL,
    shokaicode text DEFAULT ''::text NOT NULL,
    ocrdata text DEFAULT ''::text NOT NULL,
    ufirstex integer DEFAULT 0 NOT NULL,
    usecondex integer DEFAULT 0 NOT NULL,
    kagoreasons integer DEFAULT 0 NOT NULL,
    saishinsareasons integer DEFAULT 0 NOT NULL,
    henreireasons integer DEFAULT 0 NOT NULL,
    taggeddatas text DEFAULT ''::text NOT NULL,
    comnum text DEFAULT ''::text NOT NULL,
    groupnum text DEFAULT ''::text NOT NULL,
    outmemo text DEFAULT ''::text NOT NULL,
    kagoreasons_xml xml
);


ALTER TABLE public.application OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 66750)
-- Name: ocr_ordered; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ocr_ordered (
    sid integer NOT NULL
);


ALTER TABLE public.ocr_ordered OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 66753)
-- Name: ocrimport_application; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ocrimport_application (
    aid integer,
    scanid integer,
    groupid integer,
    ayear integer,
    amonth integer,
    inum text,
    hnum text,
    hpref integer,
    htype integer,
    hname text,
    hzip text,
    haddress text,
    pname text,
    psex integer,
    pbirthday date,
    asingle integer,
    afamily integer,
    aratio integer,
    publcexpense text,
    emptytext1 text,
    emptyint1 integer,
    emptytext2 text,
    ainspectdate date,
    emptyint2 integer,
    emptyint3 integer,
    aimagefile text,
    emptytext3 text,
    achargeyear integer,
    achargemonth integer,
    iname1 text,
    idate1 date,
    ifirstdate1 date,
    istartdate1 date,
    ifinishdate1 date,
    idays1 integer,
    icourse1 integer,
    ifee1 integer,
    iname2 text,
    idate2 date,
    ifirstdate2 date,
    istartdate2 date,
    ifinishdate2 date,
    idays2 integer,
    icourse2 integer,
    ifee2 integer,
    iname3 text,
    idate3 date,
    ifirstdate3 date,
    istartdate3 date,
    ifinishdate3 date,
    idays3 integer,
    icourse3 integer,
    ifee3 integer,
    iname4 text,
    idate4 date,
    ifirstdate4 date,
    istartdate4 date,
    ifinishdate4 date,
    idays4 integer,
    icourse4 integer,
    ifee4 integer,
    iname5 text,
    idate5 date,
    ifirstdate5 date,
    istartdate5 date,
    ifinishdate5 date,
    idays5 integer,
    icourse5 integer,
    ifee5 integer,
    fchargetype integer,
    fdistance integer,
    fvisittimes integer,
    fvisitfee integer,
    fvisitadd integer,
    sid text,
    sregnumber text,
    szip text,
    saddress text,
    sname text,
    stel text,
    sdoctor text,
    skana text,
    bacctype integer,
    bname text,
    btype integer,
    bbranch text,
    bbranchtype integer,
    baccname text,
    bkana text,
    baccnumber text,
    atotal integer,
    apartial integer,
    acharge integer,
    acounteddays integer,
    numbering text,
    aapptype integer,
    note text,
    ufirst integer,
    usecond integer,
    uinquiry integer,
    bui integer,
    cym integer,
    ym integer,
    statusflags integer,
    shokaireason integer,
    rrid integer,
    inspectreasons integer,
    additionaluid1 integer,
    additionaluid2 integer,
    memo_shokai text,
    memo_inspect text,
    memo text,
    paycode text,
    shokaicode text,
    ocrdata text,
    ufirstex integer,
    usecondex integer,
    kagoreasons integer,
    saishinsareasons integer,
    henreireasons integer,
    taggeddatas text,
    comnum text,
    groupnum text,
    outmemo text,
    kagoreasons_xml xml
);


ALTER TABLE public.ocrimport_application OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 66759)
-- Name: ocrimport_scan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ocrimport_scan (
    sid integer,
    scandate date,
    cyear integer,
    cmonth integer,
    note1 text,
    note2 text,
    status integer,
    apptype integer,
    cym integer
);


ALTER TABLE public.ocrimport_scan OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 66765)
-- Name: ocrimport_scangroup; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ocrimport_scangroup (
    groupid integer,
    status integer,
    scanid integer,
    scandate date,
    scanuser integer,
    checkdate date,
    checkuser integer,
    inquirydate date,
    inquiryuser integer,
    workingusers text
);


ALTER TABLE public.ocrimport_scangroup OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 66771)
-- Name: scan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.scan (
    sid integer NOT NULL,
    scandate date,
    cyear integer,
    cmonth integer,
    note1 text,
    note2 text,
    status integer DEFAULT 0,
    apptype integer DEFAULT 0 NOT NULL,
    cym integer
);


ALTER TABLE public.scan OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 66779)
-- Name: scan_sid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.scan_sid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.scan_sid_seq OWNER TO postgres;

--
-- TOC entry 3021 (class 0 OID 0)
-- Dependencies: 203
-- Name: scan_sid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.scan_sid_seq OWNED BY public.scan.sid;


--
-- TOC entry 204 (class 1259 OID 66781)
-- Name: scangroup; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.scangroup (
    groupid integer NOT NULL,
    status integer,
    scanid integer,
    scandate date,
    scanuser integer,
    checkdate date,
    checkuser integer,
    inquirydate date,
    inquiryuser integer,
    workingusers text DEFAULT ''::text NOT NULL
);


ALTER TABLE public.scangroup OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 66788)
-- Name: shokaiexclude; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shokaiexclude (
    excludeid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    mediym integer DEFAULT 0 NOT NULL,
    chargeym integer DEFAULT 0 NOT NULL,
    hihonum text DEFAULT ''::text NOT NULL,
    birth date DEFAULT '0001-01-01'::date NOT NULL
);


ALTER TABLE public.shokaiexclude OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 66800)
-- Name: shokaiimage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shokaiimage (
    imageid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    filename text NOT NULL,
    code text NOT NULL,
    aid integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.shokaiimage OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 66809)
-- Name: shokaiimageimport; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shokaiimageimport (
    importid integer NOT NULL,
    importdate date,
    uid integer
);


ALTER TABLE public.shokaiimageimport OWNER TO postgres;

--
-- TOC entry 2861 (class 2604 OID 66812)
-- Name: scan sid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scan ALTER COLUMN sid SET DEFAULT nextval('public.scan_sid_seq'::regclass);


--
-- TOC entry 2873 (class 2606 OID 66814)
-- Name: appcounter appcounter_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appcounter
    ADD CONSTRAINT appcounter_pkey PRIMARY KEY (cym);


--
-- TOC entry 2879 (class 2606 OID 66816)
-- Name: application application_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.application
    ADD CONSTRAINT application_pkey PRIMARY KEY (aid);


--
-- TOC entry 2887 (class 2606 OID 66818)
-- Name: scangroup group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scangroup
    ADD CONSTRAINT group_pkey PRIMARY KEY (groupid);


--
-- TOC entry 2883 (class 2606 OID 66820)
-- Name: ocr_ordered ocr_ordered_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ocr_ordered
    ADD CONSTRAINT ocr_ordered_pkey PRIMARY KEY (sid);


--
-- TOC entry 2885 (class 2606 OID 66822)
-- Name: scan scan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scan
    ADD CONSTRAINT scan_pkey PRIMARY KEY (sid);


--
-- TOC entry 2893 (class 2606 OID 66824)
-- Name: shokaiimageimport shokai_image_import_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shokaiimageimport
    ADD CONSTRAINT shokai_image_import_pkey PRIMARY KEY (importid);


--
-- TOC entry 2891 (class 2606 OID 66826)
-- Name: shokaiimage shokai_image_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shokaiimage
    ADD CONSTRAINT shokai_image_pkey PRIMARY KEY (imageid);


--
-- TOC entry 2889 (class 2606 OID 66828)
-- Name: shokaiexclude shokaiexclude_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shokaiexclude
    ADD CONSTRAINT shokaiexclude_pkey PRIMARY KEY (excludeid);


--
-- TOC entry 2874 (class 1259 OID 66829)
-- Name: application_cym_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX application_cym_idx ON public.application USING btree (cym);


--
-- TOC entry 2875 (class 1259 OID 66830)
-- Name: application_groupid_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX application_groupid_idx ON public.application USING btree (groupid);


--
-- TOC entry 2876 (class 1259 OID 66831)
-- Name: application_hnum_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX application_hnum_idx ON public.application USING btree (hnum);


--
-- TOC entry 2877 (class 1259 OID 66832)
-- Name: application_pbirthday_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX application_pbirthday_idx ON public.application USING btree (pbirthday);


--
-- TOC entry 2880 (class 1259 OID 66833)
-- Name: application_rrid_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX application_rrid_idx ON public.application USING btree (rrid);


--
-- TOC entry 2881 (class 1259 OID 66834)
-- Name: application_shokaicode_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX application_shokaicode_idx ON public.application USING btree (shokaicode);


--
-- TOC entry 2894 (class 2620 OID 66835)
-- Name: application trigger_appcounter_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trigger_appcounter_update BEFORE INSERT OR DELETE OR UPDATE ON public.application FOR EACH ROW EXECUTE PROCEDURE public.appcounter_update();


-- Completed on 2022-02-23 13:29:27

--
-- PostgreSQL database dump complete
--

