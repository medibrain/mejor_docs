update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=1;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=2;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=3;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=4;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=5;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=6;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=7;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=8;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=9;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=10;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=11;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=12;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=13;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=14;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=15;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=16;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=17;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=18;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=19;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=20;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=21;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=22;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=23;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=24;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=25;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=26;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=27;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=28;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=29;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=30;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=31;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=32;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=33;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=34;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=35;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=36;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=37;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=38;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=39;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=40;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=41;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=42;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=43;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=44;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=45;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=46;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=47;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=48;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=49;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=50;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=51;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=52;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=53;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=54;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=55;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=56;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=57;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=58;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=59;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=60;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=61;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=62;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=63;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=64;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=65;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=66;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=67;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=68;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=69;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=70;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=71;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=72;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=73;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=74;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=75;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=76;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=77;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=78;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=79;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=80;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=81;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=82;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=83;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=84;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=85;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=86;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=87;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=88;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=89;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=90;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=91;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=92;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=93;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=94;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=95;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=96;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=97;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=98;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=99;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=100;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=101;
update medivery_setting set setting='<fields>
<hname><use>False</use><label>被保険者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></hname>
<pname><use>False</use><label>受療者氏名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>280</y><firsttime>true</firsttime><ratio>0.7</ratio></pname>
<hzip><use>True</use><label>郵便番号（〒）</label><desc>ハイフンなし</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></hzip>
<haddress><use>True</use><label>住所</label><desc>枝番号は半角</desc><x>800</x><y>220</y><firsttime>true</firsttime><ratio>0.7</ratio></haddress>
<sname><use>False</use><label>施術所名</label><desc></desc><x>80</x><y>2700</y><firsttime>true</firsttime><ratio>0.7</ratio></sname>
<fvisittimes><use>False</use><label>往療回数</label><desc></desc><x>425</x><y>1200</y><firsttime></firsttime><ratio></ratio></fvisittimes>
<paymentcode><use>False</use><label>支払先コード</label><desc>半角数字（5桁）</desc><x>800</x><y>0</y><firsttime></firsttime><ratio></ratio></paymentcode>
<birthday><use>False</use><label>生年月日</label><desc></desc><x>80</x><y>220</y><firsttime>false</firsttime><ratio></ratio></birthday>
<hnum><use>False</use><label>記号番号</label><desc>ハイフンがある場合は入力</desc><x>800</x><y>0</y><firsttime>false</firsttime><ratio></ratio></hnum>
<mediym><use>False</use><label>診療年月</label><desc></desc><x>0</x><y>0</y><firsttime>false</firsttime><ratio></ratio></mediym>
<counteddays><use>False</use><label>実日数</label><desc></desc><x>300</x><y>800</y><firsttime>false</firsttime><ratio></ratio></counteddays>
<total><use>False</use><label>合計</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></total>
<charge><use>False</use><label>請求金額</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></charge>
<partial><use>False</use><label>一部負担</label><desc></desc><x>800</x><y>1500</y><firsttime>false</firsttime><ratio></ratio></partial>
<drname><use>False</use><label>柔整師名</label><desc>姓名の間は全角スペース</desc><x>80</x><y>2300</y><firsttime>false</firsttime><ratio></ratio></drname>
<dates><use>False</use><label>診療日</label><desc>スペースまたはドット(.)区切りですべての診療日を入力</desc><x>80</x><y>1100</y><firsttime></firsttime><ratio></ratio></dates>
</fields>' where insurerid=102
