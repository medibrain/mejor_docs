
CREATE OR REPLACE FUNCTION public.ParentRelation(
	incym integer)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare 
_incym integer:=incym;
curNAPP cursor for select a.aid from application a where a.cym=_incym and a.ayear<0 and aapptype<>6 order by a.aid;
curAPP refcursor;
cnt integer:=0;
NonAppAID integer:=0;
cntRel integer:=1;
AppAID integer:=0;

--非申請書を申請書を紐づける為、application_aux.parentaidに申請書のaidを登録する処理
begin

	open curNAPP;

	loop
		fetch curNAPP INTO NonAppAID;
		raise notice '1:%',NonAppAID;
		
		if NonAppAID is null then 
			exit;
		end if;
		
		cntRel:=1;
		loop 

			
			open curAPP for select a.aid from application a where a.cym=_incym and a.ayear>0 and a.aid = NonAppAID - cntRel;
			
			fetch curAPP into AppAID;
			
			raise notice '2:%',NonAppAID - cnt;
			raise notice '3:%',AppAID;
			
			if AppAID is null or AppAID=0 then
				if cntRel>30 then 
					close curAPP;
					exit;
				end if;
				
				cntRel:=cntRel+1;				
				--cnt:=cnt+1;
				
				raise notice '4:% %',AppAID,cntRel;
				
				close curAPP;
				continue;
			end if;
		
			
			raise notice '5:%' ,AppAID;
			update application_aux set parentaid=AppAID where aid=NonAppAID ;
			close curAPP;
			exit;
			
		end loop;
		
		cnt:=cnt+1;
	end loop;
	
	close curNAPP;

	
return cnt;
end;
$BODY$;

ALTER FUNCTION public.ParentRelation(integer)
    OWNER TO postgres;
