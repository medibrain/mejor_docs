CREATE OR REPLACE FUNCTION public.parentrelation3(
	incym integer)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
declare 
_incym integer :=incym;

curNAPP cursor for select a.aid from application a 
	where a.cym=_incym and a.ayear<0 and a.ayear not in (-7,-8) and aapptype<>6 order by a.aid;

NonAppAID integer:=0;
AppAID integer:=0;
curAPP refcursor;
appcount integer :=0;
AppType integer:=0;
minGID integer:=0;
GID integer:=0;
cntGID integer:=1;
maxAID integer:=0;

begin
	open curNAPP;
	select INTO minGID min(a.groupid) from application a where cym=_incym group by a.groupid;
		
	loop
		
		
		fetch curNAPP INTO NonAppAID;
			exit when NOT FOUND;
		
		appcount:=1;
		
		
		loop 
			
			
			select INTO AppAID,AppType a.aid,a.aapptype from application a 
				where a.cym=_incym and a.ayear>0 and a.aid = NonAppAID - appcount;
			
			--一つ前のAIDが申請書の場合紐づける
			if AppType>0 then
				update application_aux set parentaid=AppAID where aid=NonAppAID ;	
				exit;
			else
				
				--100件前(1グループ)遡っても申請書がない場合
				if appcount>100 then 
					appcount:=1;
					
					loop 
						
					
						select INTO GID,maxAID groupid,max(aid) from application where groupid=groupid-cntGID group by groupid;

						--1つ前のグループがある場合
						if GID<>null and GID>=minGID then
							
							select INTO AppAID,AppType a.aid,a.aapptype from application a 
								where a.cym=_incym and a.ayear>0 and a.groupid=a.groupid-cntGID and a.aid = maxAID - appcount;
							
							
							--申請書の場合
							if AppType>0 then
								update application_aux set parentaid=AppAID where aid=NonAppAID ;
								exit;
							else
								appcount=appcount+1;
							end if;
						--1つ前のグループがない場合、グループIDを更に戻り最大aid取得
						else
							cntGID=cntGID+1;
							--select INTO AppAID max(a.aid) from application a where a.groupid=cntGID group by a.aid;
						end if;
					end loop;
					
					cntGID:=1;
					
				--同グループで申請書でない場合
				--else
				--	AppAID=AppAID-1;
					--appcount=appcount+1;
				end if;
				
			end if;
			
			appcount=appcount+1;			
			
		end loop;
		
	
	
	end loop;
    
    close curNAPP;	
	
	
end;
$BODY$;
ALTER FUNCTION public.ParentRelation3(integer)
    OWNER TO postgres;
