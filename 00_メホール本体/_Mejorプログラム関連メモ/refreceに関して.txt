質問事項
refreceには保険者から提供されたパンチデータが入る？　
	yes

refreceとapplicationの紐付けは、保険者によってキー（where条件）が違う？
	yes　提供されるデータはかなり違うので、クラス構成（テーブル構成）はもちろん、マッチングに使えるキーまで違ってきます。

refreceにaidがあるが、それは入力画面で登録したときに初めて入る？
	yes

refreceにrridがあるが、それはインポート時に決まる？
	yes　serial型で、オートナンバーと同じ

numbering、numberって何の番号？
	numbering保険者によって違う

refreceCoreには、ある程度共通項目がある？
	よくある情報とよくキーに使われる情報を集めてみた
	coreクラスでも空になっている項目は結構ある
	refreceCoreとrefreceテーブルは一応一致させておいて、そのほかの保険者特有のフィールドは適宜追加

マッチング＝refreceとapplicationの紐付けのこと？
	被保険者証番号、診療年月、合計金額　で複数出た場合は手で選んでもらう

refreceのテーブルとクラスは同期しているのか
	dapper.dllを使用しているので、クラスのメンバーとテーブルの項目は同じじゃないとだめ

画像ファイル名＝レセプト全国共通キーで、レセプト全国共通キーが保険者提供データに入っている場合
	ScanFormの画像登録時にApplicationテーブルにファイル名をNumberingとして登録する
	入力時にマッチングするための項目を入力しなくて済む（被保険者証番号、診療年月、合計金額等）
	


