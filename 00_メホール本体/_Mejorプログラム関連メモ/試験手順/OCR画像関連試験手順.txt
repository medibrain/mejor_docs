OCR画像関連テスト方法（画像登録〜Application登録まで）


１．オリジナルDBをバックアップする
２．テストDBに１をリストア
３．画像とってくる
４．試験サーバ接続したメホールで、未使用の処理年月で画像取り込み
５．試験サーバでBackGroundOCR回す ※帳票OCR7は試験サーバにしかない
６．BackGroundOCRは、Jyusei.insurerテーブルのscanorderの若いものからOCR作業するので、1にすれば早く順番が回ってくる
