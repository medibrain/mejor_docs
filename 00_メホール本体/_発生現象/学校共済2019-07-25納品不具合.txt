学校共済　納品データ不具合　2019/07/25

内容：学校共済に納品するデータのうち、医科レセ入力データの診療年月の年号が間違っていた
原因：学校共済医科レセ専用入力ソフト「MEPS」の基本仕様を把握できていなかったため
対応：柔整部後藤伸作さんに上記原因を説明し、資料作成（後藤伸作さん）にてJAST様にお伝えした
対策：メホール、MEPS両方の変換仕様を共通にした
