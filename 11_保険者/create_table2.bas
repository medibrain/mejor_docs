Attribute VB_Name = "Module2"
'テーブル定義からCreate文作成
Option Explicit
Dim arrRng() As String
Dim pkey() As String
Dim idx() As String
Dim idxno() As String
Dim strTableName As String

Private Sub btnMakeSQL_Click()
    Call FindTable
    MsgBox "終了"
End Sub

Private Sub FindTable()
    '全テーブル定義を探す
    Dim cell As Range
    For Each cell In Range("c:c")
    'For Each cell In Range("b:b")
        If cell.Value = "テーブル名" Then
            strTableName = cell.Offset(0, 1)
            
            CreateTableSQLite (cell.address)
            CreateTablePostgres (cell.address)
            CreateDapperClass (cell.address)
            
        End If
    Next
End Sub

'CreateTableSQLite文
Private Sub CreateTableSQLite(ByVal address As String)
    Range(address).Select
    Dim rng As Range
    Set rng = Range(address)
    
    Dim strsql As String
    
    strsql = strsql & "Create Table " & rng.Offset(0, 1) & "(" & vbCrLf
    
    Dim r As Integer
    r = 2
    
    Dim val As String
    Dim pcnt As Integer
    Dim flgRedim As Boolean
    flgRedim = False
    Dim flgRedimIdx As Boolean
    flgRedimIdx = False
    
    Erase pkey
    Dim idxcnt As Integer
    Erase idx
    
    
    Do While 1
        Dim c As Integer
        For c = -2 To 3
        'For c = -1 To 3
            Select Case c
            
            'PrimaryKey
            Case -2
            'Case -1
                If rng.Offset(r, c).Value = "1" Then
                    ReDim Preserve pkey(pcnt)
                    pkey(pcnt) = rng.Offset(r, 0).Value
                    pcnt = pcnt + 1
                    flgRedim = True
                    
                End If
                
            'index
            Case -1
            
                If rng.Offset(r, c).Value <> "" Then
                    ReDim Preserve idx(2, idxcnt)
                    idx(2, idxcnt) = rng.Offset(r, 0).Value
                    idxcnt = idxcnt + 1
                    flgRedim = True
                    
                End If


            'フィールド名、型
            Case 0 To 1
                val = rng.Offset(r, c).Value & Space(1)
                If Trim(val) = "" Then Exit Do
                strsql = strsql & val
            
            'サイズ
            Case 2
                val = rng.Offset(r, c).Value & Space(1)
                If Trim(val) <> "" Then
                    strsql = strsql & "(" & val & ")"
                End If
            
            'NOTNULL
            Case 3
                If rng.Offset(r, c).Value = "1" Then
                    strsql = strsql & " not null "
                End If
            End Select
            
        Next
        
        strsql = strsql & "," & vbCrLf
        r = r + 1
        
    Loop
    
    strsql = Left(Trim(strsql), Len(strsql) - 3)
    
    If flgRedim Then Call makePkey(strsql)
    
    strsql = strsql & ");"
    
    rng.Offset(0, 7) = strsql
    With Selection
        .HorizontalAlignment = xlGeneral
        .VerticalAlignment = xlCenter
        .WrapText = False
        .Orientation = 0
        '.AddIndent = False
        '.IndentLevel = 0
        '.ShrinkToFit = False
        '.ReadingOrder = xlContext
        '.MergeCells = False
    End With
    
    
End Sub


'PrimaryKeyオプション（SQLITE)
Private Sub makePkey(ByRef strsql As String)
    Dim r As Integer
    For r = 0 To UBound(pkey)
        If r = 0 Then strsql = strsql & ", primary key ("
        strsql = strsql & pkey(r)
        If r < UBound(pkey) Then strsql = strsql & ","
    Next
    strsql = strsql & ")"
    
End Sub

'visual studioで使用するdapper classの文字列
Private Sub CreateDapperClass(ByVal address As String)
    Range(address).Select
    Dim rng As Range
    Set rng = Range(address)
    
    Dim strDapper As String
    Dim strTableName As String
        
    strTableName = rng.Offset(0, 1)
    
    Dim r As Integer
    r = 2
    
    Dim val As String
    Dim pcnt As Integer
    Dim flgRedim As Boolean
    flgRedim = False
    
    Erase pkey
    Dim arrcmm() As String
    Dim cnt As Integer
    
    Do While 1
        Dim c As Integer
        For c = -1 To 5
            Select Case c
            


            'フィールド名
            Case 0
                val = rng.Offset(r, c).Value & Space(1)
                If Trim(val) = "" Then Exit Do
                
                
                If rng.Offset(r, 1).Value = "int" Then val = "public static int " & rng.Offset(r, 0).Value
                If rng.Offset(r, 1).Value = "varchar" Then val = "public static string " & rng.Offset(r, 0).Value
                If rng.Offset(r, 1).Value = "string" Then val = "public static string " & rng.Offset(r, 0).Value
                If rng.Offset(r, 1).Value = "date" Then val = "public static DateTime " & rng.Offset(r, 0).Value
                
    strDapper = strDapper & val & " { get;set; }"
    
          

            
            Case 1
                If rng.Offset(r, c).Value <> "" Then
                  
                    Select Case rng.Offset(r, c).Value
                        Case "int": strDapper = strDapper & "=0; "
                        Case "varchar": strDapper = strDapper & "=string.Empty;"
                        Case "string": strDapper = strDapper & "=string.Empty;"
                        Case "date": strDapper = strDapper & "=DateTime.MinValue;"
                        Case Else: strDapper = strDapper & "=" & rng.Offset(r, c).Value & ";"
                    End Select
    

                End If
        

              'cmm
            Case 5
                If rng.Offset(r, c).Value <> "" Then
                    
                   strDapper = strDapper & "                                         //" & rng.Offset(r, c).Value & ";"
                End If
            End Select
        Next
        
        
        r = r + 1
        strDapper = strDapper & vbCrLf
        
        
    Loop
    
    
    
    
    
    rng.Offset(0, 11) = strDapper
    
    
    
    
   
End Sub

Private Function CreateIndex() As String

Dim strindex As String

Dim strindexall As String

Dim r As Integer
Dim ridx As Integer
Dim previdx As String

For ridx = 0 To UBound(idxno)
    If idxno(ridx) = "" Then Exit For
    
    If strindex = "" Then
        strindex = "CREATE INDEX idx_" & strTableName & "_" & idxno(ridx) & " ON public." & strTableName & " USING btree ("
    ElseIf InStr(1, strindex, "CREATE INDEX idx_" & strTableName & "_" & idx(0, ridx), vbTextCompare) < 0 Then
        strindex = "CREATE INDEX idx_" & strTableName & "_" & idxno(ridx) & " ON public." & strTableName & " USING btree ("
    End If

    For r = 0 To UBound(idx)
        If idxno(ridx) = Split(idx(r), ",")(0) Then
    
            If previdx = "" Or idx(r) <> previdx Then
                strindex = strindex & Split(idx(r), ",")(1) & " ASC NULLS LAST,"
            End If
            
            previdx = idx(r)
        End If
    Next
    strindex = Left(strindex, Len(strindex) - 1)
    strindex = strindex & ")    TABLESPACE pg_default;"
    
    strindexall = strindexall & strindex
    strindex = ""
Next


CreateIndex = strindexall


End Function
'CreateTablePostgres文
Private Sub CreateTablePostgres(ByVal address As String)
    Range(address).Select
    Dim rng As Range
    Set rng = Range(address)
    
    Dim strsql As String
    Dim strTableName As String
    
    strsql = strsql & "Create Table " & rng.Offset(0, 1) & "(" & vbCrLf
    strTableName = rng.Offset(0, 1)
    
    Dim r As Integer
    r = 2
    
    Dim val As String
    Dim pcnt As Integer
    Dim flgRedim As Boolean
    
    flgRedim = False
    Dim flgRedimIdx As Boolean
    flgRedimIdx = False
    
    Erase pkey
    Dim arrcmm() As String
    Dim cnt As Integer
    Dim idxcnt As Integer
    Erase idx
    Dim idxnocnt As Integer
    Erase idxno
    
    Dim idxnor As Integer
    
    Do While 1
        Dim c As Integer
        For c = -2 To 5
        'For c = -1 To 5
            Select Case c
            
            'PrimaryKey
            Case -2
            'Case -1
                If rng.Offset(r, c).Value = "1" Then
                    ReDim Preserve pkey(pcnt)
                    pkey(pcnt) = rng.Offset(r, 0).Value
                    pcnt = pcnt + 1
                    flgRedim = True
                    
                End If
                
            'index
            Case -1
            
                If rng.Offset(r, c).Value <> "" Then
                    
                    'index番号配列
                    ReDim Preserve idxno(idxnocnt)
                    '同じindex番号入れない
                    Dim flg As Boolean
                    For idxnor = 0 To UBound(idxno)
                        If idxno(idxnor) = rng.Offset(r, -1).Value Then
                            flg = False
                            Exit For
                        Else
                            flg = True
                        End If
                        
                    Next
                    '違うindex番号のみ追加
                    If flg Then
                        ReDim Preserve idxno(idxnocnt)
                        idxno(idxnocnt) = rng.Offset(r, -1).Value
                        idxnocnt = idxnocnt + 1
                    End If
                    
                    'index番号とインデックスに使う列名
                    ReDim Preserve idx(idxcnt)
                    idx(idxcnt) = rng.Offset(r, -1).Value & "," & rng.Offset(r, 0).Value
                    idxcnt = idxcnt + 1
                    
                    'あった場合はtrue、後でindex構文を付け加えるフラグ
                    flgRedimIdx = True
                    
                End If
                
                
            'フィールド名、型
            Case 0 To 1
                val = rng.Offset(r, c).Value & Space(1)
                If Trim(val) = "" Then Exit Do
                strsql = strsql & val
            
            'サイズ
            Case 2
                val = rng.Offset(r, c).Value & Space(1)
                If Trim(val) <> "" Then
                    strsql = strsql & "(" & val & ")"
                End If
            
            'NOTNULL
            Case 3
                If rng.Offset(r, c).Value = "1" Then
                    strsql = strsql & " not null "
                End If

            'default
            Case 4
                If rng.Offset(r, c).Value <> "" Then
                    If rng.Offset(r, c).Value <> "MIN" Then
                        strsql = strsql & " default " & rng.Offset(r, c) & Space(1)
                    Else
                        strsql = strsql & " default '0001-01-01'" & Space(1)
                    End If
                End If

              'cmm
            Case 5
                If rng.Offset(r, c).Value <> "" Then
                    ReDim Preserve arrcmm(cnt)
                    arrcmm(cnt) = "comment on column " & strTableName & "." & rng.Offset(r, 0) & " IS '" & rng.Offset(r, c) & "';"
                  cnt = cnt + 1
                End If
            End Select
        Next
        
        strsql = strsql & "," & vbCrLf
        r = r + 1
        
    Loop
    
    strsql = Left(Trim(strsql), Len(strsql) - 3)
    
    If flgRedim Then Call makePkey(strsql)
    
    strsql = strsql & ");"
    
    
    
    
    For r = 0 To UBound(arrcmm)
        strsql = strsql & arrcmm(r) & vbCrLf
    Next
    
    
    
    If flgRedimIdx Then strsql = strsql & vbCrLf & CreateIndex
    
    
    
    
    
    
    rng.Offset(0, 9) = strsql
    With Selection
        .HorizontalAlignment = xlGeneral
        .VerticalAlignment = xlCenter
        .WrapText = False
        .Orientation = 0
        '.AddIndent = False
        '.IndentLevel = 0
        '.ShrinkToFit = False
        '.ReadingOrder = xlContext
        '.MergeCells = False
    End With
    
    
End Sub

