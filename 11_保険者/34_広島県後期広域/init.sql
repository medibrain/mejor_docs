--
-- PostgreSQL database dump
--

-- Dumped from database version 11.12
-- Dumped by pg_dump version 13.2

-- Started on 2022-03-12 19:08:29

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 219 (class 1255 OID 2499745)
-- Name: addmonth(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.addmonth(ym integer, addm integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    res integer := ym;
    ay integer := addm/12;
    am integer := addm%12;
    m integer :=ym%100;
BEGIN
    res=res+(ay*100);
    res=res+am;
    IF 12<m+am THEN
        res=res+88;
    ELSIF m+am<1 THEN
        res=res-88;
    END IF;
    RETURN res;
END;
$$;


ALTER FUNCTION public.addmonth(ym integer, addm integer) OWNER TO postgres;

--
-- TOC entry 220 (class 1255 OID 2499746)
-- Name: appcounter_update(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.appcounter_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'INSERT') THEN
  IF (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
    UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
  ELSE
    INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
  END IF;
  RETURN NEW;
ELSIF (TG_OP = 'UPDATE') THEN
  IF NEW.cym=OLD.cym THEN
    RETURN NEW;
  ELSE
    UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
    SELECT cym FROM appcounter WHERE cym=NEW.cym;
    IF  (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
      UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
    ELSE
      INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
    END IF;
  END IF;
ELSIF (TG_OP = 'DELETE') THEN
  UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
  RETURN OLD;
END IF;
END;
$$;


ALTER FUNCTION public.appcounter_update() OWNER TO postgres;

SET default_tablespace = '';

--
-- TOC entry 196 (class 1259 OID 2499747)
-- Name: appcounter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.appcounter (
    cym integer NOT NULL,
    counter integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.appcounter OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 2499751)
-- Name: application; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.application (
    aid integer NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    ayear integer DEFAULT 0 NOT NULL,
    amonth integer DEFAULT 0 NOT NULL,
    inum text DEFAULT ''::text NOT NULL,
    hnum text DEFAULT ''::text NOT NULL,
    hpref integer DEFAULT 0 NOT NULL,
    htype integer DEFAULT 0 NOT NULL,
    hname text DEFAULT ''::text NOT NULL,
    hzip text DEFAULT ''::text NOT NULL,
    haddress text DEFAULT ''::text NOT NULL,
    pname text DEFAULT ''::text NOT NULL,
    psex integer DEFAULT 0 NOT NULL,
    pbirthday date DEFAULT '0001-01-01'::date NOT NULL,
    asingle integer DEFAULT 0 NOT NULL,
    afamily integer DEFAULT 0 NOT NULL,
    aratio integer DEFAULT 0 NOT NULL,
    publcexpense text DEFAULT ''::text NOT NULL,
    emptytext1 text DEFAULT ''::text NOT NULL,
    emptyint1 integer DEFAULT 0 NOT NULL,
    emptytext2 text DEFAULT ''::text NOT NULL,
    ainspectdate date DEFAULT '0001-01-01'::date NOT NULL,
    emptyint2 integer DEFAULT 0 NOT NULL,
    emptyint3 integer DEFAULT 0 NOT NULL,
    aimagefile text DEFAULT ''::text NOT NULL,
    emptytext3 text DEFAULT ''::text NOT NULL,
    achargeyear integer DEFAULT 0 NOT NULL,
    achargemonth integer DEFAULT 0 NOT NULL,
    iname1 text DEFAULT ''::text NOT NULL,
    idate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    idays1 integer DEFAULT 0 NOT NULL,
    icourse1 integer DEFAULT 0 NOT NULL,
    ifee1 integer DEFAULT 0 NOT NULL,
    iname2 text DEFAULT ''::text NOT NULL,
    idate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    idays2 integer DEFAULT 0 NOT NULL,
    icourse2 integer DEFAULT 0 NOT NULL,
    ifee2 integer DEFAULT 0 NOT NULL,
    iname3 text DEFAULT ''::text NOT NULL,
    idate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    idays3 integer DEFAULT 0 NOT NULL,
    icourse3 integer DEFAULT 0 NOT NULL,
    ifee3 integer DEFAULT 0 NOT NULL,
    iname4 text DEFAULT ''::text NOT NULL,
    idate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    idays4 integer DEFAULT 0 NOT NULL,
    icourse4 integer DEFAULT 0 NOT NULL,
    ifee4 integer DEFAULT 0 NOT NULL,
    iname5 text DEFAULT ''::text NOT NULL,
    idate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    idays5 integer DEFAULT 0 NOT NULL,
    icourse5 integer DEFAULT 0 NOT NULL,
    ifee5 integer DEFAULT 0 NOT NULL,
    fchargetype integer DEFAULT 0 NOT NULL,
    fdistance integer DEFAULT 0 NOT NULL,
    fvisittimes integer DEFAULT 0 NOT NULL,
    fvisitfee integer DEFAULT 0 NOT NULL,
    fvisitadd integer DEFAULT 0 NOT NULL,
    sid text DEFAULT ''::text NOT NULL,
    sregnumber text DEFAULT ''::text NOT NULL,
    szip text DEFAULT ''::text NOT NULL,
    saddress text DEFAULT ''::text NOT NULL,
    sname text DEFAULT ''::text NOT NULL,
    stel text DEFAULT ''::text NOT NULL,
    sdoctor text DEFAULT ''::text NOT NULL,
    skana text DEFAULT ''::text NOT NULL,
    bacctype integer DEFAULT 0 NOT NULL,
    bname text DEFAULT ''::text NOT NULL,
    btype integer DEFAULT 0 NOT NULL,
    bbranch text DEFAULT ''::text NOT NULL,
    bbranchtype integer DEFAULT 0 NOT NULL,
    baccname text DEFAULT ''::text NOT NULL,
    bkana text DEFAULT ''::text NOT NULL,
    baccnumber text DEFAULT ''::text NOT NULL,
    atotal integer DEFAULT 0 NOT NULL,
    apartial integer DEFAULT 0 NOT NULL,
    acharge integer DEFAULT 0 NOT NULL,
    acounteddays integer DEFAULT 0 NOT NULL,
    numbering text DEFAULT ''::text NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    ufirst integer DEFAULT 0 NOT NULL,
    usecond integer DEFAULT 0 NOT NULL,
    uinquiry integer DEFAULT 0 NOT NULL,
    bui integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    statusflags integer DEFAULT 0 NOT NULL,
    shokaireason integer DEFAULT 0 NOT NULL,
    rrid integer DEFAULT 0 NOT NULL,
    inspectreasons integer DEFAULT 0 NOT NULL,
    additionaluid1 integer DEFAULT 0 NOT NULL,
    additionaluid2 integer DEFAULT 0 NOT NULL,
    memo_shokai text DEFAULT ''::text NOT NULL,
    memo_inspect text DEFAULT ''::text NOT NULL,
    memo text DEFAULT ''::text NOT NULL,
    paycode text DEFAULT ''::text NOT NULL,
    shokaicode text DEFAULT ''::text NOT NULL,
    ocrdata text DEFAULT ''::text NOT NULL,
    ufirstex integer DEFAULT 0 NOT NULL,
    usecondex integer DEFAULT 0 NOT NULL,
    kagoreasons integer DEFAULT 0 NOT NULL,
    saishinsareasons integer DEFAULT 0 NOT NULL,
    henreireasons integer DEFAULT 0 NOT NULL,
    taggeddatas text DEFAULT ''::text NOT NULL,
    comnum text DEFAULT ''::text NOT NULL,
    groupnum text DEFAULT ''::text NOT NULL,
    outmemo text DEFAULT ''::text NOT NULL,
    kagoreasons_xml xml
)
PARTITION BY LIST (cym);


ALTER TABLE public.application OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 2500004)
-- Name: application_202101; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.application_202101 (
    aid integer NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    ayear integer DEFAULT 0 NOT NULL,
    amonth integer DEFAULT 0 NOT NULL,
    inum text DEFAULT ''::text NOT NULL,
    hnum text DEFAULT ''::text NOT NULL,
    hpref integer DEFAULT 0 NOT NULL,
    htype integer DEFAULT 0 NOT NULL,
    hname text DEFAULT ''::text NOT NULL,
    hzip text DEFAULT ''::text NOT NULL,
    haddress text DEFAULT ''::text NOT NULL,
    pname text DEFAULT ''::text NOT NULL,
    psex integer DEFAULT 0 NOT NULL,
    pbirthday date DEFAULT '0001-01-01'::date NOT NULL,
    asingle integer DEFAULT 0 NOT NULL,
    afamily integer DEFAULT 0 NOT NULL,
    aratio integer DEFAULT 0 NOT NULL,
    publcexpense text DEFAULT ''::text NOT NULL,
    emptytext1 text DEFAULT ''::text NOT NULL,
    emptyint1 integer DEFAULT 0 NOT NULL,
    emptytext2 text DEFAULT ''::text NOT NULL,
    ainspectdate date DEFAULT '0001-01-01'::date NOT NULL,
    emptyint2 integer DEFAULT 0 NOT NULL,
    emptyint3 integer DEFAULT 0 NOT NULL,
    aimagefile text DEFAULT ''::text NOT NULL,
    emptytext3 text DEFAULT ''::text NOT NULL,
    achargeyear integer DEFAULT 0 NOT NULL,
    achargemonth integer DEFAULT 0 NOT NULL,
    iname1 text DEFAULT ''::text NOT NULL,
    idate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    idays1 integer DEFAULT 0 NOT NULL,
    icourse1 integer DEFAULT 0 NOT NULL,
    ifee1 integer DEFAULT 0 NOT NULL,
    iname2 text DEFAULT ''::text NOT NULL,
    idate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    idays2 integer DEFAULT 0 NOT NULL,
    icourse2 integer DEFAULT 0 NOT NULL,
    ifee2 integer DEFAULT 0 NOT NULL,
    iname3 text DEFAULT ''::text NOT NULL,
    idate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    idays3 integer DEFAULT 0 NOT NULL,
    icourse3 integer DEFAULT 0 NOT NULL,
    ifee3 integer DEFAULT 0 NOT NULL,
    iname4 text DEFAULT ''::text NOT NULL,
    idate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    idays4 integer DEFAULT 0 NOT NULL,
    icourse4 integer DEFAULT 0 NOT NULL,
    ifee4 integer DEFAULT 0 NOT NULL,
    iname5 text DEFAULT ''::text NOT NULL,
    idate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    idays5 integer DEFAULT 0 NOT NULL,
    icourse5 integer DEFAULT 0 NOT NULL,
    ifee5 integer DEFAULT 0 NOT NULL,
    fchargetype integer DEFAULT 0 NOT NULL,
    fdistance integer DEFAULT 0 NOT NULL,
    fvisittimes integer DEFAULT 0 NOT NULL,
    fvisitfee integer DEFAULT 0 NOT NULL,
    fvisitadd integer DEFAULT 0 NOT NULL,
    sid text DEFAULT ''::text NOT NULL,
    sregnumber text DEFAULT ''::text NOT NULL,
    szip text DEFAULT ''::text NOT NULL,
    saddress text DEFAULT ''::text NOT NULL,
    sname text DEFAULT ''::text NOT NULL,
    stel text DEFAULT ''::text NOT NULL,
    sdoctor text DEFAULT ''::text NOT NULL,
    skana text DEFAULT ''::text NOT NULL,
    bacctype integer DEFAULT 0 NOT NULL,
    bname text DEFAULT ''::text NOT NULL,
    btype integer DEFAULT 0 NOT NULL,
    bbranch text DEFAULT ''::text NOT NULL,
    bbranchtype integer DEFAULT 0 NOT NULL,
    baccname text DEFAULT ''::text NOT NULL,
    bkana text DEFAULT ''::text NOT NULL,
    baccnumber text DEFAULT ''::text NOT NULL,
    atotal integer DEFAULT 0 NOT NULL,
    apartial integer DEFAULT 0 NOT NULL,
    acharge integer DEFAULT 0 NOT NULL,
    acounteddays integer DEFAULT 0 NOT NULL,
    numbering text DEFAULT ''::text NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    ufirst integer DEFAULT 0 NOT NULL,
    usecond integer DEFAULT 0 NOT NULL,
    uinquiry integer DEFAULT 0 NOT NULL,
    bui integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    statusflags integer DEFAULT 0 NOT NULL,
    shokaireason integer DEFAULT 0 NOT NULL,
    rrid integer DEFAULT 0 NOT NULL,
    inspectreasons integer DEFAULT 0 NOT NULL,
    additionaluid1 integer DEFAULT 0 NOT NULL,
    additionaluid2 integer DEFAULT 0 NOT NULL,
    memo_shokai text DEFAULT ''::text NOT NULL,
    memo_inspect text DEFAULT ''::text NOT NULL,
    memo text DEFAULT ''::text NOT NULL,
    paycode text DEFAULT ''::text NOT NULL,
    shokaicode text DEFAULT ''::text NOT NULL,
    ocrdata text DEFAULT ''::text NOT NULL,
    ufirstex integer DEFAULT 0 NOT NULL,
    usecondex integer DEFAULT 0 NOT NULL,
    kagoreasons integer DEFAULT 0 NOT NULL,
    saishinsareasons integer DEFAULT 0 NOT NULL,
    henreireasons integer DEFAULT 0 NOT NULL,
    taggeddatas text DEFAULT ''::text NOT NULL,
    comnum text DEFAULT ''::text NOT NULL,
    groupnum text DEFAULT ''::text NOT NULL,
    outmemo text DEFAULT ''::text NOT NULL,
    kagoreasons_xml xml
);
ALTER TABLE ONLY public.application ATTACH PARTITION public.application_202101 FOR VALUES IN (202101);


ALTER TABLE public.application_202101 OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 2500349)
-- Name: application_202102; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.application_202102 (
    aid integer NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    ayear integer DEFAULT 0 NOT NULL,
    amonth integer DEFAULT 0 NOT NULL,
    inum text DEFAULT ''::text NOT NULL,
    hnum text DEFAULT ''::text NOT NULL,
    hpref integer DEFAULT 0 NOT NULL,
    htype integer DEFAULT 0 NOT NULL,
    hname text DEFAULT ''::text NOT NULL,
    hzip text DEFAULT ''::text NOT NULL,
    haddress text DEFAULT ''::text NOT NULL,
    pname text DEFAULT ''::text NOT NULL,
    psex integer DEFAULT 0 NOT NULL,
    pbirthday date DEFAULT '0001-01-01'::date NOT NULL,
    asingle integer DEFAULT 0 NOT NULL,
    afamily integer DEFAULT 0 NOT NULL,
    aratio integer DEFAULT 0 NOT NULL,
    publcexpense text DEFAULT ''::text NOT NULL,
    emptytext1 text DEFAULT ''::text NOT NULL,
    emptyint1 integer DEFAULT 0 NOT NULL,
    emptytext2 text DEFAULT ''::text NOT NULL,
    ainspectdate date DEFAULT '0001-01-01'::date NOT NULL,
    emptyint2 integer DEFAULT 0 NOT NULL,
    emptyint3 integer DEFAULT 0 NOT NULL,
    aimagefile text DEFAULT ''::text NOT NULL,
    emptytext3 text DEFAULT ''::text NOT NULL,
    achargeyear integer DEFAULT 0 NOT NULL,
    achargemonth integer DEFAULT 0 NOT NULL,
    iname1 text DEFAULT ''::text NOT NULL,
    idate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    idays1 integer DEFAULT 0 NOT NULL,
    icourse1 integer DEFAULT 0 NOT NULL,
    ifee1 integer DEFAULT 0 NOT NULL,
    iname2 text DEFAULT ''::text NOT NULL,
    idate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    idays2 integer DEFAULT 0 NOT NULL,
    icourse2 integer DEFAULT 0 NOT NULL,
    ifee2 integer DEFAULT 0 NOT NULL,
    iname3 text DEFAULT ''::text NOT NULL,
    idate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    idays3 integer DEFAULT 0 NOT NULL,
    icourse3 integer DEFAULT 0 NOT NULL,
    ifee3 integer DEFAULT 0 NOT NULL,
    iname4 text DEFAULT ''::text NOT NULL,
    idate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    idays4 integer DEFAULT 0 NOT NULL,
    icourse4 integer DEFAULT 0 NOT NULL,
    ifee4 integer DEFAULT 0 NOT NULL,
    iname5 text DEFAULT ''::text NOT NULL,
    idate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    idays5 integer DEFAULT 0 NOT NULL,
    icourse5 integer DEFAULT 0 NOT NULL,
    ifee5 integer DEFAULT 0 NOT NULL,
    fchargetype integer DEFAULT 0 NOT NULL,
    fdistance integer DEFAULT 0 NOT NULL,
    fvisittimes integer DEFAULT 0 NOT NULL,
    fvisitfee integer DEFAULT 0 NOT NULL,
    fvisitadd integer DEFAULT 0 NOT NULL,
    sid text DEFAULT ''::text NOT NULL,
    sregnumber text DEFAULT ''::text NOT NULL,
    szip text DEFAULT ''::text NOT NULL,
    saddress text DEFAULT ''::text NOT NULL,
    sname text DEFAULT ''::text NOT NULL,
    stel text DEFAULT ''::text NOT NULL,
    sdoctor text DEFAULT ''::text NOT NULL,
    skana text DEFAULT ''::text NOT NULL,
    bacctype integer DEFAULT 0 NOT NULL,
    bname text DEFAULT ''::text NOT NULL,
    btype integer DEFAULT 0 NOT NULL,
    bbranch text DEFAULT ''::text NOT NULL,
    bbranchtype integer DEFAULT 0 NOT NULL,
    baccname text DEFAULT ''::text NOT NULL,
    bkana text DEFAULT ''::text NOT NULL,
    baccnumber text DEFAULT ''::text NOT NULL,
    atotal integer DEFAULT 0 NOT NULL,
    apartial integer DEFAULT 0 NOT NULL,
    acharge integer DEFAULT 0 NOT NULL,
    acounteddays integer DEFAULT 0 NOT NULL,
    numbering text DEFAULT ''::text NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    ufirst integer DEFAULT 0 NOT NULL,
    usecond integer DEFAULT 0 NOT NULL,
    uinquiry integer DEFAULT 0 NOT NULL,
    bui integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    statusflags integer DEFAULT 0 NOT NULL,
    shokaireason integer DEFAULT 0 NOT NULL,
    rrid integer DEFAULT 0 NOT NULL,
    inspectreasons integer DEFAULT 0 NOT NULL,
    additionaluid1 integer DEFAULT 0 NOT NULL,
    additionaluid2 integer DEFAULT 0 NOT NULL,
    memo_shokai text DEFAULT ''::text NOT NULL,
    memo_inspect text DEFAULT ''::text NOT NULL,
    memo text DEFAULT ''::text NOT NULL,
    paycode text DEFAULT ''::text NOT NULL,
    shokaicode text DEFAULT ''::text NOT NULL,
    ocrdata text DEFAULT ''::text NOT NULL,
    ufirstex integer DEFAULT 0 NOT NULL,
    usecondex integer DEFAULT 0 NOT NULL,
    kagoreasons integer DEFAULT 0 NOT NULL,
    saishinsareasons integer DEFAULT 0 NOT NULL,
    henreireasons integer DEFAULT 0 NOT NULL,
    taggeddatas text DEFAULT ''::text NOT NULL,
    comnum text DEFAULT ''::text NOT NULL,
    groupnum text DEFAULT ''::text NOT NULL,
    outmemo text DEFAULT ''::text NOT NULL,
    kagoreasons_xml xml
);
ALTER TABLE ONLY public.application ATTACH PARTITION public.application_202102 FOR VALUES IN (202102);


ALTER TABLE public.application_202102 OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 2499877)
-- Name: application_aux; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.application_aux (
    aid integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    aimagefile character varying DEFAULT ''::character varying NOT NULL,
    origfile character varying DEFAULT ''::character varying NOT NULL,
    multitiff_pageno integer DEFAULT 0 NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    parentaid integer DEFAULT 0 NOT NULL,
    batchaid integer DEFAULT 0 NOT NULL,
    matchingid01 character varying DEFAULT ''::character varying NOT NULL,
    matchingid02 character varying DEFAULT ''::character varying NOT NULL,
    matchingid03 character varying DEFAULT ''::character varying NOT NULL,
    matchingid04 character varying DEFAULT ''::character varying NOT NULL,
    matchingid05 character varying DEFAULT ''::character varying NOT NULL,
    matchingid01date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid02date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid03date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid04date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid05date date DEFAULT '0001-01-01'::date NOT NULL
)
PARTITION BY LIST (cym);


ALTER TABLE public.application_aux OWNER TO postgres;

--
-- TOC entry 4519 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.aid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.aid IS 'aid';


--
-- TOC entry 4520 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.cym; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.cym IS 'メホール請求年月';


--
-- TOC entry 4521 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.scanid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.scanid IS 'スキャンID';


--
-- TOC entry 4522 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.groupid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.groupid IS 'グループID';


--
-- TOC entry 4523 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.aimagefile; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.aimagefile IS '変換後画像ファイル名';


--
-- TOC entry 4524 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.origfile; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.origfile IS '元画像ファイル名';


--
-- TOC entry 4525 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.multitiff_pageno; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.multitiff_pageno IS 'マルチtiff画像ページ番号';


--
-- TOC entry 4526 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.aapptype; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.aapptype IS 'AIDのaapptype';


--
-- TOC entry 4527 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.parentaid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.parentaid IS '続紙の親申請書AID';


--
-- TOC entry 4528 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.batchaid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.batchaid IS '所属バッチAID';


--
-- TOC entry 4529 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid01; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid01 IS 'マッチングしたデータのID1';


--
-- TOC entry 4530 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid02; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid02 IS 'マッチングしたデータのID2';


--
-- TOC entry 4531 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid03; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid03 IS 'マッチングしたデータのID3';


--
-- TOC entry 4532 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid04; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid04 IS 'マッチングしたデータのID4';


--
-- TOC entry 4533 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid05; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid05 IS 'マッチングしたデータのID5';


--
-- TOC entry 4534 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid01date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid01date IS 'マッチング時刻1';


--
-- TOC entry 4535 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid02date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid02date IS 'マッチング時刻2';


--
-- TOC entry 4536 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid03date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid03date IS 'マッチング時刻3';


--
-- TOC entry 4537 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid04date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid04date IS 'マッチング時刻4';


--
-- TOC entry 4538 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN application_aux.matchingid05date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.application_aux.matchingid05date IS 'マッチング時刻5';


--
-- TOC entry 208 (class 1259 OID 2500140)
-- Name: application_aux_202101; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.application_aux_202101 (
    aid integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    aimagefile character varying DEFAULT ''::character varying NOT NULL,
    origfile character varying DEFAULT ''::character varying NOT NULL,
    multitiff_pageno integer DEFAULT 0 NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    parentaid integer DEFAULT 0 NOT NULL,
    batchaid integer DEFAULT 0 NOT NULL,
    matchingid01 character varying DEFAULT ''::character varying NOT NULL,
    matchingid02 character varying DEFAULT ''::character varying NOT NULL,
    matchingid03 character varying DEFAULT ''::character varying NOT NULL,
    matchingid04 character varying DEFAULT ''::character varying NOT NULL,
    matchingid05 character varying DEFAULT ''::character varying NOT NULL,
    matchingid01date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid02date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid03date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid04date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid05date date DEFAULT '0001-01-01'::date NOT NULL
);
ALTER TABLE ONLY public.application_aux ATTACH PARTITION public.application_aux_202101 FOR VALUES IN (202101);


ALTER TABLE public.application_aux_202101 OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 2500485)
-- Name: application_aux_202102; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.application_aux_202102 (
    aid integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    aimagefile character varying DEFAULT ''::character varying NOT NULL,
    origfile character varying DEFAULT ''::character varying NOT NULL,
    multitiff_pageno integer DEFAULT 0 NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    parentaid integer DEFAULT 0 NOT NULL,
    batchaid integer DEFAULT 0 NOT NULL,
    matchingid01 character varying DEFAULT ''::character varying NOT NULL,
    matchingid02 character varying DEFAULT ''::character varying NOT NULL,
    matchingid03 character varying DEFAULT ''::character varying NOT NULL,
    matchingid04 character varying DEFAULT ''::character varying NOT NULL,
    matchingid05 character varying DEFAULT ''::character varying NOT NULL,
    matchingid01date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid02date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid03date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid04date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid05date date DEFAULT '0001-01-01'::date NOT NULL
);
ALTER TABLE ONLY public.application_aux ATTACH PARTITION public.application_aux_202102 FOR VALUES IN (202102);


ALTER TABLE public.application_aux_202102 OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 2500212)
-- Name: refrece_rrid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.refrece_rrid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.refrece_rrid_seq OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 2500214)
-- Name: refrece; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.refrece (
    rrid integer DEFAULT nextval('public.refrece_rrid_seq'::regclass) NOT NULL,
    apptype integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    mym integer DEFAULT 0 NOT NULL,
    insnum text DEFAULT ''::text NOT NULL,
    num text DEFAULT ''::text NOT NULL,
    name text DEFAULT ''::text NOT NULL,
    days integer DEFAULT 0 NOT NULL,
    total integer DEFAULT 0 NOT NULL,
    charge integer DEFAULT 0 NOT NULL,
    clinicnum text DEFAULT ''::text NOT NULL,
    clinicname text DEFAULT ''::text NOT NULL,
    drnum text DEFAULT ''::text NOT NULL,
    drname text DEFAULT ''::text NOT NULL,
    searchnum text DEFAULT ''::text NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    aid integer DEFAULT 0 NOT NULL,
    kana text DEFAULT ''::text NOT NULL,
    zip text DEFAULT ''::text NOT NULL,
    add text DEFAULT ''::text NOT NULL,
    destname text DEFAULT ''::text NOT NULL,
    destzip text DEFAULT ''::text NOT NULL,
    destadd text DEFAULT ''::text NOT NULL,
    cliniczip text DEFAULT ''::text NOT NULL,
    clinicadd text DEFAULT ''::text NOT NULL
)
PARTITION BY LIST (cym);


ALTER TABLE public.refrece OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 2500647)
-- Name: refrece_202101; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.refrece_202101 (
    rrid integer DEFAULT nextval('public.refrece_rrid_seq'::regclass) NOT NULL,
    apptype integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    mym integer DEFAULT 0 NOT NULL,
    insnum text DEFAULT ''::text NOT NULL,
    num text DEFAULT ''::text NOT NULL,
    name text DEFAULT ''::text NOT NULL,
    days integer DEFAULT 0 NOT NULL,
    total integer DEFAULT 0 NOT NULL,
    charge integer DEFAULT 0 NOT NULL,
    clinicnum text DEFAULT ''::text NOT NULL,
    clinicname text DEFAULT ''::text NOT NULL,
    drnum text DEFAULT ''::text NOT NULL,
    drname text DEFAULT ''::text NOT NULL,
    searchnum text DEFAULT ''::text NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    aid integer DEFAULT 0 NOT NULL,
    kana text DEFAULT ''::text NOT NULL,
    zip text DEFAULT ''::text NOT NULL,
    add text DEFAULT ''::text NOT NULL,
    destname text DEFAULT ''::text NOT NULL,
    destzip text DEFAULT ''::text NOT NULL,
    destadd text DEFAULT ''::text NOT NULL,
    cliniczip text DEFAULT ''::text NOT NULL,
    clinicadd text DEFAULT ''::text NOT NULL
);
ALTER TABLE ONLY public.refrece ATTACH PARTITION public.refrece_202101 FOR VALUES IN (202101);


ALTER TABLE public.refrece_202101 OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 2500524)
-- Name: refrece_202102; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.refrece_202102 (
    rrid integer DEFAULT nextval('public.refrece_rrid_seq'::regclass) NOT NULL,
    apptype integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    mym integer DEFAULT 0 NOT NULL,
    insnum text DEFAULT ''::text NOT NULL,
    num text DEFAULT ''::text NOT NULL,
    name text DEFAULT ''::text NOT NULL,
    days integer DEFAULT 0 NOT NULL,
    total integer DEFAULT 0 NOT NULL,
    charge integer DEFAULT 0 NOT NULL,
    clinicnum text DEFAULT ''::text NOT NULL,
    clinicname text DEFAULT ''::text NOT NULL,
    drnum text DEFAULT ''::text NOT NULL,
    drname text DEFAULT ''::text NOT NULL,
    searchnum text DEFAULT ''::text NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    aid integer DEFAULT 0 NOT NULL,
    kana text DEFAULT ''::text NOT NULL,
    zip text DEFAULT ''::text NOT NULL,
    add text DEFAULT ''::text NOT NULL,
    destname text DEFAULT ''::text NOT NULL,
    destzip text DEFAULT ''::text NOT NULL,
    destadd text DEFAULT ''::text NOT NULL,
    cliniczip text DEFAULT ''::text NOT NULL,
    clinicadd text DEFAULT ''::text NOT NULL
);
ALTER TABLE ONLY public.refrece ATTACH PARTITION public.refrece_202102 FOR VALUES IN (202102);


ALTER TABLE public.refrece_202102 OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 2499990)
-- Name: refreceimport; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.refreceimport (
    importid integer NOT NULL,
    apptype integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    rececount integer DEFAULT 0 NOT NULL,
    importdate date DEFAULT '0001-01-01'::date NOT NULL,
    filename text DEFAULT ''::text NOT NULL,
    userid integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.refreceimport OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 2499912)
-- Name: scan_sid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.scan_sid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.scan_sid_seq OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 2499914)
-- Name: scan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.scan (
    sid integer DEFAULT nextval('public.scan_sid_seq'::regclass) NOT NULL,
    scandate date,
    cyear integer,
    cmonth integer,
    note1 text,
    note2 text,
    status integer DEFAULT 0,
    apptype integer DEFAULT 0 NOT NULL,
    cym integer NOT NULL
)
PARTITION BY LIST (cym);


ALTER TABLE public.scan OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 2500168)
-- Name: scan_202101; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.scan_202101 (
    sid integer DEFAULT nextval('public.scan_sid_seq'::regclass) NOT NULL,
    scandate date,
    cyear integer,
    cmonth integer,
    note1 text,
    note2 text,
    status integer DEFAULT 0,
    apptype integer DEFAULT 0 NOT NULL,
    cym integer NOT NULL
);
ALTER TABLE ONLY public.scan ATTACH PARTITION public.scan_202101 FOR VALUES IN (202101);


ALTER TABLE public.scan_202101 OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 2500513)
-- Name: scan_202102; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.scan_202102 (
    sid integer DEFAULT nextval('public.scan_sid_seq'::regclass) NOT NULL,
    scandate date,
    cyear integer,
    cmonth integer,
    note1 text,
    note2 text,
    status integer DEFAULT 0,
    apptype integer DEFAULT 0 NOT NULL,
    cym integer NOT NULL
);
ALTER TABLE ONLY public.scan ATTACH PARTITION public.scan_202102 FOR VALUES IN (202102);


ALTER TABLE public.scan_202102 OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 2499920)
-- Name: scangroup; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.scangroup (
    groupid integer NOT NULL,
    status integer,
    scanid integer,
    scandate date NOT NULL,
    scanuser integer,
    checkdate date,
    checkuser integer,
    inquirydate date,
    inquiryuser integer,
    workingusers text DEFAULT ''::text NOT NULL
)
PARTITION BY LIST (scandate);


ALTER TABLE public.scangroup OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 2500179)
-- Name: scangroup_20220311; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.scangroup_20220311 (
    groupid integer NOT NULL,
    status integer,
    scanid integer,
    scandate date NOT NULL,
    scanuser integer,
    checkdate date,
    checkuser integer,
    inquirydate date,
    inquiryuser integer,
    workingusers text DEFAULT ''::text NOT NULL
);
ALTER TABLE ONLY public.scangroup ATTACH PARTITION public.scangroup_20220311 FOR VALUES IN ('2022-03-11');


ALTER TABLE public.scangroup_20220311 OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 2500200)
-- Name: scangroup_20220312; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.scangroup_20220312 (
    groupid integer NOT NULL,
    status integer,
    scanid integer,
    scandate date NOT NULL,
    scanuser integer,
    checkdate date,
    checkuser integer,
    inquirydate date,
    inquiryuser integer,
    workingusers text DEFAULT ''::text NOT NULL
);
ALTER TABLE ONLY public.scangroup ATTACH PARTITION public.scangroup_20220312 FOR VALUES IN ('2022-03-12');


ALTER TABLE public.scangroup_20220312 OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 2499924)
-- Name: shokai; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shokai (
    aid integer NOT NULL,
    shokai_id integer NOT NULL,
    ym integer NOT NULL,
    barcode text NOT NULL,
    shokai_reason integer DEFAULT 0 NOT NULL,
    shokai_status integer DEFAULT 0 NOT NULL,
    shokai_result integer DEFAULT 0 NOT NULL,
    henrei boolean DEFAULT false NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    update_date date DEFAULT '0001-01-01'::date NOT NULL,
    update_uid integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.shokai OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 2499937)
-- Name: shokaiexclude; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shokaiexclude (
    excludeid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    mediym integer DEFAULT 0 NOT NULL,
    chargeym integer DEFAULT 0 NOT NULL,
    hihonum text DEFAULT ''::text NOT NULL,
    birth date DEFAULT '0001-01-01'::date NOT NULL
);


ALTER TABLE public.shokaiexclude OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 2499949)
-- Name: shokaiimage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shokaiimage (
    imageid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    filename text NOT NULL,
    code text NOT NULL,
    aid integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.shokaiimage OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 2499958)
-- Name: shokaiimageimport; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shokaiimageimport (
    importid integer NOT NULL,
    importdate date,
    uid integer
);


ALTER TABLE public.shokaiimageimport OWNER TO postgres;

--
-- TOC entry 4318 (class 2606 OID 2499962)
-- Name: appcounter appcounter_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appcounter
    ADD CONSTRAINT appcounter_pkey PRIMARY KEY (cym);


--
-- TOC entry 4320 (class 2606 OID 2499966)
-- Name: application application_p_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.application
    ADD CONSTRAINT application_p_pkey PRIMARY KEY (aid, cym);


--
-- TOC entry 4345 (class 2606 OID 2500131)
-- Name: application_202101 application_202101_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.application_202101
    ADD CONSTRAINT application_202101_pkey PRIMARY KEY (aid, cym);


--
-- TOC entry 4361 (class 2606 OID 2500476)
-- Name: application_202102 application_202102_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.application_202102
    ADD CONSTRAINT application_202102_pkey PRIMARY KEY (aid, cym);


--
-- TOC entry 4326 (class 2606 OID 2499964)
-- Name: application_aux application_p_aux_pkey1; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.application_aux
    ADD CONSTRAINT application_p_aux_pkey1 PRIMARY KEY (aid, cym);


--
-- TOC entry 4349 (class 2606 OID 2500164)
-- Name: application_aux_202101 application_aux_202101_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.application_aux_202101
    ADD CONSTRAINT application_aux_202101_pkey PRIMARY KEY (aid, cym);


--
-- TOC entry 4365 (class 2606 OID 2500509)
-- Name: application_aux_202102 application_aux_202102_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.application_aux_202102
    ADD CONSTRAINT application_aux_202102_pkey PRIMARY KEY (aid, cym);


--
-- TOC entry 4330 (class 2606 OID 2499968)
-- Name: scangroup group_p_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scangroup
    ADD CONSTRAINT group_p_pkey PRIMARY KEY (groupid, scandate);


--
-- TOC entry 4357 (class 2606 OID 2500243)
-- Name: refrece refrece_p_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.refrece
    ADD CONSTRAINT refrece_p_pkey PRIMARY KEY (rrid, cym);


--
-- TOC entry 4371 (class 2606 OID 2500676)
-- Name: refrece_202101 refrece_202101_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.refrece_202101
    ADD CONSTRAINT refrece_202101_pkey PRIMARY KEY (rrid, cym);


--
-- TOC entry 4369 (class 2606 OID 2500553)
-- Name: refrece_202102 refrece_202102_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.refrece_202102
    ADD CONSTRAINT refrece_202102_pkey PRIMARY KEY (rrid, cym);


--
-- TOC entry 4341 (class 2606 OID 2500003)
-- Name: refreceimport refreceimport_p_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.refreceimport
    ADD CONSTRAINT refreceimport_p_pkey PRIMARY KEY (importid, cym);


--
-- TOC entry 4328 (class 2606 OID 2499974)
-- Name: scan scan_p_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scan
    ADD CONSTRAINT scan_p_pkey PRIMARY KEY (sid, cym);


--
-- TOC entry 4351 (class 2606 OID 2500175)
-- Name: scan_202101 scan_202101_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scan_202101
    ADD CONSTRAINT scan_202101_pkey PRIMARY KEY (sid, cym);


--
-- TOC entry 4367 (class 2606 OID 2500520)
-- Name: scan_202102 scan_202102_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scan_202102
    ADD CONSTRAINT scan_202102_pkey PRIMARY KEY (sid, cym);


--
-- TOC entry 4353 (class 2606 OID 2500184)
-- Name: scangroup_20220311 scangroup_20220311_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scangroup_20220311
    ADD CONSTRAINT scangroup_20220311_pkey PRIMARY KEY (groupid, scandate);


--
-- TOC entry 4355 (class 2606 OID 2500205)
-- Name: scangroup_20220312 scangroup_20220312_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scangroup_20220312
    ADD CONSTRAINT scangroup_20220312_pkey PRIMARY KEY (groupid, scandate);


--
-- TOC entry 4339 (class 2606 OID 2499976)
-- Name: shokaiimageimport shokai_image_import_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shokaiimageimport
    ADD CONSTRAINT shokai_image_import_pkey PRIMARY KEY (importid);


--
-- TOC entry 4337 (class 2606 OID 2499978)
-- Name: shokaiimage shokai_image_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shokaiimage
    ADD CONSTRAINT shokai_image_pkey PRIMARY KEY (imageid);


--
-- TOC entry 4333 (class 2606 OID 2499980)
-- Name: shokai shokai_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shokai
    ADD CONSTRAINT shokai_pkey PRIMARY KEY (aid);


--
-- TOC entry 4335 (class 2606 OID 2499982)
-- Name: shokaiexclude shokaiexclude_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shokaiexclude
    ADD CONSTRAINT shokaiexclude_pkey PRIMARY KEY (excludeid);


--
-- TOC entry 4321 (class 1259 OID 2499983)
-- Name: idx_application_cym_desc_aid; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_application_cym_desc_aid ON ONLY public.application USING btree (cym DESC NULLS LAST, aid);


--
-- TOC entry 4342 (class 1259 OID 2500132)
-- Name: application_202101_cym_aid_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX application_202101_cym_aid_idx ON public.application_202101 USING btree (cym DESC NULLS LAST, aid);


--
-- TOC entry 4324 (class 1259 OID 2499986)
-- Name: idx_application_scangroup; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_application_scangroup ON ONLY public.application USING btree (groupid);


--
-- TOC entry 4343 (class 1259 OID 2500135)
-- Name: application_202101_groupid_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX application_202101_groupid_idx ON public.application_202101 USING btree (groupid);


--
-- TOC entry 4323 (class 1259 OID 2499985)
-- Name: idx_application_scan_scangroup; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_application_scan_scangroup ON ONLY public.application USING btree (scanid, groupid);


--
-- TOC entry 4346 (class 1259 OID 2500134)
-- Name: application_202101_scanid_groupid_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX application_202101_scanid_groupid_idx ON public.application_202101 USING btree (scanid, groupid);


--
-- TOC entry 4322 (class 1259 OID 2499984)
-- Name: idx_application_scan; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_application_scan ON ONLY public.application USING btree (scanid);


--
-- TOC entry 4347 (class 1259 OID 2500133)
-- Name: application_202101_scanid_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX application_202101_scanid_idx ON public.application_202101 USING btree (scanid);


--
-- TOC entry 4358 (class 1259 OID 2500477)
-- Name: application_202102_cym_aid_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX application_202102_cym_aid_idx ON public.application_202102 USING btree (cym DESC NULLS LAST, aid);


--
-- TOC entry 4359 (class 1259 OID 2500480)
-- Name: application_202102_groupid_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX application_202102_groupid_idx ON public.application_202102 USING btree (groupid);


--
-- TOC entry 4362 (class 1259 OID 2500479)
-- Name: application_202102_scanid_groupid_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX application_202102_scanid_groupid_idx ON public.application_202102 USING btree (scanid, groupid);


--
-- TOC entry 4363 (class 1259 OID 2500478)
-- Name: application_202102_scanid_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX application_202102_scanid_idx ON public.application_202102 USING btree (scanid);


--
-- TOC entry 4331 (class 1259 OID 2499988)
-- Name: shokai_barcode_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX shokai_barcode_idx ON public.shokai USING btree (barcode);


--
-- TOC entry 4372 (class 0 OID 0)
-- Name: application_202101_cym_aid_idx; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.idx_application_cym_desc_aid ATTACH PARTITION public.application_202101_cym_aid_idx;


--
-- TOC entry 4373 (class 0 OID 0)
-- Name: application_202101_groupid_idx; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.idx_application_scangroup ATTACH PARTITION public.application_202101_groupid_idx;


--
-- TOC entry 4374 (class 0 OID 0)
-- Name: application_202101_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.application_p_pkey ATTACH PARTITION public.application_202101_pkey;


--
-- TOC entry 4375 (class 0 OID 0)
-- Name: application_202101_scanid_groupid_idx; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.idx_application_scan_scangroup ATTACH PARTITION public.application_202101_scanid_groupid_idx;


--
-- TOC entry 4376 (class 0 OID 0)
-- Name: application_202101_scanid_idx; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.idx_application_scan ATTACH PARTITION public.application_202101_scanid_idx;


--
-- TOC entry 4381 (class 0 OID 0)
-- Name: application_202102_cym_aid_idx; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.idx_application_cym_desc_aid ATTACH PARTITION public.application_202102_cym_aid_idx;


--
-- TOC entry 4382 (class 0 OID 0)
-- Name: application_202102_groupid_idx; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.idx_application_scangroup ATTACH PARTITION public.application_202102_groupid_idx;


--
-- TOC entry 4383 (class 0 OID 0)
-- Name: application_202102_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.application_p_pkey ATTACH PARTITION public.application_202102_pkey;


--
-- TOC entry 4384 (class 0 OID 0)
-- Name: application_202102_scanid_groupid_idx; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.idx_application_scan_scangroup ATTACH PARTITION public.application_202102_scanid_groupid_idx;


--
-- TOC entry 4385 (class 0 OID 0)
-- Name: application_202102_scanid_idx; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.idx_application_scan ATTACH PARTITION public.application_202102_scanid_idx;


--
-- TOC entry 4377 (class 0 OID 0)
-- Name: application_aux_202101_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.application_p_aux_pkey1 ATTACH PARTITION public.application_aux_202101_pkey;


--
-- TOC entry 4386 (class 0 OID 0)
-- Name: application_aux_202102_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.application_p_aux_pkey1 ATTACH PARTITION public.application_aux_202102_pkey;


--
-- TOC entry 4389 (class 0 OID 0)
-- Name: refrece_202101_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.refrece_p_pkey ATTACH PARTITION public.refrece_202101_pkey;


--
-- TOC entry 4388 (class 0 OID 0)
-- Name: refrece_202102_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.refrece_p_pkey ATTACH PARTITION public.refrece_202102_pkey;


--
-- TOC entry 4378 (class 0 OID 0)
-- Name: scan_202101_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.scan_p_pkey ATTACH PARTITION public.scan_202101_pkey;


--
-- TOC entry 4387 (class 0 OID 0)
-- Name: scan_202102_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.scan_p_pkey ATTACH PARTITION public.scan_202102_pkey;


--
-- TOC entry 4379 (class 0 OID 0)
-- Name: scangroup_20220311_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.group_p_pkey ATTACH PARTITION public.scangroup_20220311_pkey;


--
-- TOC entry 4380 (class 0 OID 0)
-- Name: scangroup_20220312_pkey; Type: INDEX ATTACH; Schema: public; Owner: postgres
--

ALTER INDEX public.group_p_pkey ATTACH PARTITION public.scangroup_20220312_pkey;


--
-- TOC entry 4390 (class 2620 OID 2500681)
-- Name: application_202101 trigger_appcounter_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trigger_appcounter_update BEFORE INSERT OR DELETE OR UPDATE ON public.application_202101 FOR EACH ROW EXECUTE PROCEDURE public.appcounter_update();


--
-- TOC entry 4391 (class 2620 OID 2500558)
-- Name: application_202102 trigger_appcounter_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trigger_appcounter_update BEFORE INSERT OR DELETE OR UPDATE ON public.application_202102 FOR EACH ROW EXECUTE PROCEDURE public.appcounter_update();


--
-- TOC entry 4518 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM rdsadmin;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2022-03-12 19:08:35

--
-- PostgreSQL database dump complete
--

