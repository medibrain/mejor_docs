PGDMP                         y            awajishi    11.10    13.2 l               0    0    ENCODING    ENCODING     (   SET client_encoding = 'SHIFT_JIS_2004';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                       1262    556202    awajishi    DATABASE     ]   CREATE DATABASE awajishi WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.UTF-8';
    DROP DATABASE awajishi;
                postgres    false                       0    0    SCHEMA public    ACL     ¢   REVOKE ALL ON SCHEMA public FROM rdsadmin;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                   postgres    false    3            Ñ            1255    556203    addmonth(integer, integer)    FUNCTION       CREATE FUNCTION public.addmonth(ym integer, addm integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    res integer := ym;
    ay integer := addm/12;
    am integer := addm%12;
    m integer :=ym%100;
BEGIN
    res=res+(ay*100);
    res=res+am;
    IF 12<m+am THEN
        res=res+88;
    ELSIF m+am<1 THEN
        res=res-88;
    END IF;
    RETURN res;
END;
$$;
 9   DROP FUNCTION public.addmonth(ym integer, addm integer);
       public          postgres    false            Ò            1255    556204    appcounter_update()    FUNCTION       CREATE FUNCTION public.appcounter_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'INSERT') THEN
  IF (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
    UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
  ELSE
    INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
  END IF;
  RETURN NEW;
ELSIF (TG_OP = 'UPDATE') THEN
  IF NEW.cym=OLD.cym THEN
    RETURN NEW;
  ELSE
    UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
    SELECT cym FROM appcounter WHERE cym=NEW.cym;
    IF  (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
      UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
    ELSE
      INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
    END IF;
  END IF;
ELSIF (TG_OP = 'DELETE') THEN
  UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
  RETURN OLD;
END IF;
END;
$$;
 *   DROP FUNCTION public.appcounter_update();
       public          postgres    false            Ä            1259    556205 
   appcounter    TABLE     e   CREATE TABLE public.appcounter (
    cym integer NOT NULL,
    counter integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.appcounter;
       public            postgres    false            Å            1259    556209    application    TABLE     à  CREATE TABLE public.application (
    aid integer NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    ayear integer DEFAULT 0 NOT NULL,
    amonth integer DEFAULT 0 NOT NULL,
    inum text DEFAULT ''::text NOT NULL,
    hnum text DEFAULT ''::text NOT NULL,
    hpref integer DEFAULT 0 NOT NULL,
    htype integer DEFAULT 0 NOT NULL,
    hname text DEFAULT ''::text NOT NULL,
    hzip text DEFAULT ''::text NOT NULL,
    haddress text DEFAULT ''::text NOT NULL,
    pname text DEFAULT ''::text NOT NULL,
    psex integer DEFAULT 0 NOT NULL,
    pbirthday date DEFAULT '0001-01-01'::date NOT NULL,
    asingle integer DEFAULT 0 NOT NULL,
    afamily integer DEFAULT 0 NOT NULL,
    aratio integer DEFAULT 0 NOT NULL,
    publcexpense text DEFAULT ''::text NOT NULL,
    emptytext1 text DEFAULT ''::text NOT NULL,
    emptyint1 integer DEFAULT 0 NOT NULL,
    emptytext2 text DEFAULT ''::text NOT NULL,
    ainspectdate date DEFAULT '0001-01-01'::date NOT NULL,
    emptyint2 integer DEFAULT 0 NOT NULL,
    emptyint3 integer DEFAULT 0 NOT NULL,
    aimagefile text DEFAULT ''::text NOT NULL,
    emptytext3 text DEFAULT ''::text NOT NULL,
    achargeyear integer DEFAULT 0 NOT NULL,
    achargemonth integer DEFAULT 0 NOT NULL,
    iname1 text DEFAULT ''::text NOT NULL,
    idate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    idays1 integer DEFAULT 0 NOT NULL,
    icourse1 integer DEFAULT 0 NOT NULL,
    ifee1 integer DEFAULT 0 NOT NULL,
    iname2 text DEFAULT ''::text NOT NULL,
    idate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    idays2 integer DEFAULT 0 NOT NULL,
    icourse2 integer DEFAULT 0 NOT NULL,
    ifee2 integer DEFAULT 0 NOT NULL,
    iname3 text DEFAULT ''::text NOT NULL,
    idate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    idays3 integer DEFAULT 0 NOT NULL,
    icourse3 integer DEFAULT 0 NOT NULL,
    ifee3 integer DEFAULT 0 NOT NULL,
    iname4 text DEFAULT ''::text NOT NULL,
    idate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    idays4 integer DEFAULT 0 NOT NULL,
    icourse4 integer DEFAULT 0 NOT NULL,
    ifee4 integer DEFAULT 0 NOT NULL,
    iname5 text DEFAULT ''::text NOT NULL,
    idate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    idays5 integer DEFAULT 0 NOT NULL,
    icourse5 integer DEFAULT 0 NOT NULL,
    ifee5 integer DEFAULT 0 NOT NULL,
    fchargetype integer DEFAULT 0 NOT NULL,
    fdistance integer DEFAULT 0 NOT NULL,
    fvisittimes integer DEFAULT 0 NOT NULL,
    fvisitfee integer DEFAULT 0 NOT NULL,
    fvisitadd integer DEFAULT 0 NOT NULL,
    sid text DEFAULT ''::text NOT NULL,
    sregnumber text DEFAULT ''::text NOT NULL,
    szip text DEFAULT ''::text NOT NULL,
    saddress text DEFAULT ''::text NOT NULL,
    sname text DEFAULT ''::text NOT NULL,
    stel text DEFAULT ''::text NOT NULL,
    sdoctor text DEFAULT ''::text NOT NULL,
    skana text DEFAULT ''::text NOT NULL,
    bacctype integer DEFAULT 0 NOT NULL,
    bname text DEFAULT ''::text NOT NULL,
    btype integer DEFAULT 0 NOT NULL,
    bbranch text DEFAULT ''::text NOT NULL,
    bbranchtype integer DEFAULT 0 NOT NULL,
    baccname text DEFAULT ''::text NOT NULL,
    bkana text DEFAULT ''::text NOT NULL,
    baccnumber text DEFAULT ''::text NOT NULL,
    atotal integer DEFAULT 0 NOT NULL,
    apartial integer DEFAULT 0 NOT NULL,
    acharge integer DEFAULT 0 NOT NULL,
    acounteddays integer DEFAULT 0 NOT NULL,
    numbering text DEFAULT ''::text NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    ufirst integer DEFAULT 0 NOT NULL,
    usecond integer DEFAULT 0 NOT NULL,
    uinquiry integer DEFAULT 0 NOT NULL,
    bui integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    statusflags integer DEFAULT 0 NOT NULL,
    shokaireason integer DEFAULT 0 NOT NULL,
    rrid integer DEFAULT 0 NOT NULL,
    inspectreasons integer DEFAULT 0 NOT NULL,
    additionaluid1 integer DEFAULT 0 NOT NULL,
    additionaluid2 integer DEFAULT 0 NOT NULL,
    memo_shokai text DEFAULT ''::text NOT NULL,
    memo_inspect text DEFAULT ''::text NOT NULL,
    memo text DEFAULT ''::text NOT NULL,
    paycode text DEFAULT ''::text NOT NULL,
    shokaicode text DEFAULT ''::text NOT NULL,
    ocrdata text DEFAULT ''::text NOT NULL,
    ufirstex integer DEFAULT 0 NOT NULL,
    usecondex integer DEFAULT 0 NOT NULL,
    kagoreasons integer DEFAULT 0 NOT NULL,
    saishinsareasons integer DEFAULT 0 NOT NULL,
    henreireasons integer DEFAULT 0 NOT NULL,
    taggeddatas text DEFAULT ''::text NOT NULL,
    comnum text DEFAULT ''::text NOT NULL,
    groupnum text DEFAULT ''::text NOT NULL,
    outmemo text DEFAULT ''::text NOT NULL,
    kagoreasons_xml xml
);
    DROP TABLE public.application;
       public            postgres    false            Æ            1259    556338    application_aux    TABLE     ®  CREATE TABLE public.application_aux (
    aid integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    aimagefile character varying DEFAULT ''::character varying NOT NULL,
    origfile character varying DEFAULT ''::character varying NOT NULL,
    multitiff_pageno integer DEFAULT 0 NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    parentaid integer DEFAULT 0 NOT NULL,
    batchaid integer DEFAULT 0 NOT NULL,
    matchingid01 character varying DEFAULT ''::character varying NOT NULL,
    matchingid02 character varying DEFAULT ''::character varying NOT NULL,
    matchingid03 character varying DEFAULT ''::character varying NOT NULL,
    matchingid04 character varying DEFAULT ''::character varying NOT NULL,
    matchingid05 character varying DEFAULT ''::character varying NOT NULL,
    matchingid01date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid02date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid03date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid04date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid05date date DEFAULT '0001-01-01'::date NOT NULL
);
 #   DROP TABLE public.application_aux;
       public            postgres    false                       0    0    COLUMN application_aux.aid    COMMENT     7   COMMENT ON COLUMN public.application_aux.aid IS 'aid';
          public          postgres    false    198                       0    0    COLUMN application_aux.cym    COMMENT     D   COMMENT ON COLUMN public.application_aux.cym IS 'z[¿N';
          public          postgres    false    198                       0    0    COLUMN application_aux.scanid    COMMENT     A   COMMENT ON COLUMN public.application_aux.scanid IS 'XLID';
          public          postgres    false    198                       0    0    COLUMN application_aux.groupid    COMMENT     B   COMMENT ON COLUMN public.application_aux.groupid IS 'O[vID';
          public          postgres    false    198                       0    0 !   COLUMN application_aux.aimagefile    COMMENT     O   COMMENT ON COLUMN public.application_aux.aimagefile IS 'Ï·ãæt@C¼';
          public          postgres    false    198                       0    0    COLUMN application_aux.origfile    COMMENT     I   COMMENT ON COLUMN public.application_aux.origfile IS '³æt@C¼';
          public          postgres    false    198                       0    0 '   COLUMN application_aux.multitiff_pageno    COMMENT     Y   COMMENT ON COLUMN public.application_aux.multitiff_pageno IS '}`tiffæy[WÔ';
          public          postgres    false    198                       0    0    COLUMN application_aux.aapptype    COMMENT     F   COMMENT ON COLUMN public.application_aux.aapptype IS 'AIDÌaapptype';
          public          postgres    false    198                       0    0     COLUMN application_aux.parentaid    COMMENT     K   COMMENT ON COLUMN public.application_aux.parentaid IS '±Ìe\¿AID';
          public          postgres    false    198                       0    0    COLUMN application_aux.batchaid    COMMENT     F   COMMENT ON COLUMN public.application_aux.batchaid IS '®ob`AID';
          public          postgres    false    198                        0    0 #   COLUMN application_aux.matchingid01    COMMENT     V   COMMENT ON COLUMN public.application_aux.matchingid01 IS '}b`Oµ½f[^ÌID1';
          public          postgres    false    198            !           0    0 #   COLUMN application_aux.matchingid02    COMMENT     V   COMMENT ON COLUMN public.application_aux.matchingid02 IS '}b`Oµ½f[^ÌID2';
          public          postgres    false    198            "           0    0 #   COLUMN application_aux.matchingid03    COMMENT     V   COMMENT ON COLUMN public.application_aux.matchingid03 IS '}b`Oµ½f[^ÌID3';
          public          postgres    false    198            #           0    0 #   COLUMN application_aux.matchingid04    COMMENT     V   COMMENT ON COLUMN public.application_aux.matchingid04 IS '}b`Oµ½f[^ÌID4';
          public          postgres    false    198            $           0    0 #   COLUMN application_aux.matchingid05    COMMENT     V   COMMENT ON COLUMN public.application_aux.matchingid05 IS '}b`Oµ½f[^ÌID5';
          public          postgres    false    198            %           0    0 '   COLUMN application_aux.matchingid01date    COMMENT     P   COMMENT ON COLUMN public.application_aux.matchingid01date IS '}b`O1';
          public          postgres    false    198            &           0    0 '   COLUMN application_aux.matchingid02date    COMMENT     P   COMMENT ON COLUMN public.application_aux.matchingid02date IS '}b`O2';
          public          postgres    false    198            '           0    0 '   COLUMN application_aux.matchingid03date    COMMENT     P   COMMENT ON COLUMN public.application_aux.matchingid03date IS '}b`O3';
          public          postgres    false    198            (           0    0 '   COLUMN application_aux.matchingid04date    COMMENT     P   COMMENT ON COLUMN public.application_aux.matchingid04date IS '}b`O4';
          public          postgres    false    198            )           0    0 '   COLUMN application_aux.matchingid05date    COMMENT     P   COMMENT ON COLUMN public.application_aux.matchingid05date IS '}b`O5';
          public          postgres    false    198            Ï            1259    556592    imp_seq    SEQUENCE     p   CREATE SEQUENCE public.imp_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
    DROP SEQUENCE public.imp_seq;
       public          postgres    false            Ð            1259    556652 
   dataimport    TABLE     Þ  CREATE TABLE public.dataimport (
    f000_importid integer DEFAULT nextval('public.imp_seq'::regclass) NOT NULL,
    f001_pageins character varying DEFAULT ''::character varying NOT NULL,
    f002_pageall character varying DEFAULT ''::character varying NOT NULL,
    f003_shinsay character varying DEFAULT ''::character varying NOT NULL,
    f004_shinsam character varying DEFAULT ''::character varying NOT NULL,
    f005_menjo character varying DEFAULT ''::character varying NOT NULL,
    f006_insnum character varying(10) DEFAULT ''::character varying NOT NULL,
    f007_insname character varying(10) DEFAULT ''::character varying NOT NULL,
    f008_clinicnum character varying(16) DEFAULT ''::character varying NOT NULL,
    f009_pname character varying DEFAULT ''::character varying NOT NULL,
    f010_knum character varying DEFAULT ''::character varying NOT NULL,
    f011_futan character varying DEFAULT ''::character varying NOT NULL,
    f012_startdate character varying(10) DEFAULT ''::character varying NOT NULL,
    f013_fukusu character varying DEFAULT ''::character varying NOT NULL,
    f014_total character varying DEFAULT ''::character varying NOT NULL,
    f015_biko1 character varying DEFAULT ''::character varying NOT NULL,
    f016_shusei character varying DEFAULT ''::character varying NOT NULL,
    f017_examy character varying DEFAULT ''::character varying NOT NULL,
    f018_examm character varying DEFAULT ''::character varying NOT NULL,
    f019_clinicname character varying DEFAULT ''::character varying NOT NULL,
    f020_hnum character varying DEFAULT ''::character varying NOT NULL,
    f021_pbirthe character varying DEFAULT ''::character varying NOT NULL,
    f022_pbirthy character varying DEFAULT ''::character varying NOT NULL,
    f023_pbirthm character varying DEFAULT ''::character varying NOT NULL,
    f024_pbirthd character varying DEFAULT ''::character varying NOT NULL,
    f025_personalcode character varying DEFAULT ''::character varying NOT NULL,
    f026_pgender character varying DEFAULT ''::character varying NOT NULL,
    f027_jukyunum character varying DEFAULT ''::character varying NOT NULL,
    f028_ratio character varying DEFAULT ''::character varying NOT NULL,
    f029_firstdate character varying DEFAULT ''::character varying NOT NULL,
    f030_finishdate character varying DEFAULT ''::character varying NOT NULL,
    f031_counteddays character varying DEFAULT ''::character varying NOT NULL,
    f032_partial character varying DEFAULT ''::character varying NOT NULL,
    f033_biko2 character varying DEFAULT ''::character varying NOT NULL,
    f034_comnum character varying(20) DEFAULT ''::character varying NOT NULL,
    cym integer DEFAULT 0,
    shinsaymad integer DEFAULT 0,
    ymad integer DEFAULT 0,
    birthad date DEFAULT '0001-01-01'::date,
    firstdatead date DEFAULT '0001-01-01'::date,
    startdatead date DEFAULT '0001-01-01'::date,
    finishdatead date DEFAULT '0001-01-01'::date,
    hihonum_narrow character varying DEFAULT ''::character varying NOT NULL
);
    DROP TABLE public.dataimport;
       public            postgres    false    207            *           0    0    COLUMN dataimport.f000_importid    COMMENT     E   COMMENT ON COLUMN public.dataimport.f000_importid IS 'C|[gID';
          public          postgres    false    208            +           0    0    COLUMN dataimport.f001_pageins    COMMENT     F   COMMENT ON COLUMN public.dataimport.f001_pageins IS 'Û¯Òy[W';
          public          postgres    false    208            ,           0    0    COLUMN dataimport.f002_pageall    COMMENT     B   COMMENT ON COLUMN public.dataimport.f002_pageall IS 'y[W';
          public          postgres    false    208            -           0    0    COLUMN dataimport.f003_shinsay    COMMENT     >   COMMENT ON COLUMN public.dataimport.f003_shinsay IS 'R¸N';
          public          postgres    false    208            .           0    0    COLUMN dataimport.f004_shinsam    COMMENT     >   COMMENT ON COLUMN public.dataimport.f004_shinsam IS 'R¸';
          public          postgres    false    208            /           0    0    COLUMN dataimport.f005_menjo    COMMENT     :   COMMENT ON COLUMN public.dataimport.f005_menjo IS 'Æ';
          public          postgres    false    208            0           0    0    COLUMN dataimport.f006_insnum    COMMENT     A   COMMENT ON COLUMN public.dataimport.f006_insnum IS 'Û¯ÒÔ';
          public          postgres    false    208            1           0    0    COLUMN dataimport.f007_insname    COMMENT     @   COMMENT ON COLUMN public.dataimport.f007_insname IS 'Û¯Ò¼';
          public          postgres    false    208            2           0    0     COLUMN dataimport.f008_clinicnum    COMMENT     B   COMMENT ON COLUMN public.dataimport.f008_clinicnum IS '@ÖÔ';
          public          postgres    false    208            3           0    0    COLUMN dataimport.f009_pname    COMMENT     @   COMMENT ON COLUMN public.dataimport.f009_pname IS 'ófÒ¼';
          public          postgres    false    208            4           0    0    COLUMN dataimport.f010_knum    COMMENT     C   COMMENT ON COLUMN public.dataimport.f010_knum IS 'öïSÒÔ';
          public          postgres    false    208            5           0    0    COLUMN dataimport.f011_futan    COMMENT     >   COMMENT ON COLUMN public.dataimport.f011_futan IS 'Sæª';
          public          postgres    false    208            6           0    0     COLUMN dataimport.f012_startdate    COMMENT     H   COMMENT ON COLUMN public.dataimport.f012_startdate IS '{pJnNú';
          public          postgres    false    208            7           0    0    COLUMN dataimport.f013_fukusu    COMMENT     A   COMMENT ON COLUMN public.dataimport.f013_fukusu IS '¡æª';
          public          postgres    false    208            8           0    0    COLUMN dataimport.f014_total    COMMENT     >   COMMENT ON COLUMN public.dataimport.f014_total IS 'èàz';
          public          postgres    false    208            9           0    0    COLUMN dataimport.f015_biko1    COMMENT     ;   COMMENT ON COLUMN public.dataimport.f015_biko1 IS 'õl1';
          public          postgres    false    208            :           0    0    COLUMN dataimport.f016_shusei    COMMENT     ?   COMMENT ON COLUMN public.dataimport.f016_shusei IS 'C³æª';
          public          postgres    false    208            ;           0    0    COLUMN dataimport.f017_examy    COMMENT     <   COMMENT ON COLUMN public.dataimport.f017_examy IS '{ÃN';
          public          postgres    false    208            <           0    0    COLUMN dataimport.f018_examm    COMMENT     <   COMMENT ON COLUMN public.dataimport.f018_examm IS '{Ã';
          public          postgres    false    208            =           0    0 !   COLUMN dataimport.f019_clinicname    COMMENT     A   COMMENT ON COLUMN public.dataimport.f019_clinicname IS '@Ö¼';
          public          postgres    false    208            >           0    0    COLUMN dataimport.f020_hnum    COMMENT     A   COMMENT ON COLUMN public.dataimport.f020_hnum IS 'íÛ¯ÒÔ';
          public          postgres    false    208            ?           0    0    COLUMN dataimport.f021_pbirthe    COMMENT     D   COMMENT ON COLUMN public.dataimport.f021_pbirthe IS '¶NúN';
          public          postgres    false    208            @           0    0    COLUMN dataimport.f022_pbirthy    COMMENT     B   COMMENT ON COLUMN public.dataimport.f022_pbirthy IS '¶NúN';
          public          postgres    false    208            A           0    0    COLUMN dataimport.f023_pbirthm    COMMENT     B   COMMENT ON COLUMN public.dataimport.f023_pbirthm IS '¶Nú';
          public          postgres    false    208            B           0    0    COLUMN dataimport.f024_pbirthd    COMMENT     B   COMMENT ON COLUMN public.dataimport.f024_pbirthd IS '¶Núú';
          public          postgres    false    208            C           0    0 #   COLUMN dataimport.f025_personalcode    COMMENT     G   COMMENT ON COLUMN public.dataimport.f025_personalcode IS 'Z¯R[h';
          public          postgres    false    208            D           0    0    COLUMN dataimport.f026_pgender    COMMENT     <   COMMENT ON COLUMN public.dataimport.f026_pgender IS '«Ê';
          public          postgres    false    208            E           0    0    COLUMN dataimport.f027_jukyunum    COMMENT     C   COMMENT ON COLUMN public.dataimport.f027_jukyunum IS 'óÒÔ';
          public          postgres    false    208            F           0    0    COLUMN dataimport.f028_ratio    COMMENT     >   COMMENT ON COLUMN public.dataimport.f028_ratio IS 'S';
          public          postgres    false    208            G           0    0     COLUMN dataimport.f029_firstdate    COMMENT     D   COMMENT ON COLUMN public.dataimport.f029_firstdate IS 'Nú';
          public          postgres    false    208            H           0    0 !   COLUMN dataimport.f030_finishdate    COMMENT     I   COMMENT ON COLUMN public.dataimport.f030_finishdate IS '{pI¹Nú';
          public          postgres    false    208            I           0    0 "   COLUMN dataimport.f031_counteddays    COMMENT     B   COMMENT ON COLUMN public.dataimport.f031_counteddays IS 'Àú';
          public          postgres    false    208            J           0    0    COLUMN dataimport.f032_partial    COMMENT     B   COMMENT ON COLUMN public.dataimport.f032_partial IS 'êSà';
          public          postgres    false    208            K           0    0    COLUMN dataimport.f033_biko2    COMMENT     ;   COMMENT ON COLUMN public.dataimport.f033_biko2 IS 'õl2';
          public          postgres    false    208            L           0    0    COLUMN dataimport.f034_comnum    COMMENT     C   COMMENT ON COLUMN public.dataimport.f034_comnum IS 'S¤ÊL[';
          public          postgres    false    208            M           0    0    COLUMN dataimport.cym    COMMENT     ?   COMMENT ON COLUMN public.dataimport.cym IS 'z[¿N';
          public          postgres    false    208            N           0    0    COLUMN dataimport.shinsaymad    COMMENT     B   COMMENT ON COLUMN public.dataimport.shinsaymad IS 'R¸N¼ï';
          public          postgres    false    208            O           0    0    COLUMN dataimport.ymad    COMMENT     E   COMMENT ON COLUMN public.dataimport.ymad IS '{pN¼ï_{ÃN';
          public          postgres    false    208            P           0    0    COLUMN dataimport.birthad    COMMENT     ?   COMMENT ON COLUMN public.dataimport.birthad IS '¶Nú¼ï';
          public          postgres    false    208            Q           0    0    COLUMN dataimport.firstdatead    COMMENT     E   COMMENT ON COLUMN public.dataimport.firstdatead IS 'Nú¼ï';
          public          postgres    false    208            R           0    0    COLUMN dataimport.startdatead    COMMENT     I   COMMENT ON COLUMN public.dataimport.startdatead IS '{pJnNú¼ï';
          public          postgres    false    208            S           0    0    COLUMN dataimport.finishdatead    COMMENT     J   COMMENT ON COLUMN public.dataimport.finishdatead IS '{pI¹Nú¼ï';
          public          postgres    false    208            T           0    0     COLUMN dataimport.hihonum_narrow    COMMENT     L   COMMENT ON COLUMN public.dataimport.hihonum_narrow IS 'íÛ¯ÒØÔ¼p';
          public          postgres    false    208            Ç            1259    556364    ocr_ordered    TABLE     >   CREATE TABLE public.ocr_ordered (
    sid integer NOT NULL
);
    DROP TABLE public.ocr_ordered;
       public            postgres    false            È            1259    556367    refrece    TABLE     @  CREATE TABLE public.refrece (
    rrid integer NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    num text DEFAULT ''::text NOT NULL,
    name text DEFAULT ''::text NOT NULL,
    kana text DEFAULT ''::text NOT NULL,
    zip text DEFAULT ''::text NOT NULL,
    add text DEFAULT ''::text NOT NULL,
    destzip text DEFAULT ''::text NOT NULL,
    destadd text DEFAULT ''::text NOT NULL,
    sex integer DEFAULT 0 NOT NULL,
    birth date DEFAULT '0001-01-01'::date NOT NULL,
    drnum text DEFAULT ''::text NOT NULL,
    drname text DEFAULT ''::text NOT NULL,
    clinicnum text DEFAULT ''::text NOT NULL,
    clinicname text DEFAULT ''::text NOT NULL,
    days integer DEFAULT 0 NOT NULL,
    total integer DEFAULT 0 NOT NULL,
    charge integer DEFAULT 0 NOT NULL,
    partial integer DEFAULT 0 NOT NULL,
    apptype integer DEFAULT 0 NOT NULL,
    aid integer DEFAULT 0 NOT NULL,
    insnum text DEFAULT ''::text NOT NULL,
    insname text DEFAULT ''::text NOT NULL,
    distance100 integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.refrece;
       public            postgres    false            U           0    0    COLUMN refrece.distance100    COMMENT     D   COMMENT ON COLUMN public.refrece.distance100 IS 'Ã£km x 100';
          public          postgres    false    200            É            1259    556398    scan    TABLE     è   CREATE TABLE public.scan (
    sid integer NOT NULL,
    scandate date,
    cyear integer,
    cmonth integer,
    note1 text,
    note2 text,
    status integer DEFAULT 0,
    apptype integer DEFAULT 0 NOT NULL,
    cym integer
);
    DROP TABLE public.scan;
       public            postgres    false            Ê            1259    556406    scan_sid_seq    SEQUENCE     u   CREATE SEQUENCE public.scan_sid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.scan_sid_seq;
       public          postgres    false    201            V           0    0    scan_sid_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.scan_sid_seq OWNED BY public.scan.sid;
          public          postgres    false    202            Ë            1259    556408 	   scangroup    TABLE       CREATE TABLE public.scangroup (
    groupid integer NOT NULL,
    status integer,
    scanid integer,
    scandate date,
    scanuser integer,
    checkdate date,
    checkuser integer,
    inquirydate date,
    inquiryuser integer,
    workingusers text DEFAULT ''::text NOT NULL
);
    DROP TABLE public.scangroup;
       public            postgres    false            Ì            1259    556415    shokaiexclude    TABLE     )  CREATE TABLE public.shokaiexclude (
    excludeid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    mediym integer DEFAULT 0 NOT NULL,
    chargeym integer DEFAULT 0 NOT NULL,
    hihonum text DEFAULT ''::text NOT NULL,
    birth date DEFAULT '0001-01-01'::date NOT NULL
);
 !   DROP TABLE public.shokaiexclude;
       public            postgres    false            Í            1259    556427    shokaiimage    TABLE     Í   CREATE TABLE public.shokaiimage (
    imageid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    filename text NOT NULL,
    code text NOT NULL,
    aid integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.shokaiimage;
       public            postgres    false            Î            1259    556436    shokaiimageimport    TABLE     o   CREATE TABLE public.shokaiimageimport (
    importid integer NOT NULL,
    importdate date,
    uid integer
);
 %   DROP TABLE public.shokaiimageimport;
       public            postgres    false            ?           2604    556439    scan sid    DEFAULT     d   ALTER TABLE ONLY public.scan ALTER COLUMN sid SET DEFAULT nextval('public.scan_sid_seq'::regclass);
 7   ALTER TABLE public.scan ALTER COLUMN sid DROP DEFAULT;
       public          postgres    false    202    201            v           2606    556441    appcounter appcounter_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.appcounter
    ADD CONSTRAINT appcounter_pkey PRIMARY KEY (cym);
 D   ALTER TABLE ONLY public.appcounter DROP CONSTRAINT appcounter_pkey;
       public            postgres    false    196                       2606    556443 %   application_aux application_aux_pkey1 
   CONSTRAINT     d   ALTER TABLE ONLY public.application_aux
    ADD CONSTRAINT application_aux_pkey1 PRIMARY KEY (aid);
 O   ALTER TABLE ONLY public.application_aux DROP CONSTRAINT application_aux_pkey1;
       public            postgres    false    198            |           2606    556445    application application_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.application
    ADD CONSTRAINT application_pkey PRIMARY KEY (aid);
 F   ALTER TABLE ONLY public.application DROP CONSTRAINT application_pkey;
       public            postgres    false    197                       2606    556702    dataimport dataimport_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY public.dataimport
    ADD CONSTRAINT dataimport_pkey PRIMARY KEY (f000_importid);
 D   ALTER TABLE ONLY public.dataimport DROP CONSTRAINT dataimport_pkey;
       public            postgres    false    208                       2606    556447    scangroup group_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.scangroup
    ADD CONSTRAINT group_pkey PRIMARY KEY (groupid);
 >   ALTER TABLE ONLY public.scangroup DROP CONSTRAINT group_pkey;
       public            postgres    false    203                       2606    556449    ocr_ordered ocr_ordered_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.ocr_ordered
    ADD CONSTRAINT ocr_ordered_pkey PRIMARY KEY (sid);
 F   ALTER TABLE ONLY public.ocr_ordered DROP CONSTRAINT ocr_ordered_pkey;
       public            postgres    false    199                       2606    556451    refrece refrece_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.refrece
    ADD CONSTRAINT refrece_pkey PRIMARY KEY (rrid);
 >   ALTER TABLE ONLY public.refrece DROP CONSTRAINT refrece_pkey;
       public            postgres    false    200                       2606    556453    scan scan_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY public.scan
    ADD CONSTRAINT scan_pkey PRIMARY KEY (sid);
 8   ALTER TABLE ONLY public.scan DROP CONSTRAINT scan_pkey;
       public            postgres    false    201                       2606    556455 *   shokaiimageimport shokai_image_import_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.shokaiimageimport
    ADD CONSTRAINT shokai_image_import_pkey PRIMARY KEY (importid);
 T   ALTER TABLE ONLY public.shokaiimageimport DROP CONSTRAINT shokai_image_import_pkey;
       public            postgres    false    206                       2606    556457    shokaiimage shokai_image_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.shokaiimage
    ADD CONSTRAINT shokai_image_pkey PRIMARY KEY (imageid);
 G   ALTER TABLE ONLY public.shokaiimage DROP CONSTRAINT shokai_image_pkey;
       public            postgres    false    205                       2606    556459     shokaiexclude shokaiexclude_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY public.shokaiexclude
    ADD CONSTRAINT shokaiexclude_pkey PRIMARY KEY (excludeid);
 J   ALTER TABLE ONLY public.shokaiexclude DROP CONSTRAINT shokaiexclude_pkey;
       public            postgres    false    204            w           1259    556460    application_cym_idx    INDEX     J   CREATE INDEX application_cym_idx ON public.application USING btree (cym);
 '   DROP INDEX public.application_cym_idx;
       public            postgres    false    197            x           1259    556461    application_groupid_idx    INDEX     R   CREATE INDEX application_groupid_idx ON public.application USING btree (groupid);
 +   DROP INDEX public.application_groupid_idx;
       public            postgres    false    197            y           1259    556462    application_hnum_idx    INDEX     L   CREATE INDEX application_hnum_idx ON public.application USING btree (hnum);
 (   DROP INDEX public.application_hnum_idx;
       public            postgres    false    197            z           1259    556463    application_pbirthday_idx    INDEX     V   CREATE INDEX application_pbirthday_idx ON public.application USING btree (pbirthday);
 -   DROP INDEX public.application_pbirthday_idx;
       public            postgres    false    197            }           1259    556464    application_rrid_idx    INDEX     L   CREATE INDEX application_rrid_idx ON public.application USING btree (rrid);
 (   DROP INDEX public.application_rrid_idx;
       public            postgres    false    197            ~           1259    556465    application_shokaicode_idx    INDEX     X   CREATE INDEX application_shokaicode_idx ON public.application USING btree (shokaicode);
 .   DROP INDEX public.application_shokaicode_idx;
       public            postgres    false    197                       1259    556704    idx_dataimport_1    INDEX     N   CREATE INDEX idx_dataimport_1 ON public.dataimport USING btree (f034_comnum);
 $   DROP INDEX public.idx_dataimport_1;
       public            postgres    false    208                       1259    556703    idx_dataimport_2    INDEX     ]   CREATE INDEX idx_dataimport_2 ON public.dataimport USING btree (f001_pageins, f002_pageall);
 $   DROP INDEX public.idx_dataimport_2;
       public            postgres    false    208    208                       1259    556705    idx_dataimport_3    INDEX     V   CREATE INDEX idx_dataimport_3 ON public.dataimport USING btree (cym, hihonum_narrow);
 $   DROP INDEX public.idx_dataimport_3;
       public            postgres    false    208    208                       1259    556466    refrece_num_idx    INDEX     B   CREATE INDEX refrece_num_idx ON public.refrece USING btree (num);
 #   DROP INDEX public.refrece_num_idx;
       public            postgres    false    200                       2620    556467 %   application trigger_appcounter_update    TRIGGER        CREATE TRIGGER trigger_appcounter_update BEFORE INSERT OR DELETE OR UPDATE ON public.application FOR EACH ROW EXECUTE PROCEDURE public.appcounter_update();
 >   DROP TRIGGER trigger_appcounter_update ON public.application;
       public          postgres    false    210    197           