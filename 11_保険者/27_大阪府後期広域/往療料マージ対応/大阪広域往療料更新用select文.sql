select aid,fdistance,fvisitadd,d.until4km, d.over4km,
case 
	when trim(until4km) = '' and trim(over4km) ='' then 0
	when trim(until4km) = '' and trim(over4km) <>'' then 999 
	when trim(until4km) <>'' and trim(over4km) <>'' then 999
	when trim(until4km) <>'' and trim(over4km) ='' then 999
	else 0
end fdistance,
case 
	when trim(until4km) = '' and trim(over4km) ='' then 0
	when trim(until4km) = '' and trim(over4km) <>'' then 999 
	when trim(until4km) <>'' and trim(over4km) <>'' then 999
	when trim(until4km) <>'' and trim(over4km) ='' then 0
	else 0
end fvisitadd 

from application a
inner join  tdata_ahk d 
on
d.detailnum2 = replace(a.emptytext3,'.tif','') 
 and  a.cym=202106
 

order by a.aid