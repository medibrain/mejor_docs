-- FUNCTION: public.upd(integer)

-- DROP FUNCTION public.upd(integer);

CREATE OR REPLACE FUNCTION public.upd(
	incym integer)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare 
_incym integer :=incym;				--処理年月
res integer :=0;					--処理行数

begin


	update application set 
	
	psex		=	cast(tsex as integer) 				--性別
	,pbirthday	=	cast(tbirthad as date) 				--生年月日

	,ayear	=	cast(substr(tyearmonth,2,2) as int)		--施術年
	,amonth	=	cast(substr(tyearmonth,4,2) as int)		--施術月
	,atotal	=	cast(iAmountValue as int)				--合計金額
	,hnum	=	tinsurednumber							--被保険者番号

--20200218162513 furukawa st ////////////////////////
--data.mdb等の付随情報を取り込んだらtaggeddatasに文字列を追加
	,taggeddatas = taggeddatas || 'GeneralString1:"データ取得済"'
--20200218162513 furukawa ed ////////////////////////


	from tdata 
	where 
	emptytext3 <>'' and 		--広域から貰ったときのファイル名はemptytext3に入れてある


--//20200207100744 furukawa st ////////////////////////
--//ファイル名の検索条件をフルパスからファイル名のみにする。フルパスにするとファイル名がとりづらく、条件が反映できないことがある
	 --ファイル名が合致するレコードを更新
	emptytext3	=	split_part(timagefilename,'\',3)
	--split_part(emptytext3, '\',13)=split_part(timagefilename,'\',3)
--//20200207100744 furukawa ed ////////////////////////

	
	--処理年月
	and cym=_incym;

	--一応行数を返したいが、受け取り方法知らん
	if found then
		GET DIAGNOSTICS res=ROW_COUNT;
		return res;
	else
		return -1;
	end if;



end;
$BODY$;

ALTER FUNCTION public.upd(integer)
    OWNER TO postgres;
