﻿--2020/01/27  広域からのdata.mdbをapplicaitonに適用するためのストアド
--2020/01/30  ４つの項目を追加

CREATE or replace function upd(IN incym INT) returns integer as $$

declare 
_incym integer :=incym;
res integer :=0;

begin


	update application set 
	
	 psex=cast(tsex as integer), 
	 pbirthday=cast(tbirthad as date) 


--2020/01/30  ４つの項目を追加
	ayear	=	cast(substr(tyearmonth,2,2) as int),		--施術年
	amonth	=	cast(substr(tyearmonth,4,2) as int),		--施術月
	atotal	=	cast(iAmountValue as int),			--合計金額
	hnum	=	tinsurednumber					--被保険者番号
		
	 
	from tdata 
	 where 

	 --広域から貰ったときのファイル名はemptytext3に入れてある
	 emptytext3 <>'' and 

	 --ファイル名が合致するレコードを更新
	split_part(emptytext3, '\',13)=split_part(timagefilename,'\',3)
	
	--処理年月
	and cym=_incym;

	if found then
		GET DIAGNOSTICS res=ROW_COUNT;
		return res;
	else
		return -1;
	end if;



end;
$$ language plpgsql;