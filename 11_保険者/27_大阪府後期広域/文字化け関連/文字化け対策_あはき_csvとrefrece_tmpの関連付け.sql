﻿
select 
i.f10,r.drname,--drname
i.f03,r.name,--name
i.f28,r.clinicname,--clinicname
i.f16,r.kana,--kana
replace(trim(i.f18) || trim(i.f19) || trim(i.f20),'　','') as add,r.add,--add
replace(i.f00,'　','') as insname,r.insname, --insname
trim(i.f26) as destname,r.destname,--destname
trim(i.f27) as destkana,r.destkana ,--destkana
replace(trim(i.f23),'　','') || '　' || replace(trim(i.f24),'　','')  || '　' || trim(i.f25) as destadd,r.destadd, --destadd


trim(i.f09) as clinicnum ,r.clinicnum --clinicnum
,i.f15 as searchnum,r.searchnum --searchnum
,cast(substr(i.f11,1,2) as integer)+1988 || substr(i.f11,3)  as startdate,
	replace(cast(r.startdate as varchar),'-','') as startdate--startdate

from csvimp_ahk i 
left join 
refrece_tmp r on 

i.f01 = r.insnum --保険者番号
and i.f02 = r.num --被保険者番号
and cast(i.f12 as integer)=r.days --日数
and cast(i.f13 as integer)= r.total --合計金額
and cast(i.f14 as integer)= r.charge --支給金額 37015

and trim(i.f22)=r.destzip	--送付先郵便番号 37015
and trim(i.f09) = r.clinicnum	--施術所番号 36921
and cast(substr(i.f11,1,2) as integer)+1988 || substr(i.f11,3) = replace(cast(r.startdate as varchar),'-','')--施術開始年月日　35917


--and i.f10=r.groupcode	--グループ番号

 
where r.cym='201901'
--and trim(cast(destzip as varchar))<>''

--limit 10