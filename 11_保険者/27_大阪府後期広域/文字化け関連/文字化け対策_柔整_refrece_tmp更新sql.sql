﻿--jyusei update
update refrece_tmp r set


clinicname=i.f11,
kana=i.f16,
add=replace(trim(i.f18) || trim(i.f19) || trim(i.f20),'　','') ,
insname=replace(i.f00,'　','') ,
destname=trim(i.f26) ,
destkana=trim(i.f27),
destadd=replace(trim(i.f23),'　','') || '　' || replace(trim(i.f24),'　','')  || '　' || trim(i.f25) 

from csvimp i 

where

i.f01 = r.insnum --保険者番号
and i.f02 = r.num --被保険者番号
and cast(i.f13 as integer)=r.days --日数
and cast(i.f14 as integer)= r.total --合計金額
and cast(i.f15 as integer)= r.charge --支給金額

and trim(i.f22)=r.destzip	--送付先郵便番号 95701
and (trim(i.f08) || '7' || trim(i.f09)) = r.clinicnum	--施術所番号 95669rec
and cast(substr(i.f12,1,2) as integer)+1988 || substr(i.f12,3) = replace(cast(r.startdate as varchar),'-','')--施術開始年月日　95507rec

