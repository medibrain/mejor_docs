必要な改修
	画像登録時、もとのファイル名をnumberingにいれる　2020/02/27実装済
	入力画面
		入力項目デザイン
		setapp
		getapp
	インポートデータ用テーブル作成		2020/02/27試験DBに作成
	インポート機能作成　2020/02/27実装済み
	出力用テーブル=インポートデータ用テーブル	2020/02/27試験DBに作成
	出力画面 2020年2月28日設計開始 2020/03/02実装完了
	出力ファイル　2020年3月3日実装完了
	結合試験　2020年3月4日予定　まずdb全部消してから

処理フロー
	１．画像ファイル名リストを作ってもらう
	２．画像登録
	３．メホールインポート
	４．入力作業。Applicationテーブルにユニークキー以下、追記必要なデータのフィールドを追加
	５．リスト作成画面からエクスポート
	




５．エクスポート

1.提供データのexcelをexportdataテーブルに取り込み、下記SQLでmedi_keyとapplication.numbering で結合し更新

update exportdata set

medi_fusho1		=	a.iname1				 ,
medi_shoken1	=	a.ifirstdate1            ,
medi_days1		=	a.idays1                 ,
medi_tenki1		=	a.icourse1               ,
medi_fusho2		=	a.iname2                 ,
medi_shoken2	=	a.ifirstdate2            ,
medi_days2		=	a.idays2                 ,
medi_tenki2		=	a.icourse2               ,
medi_fusho3		=	a.iname3                 ,
medi_shoken3	=	a.ifirstdate3            ,
medi_days3		=	a.idays3                 ,
medi_tenki3		=	a.icourse3               ,
medi_fusho4		=	a.iname4                 ,
medi_shoken4	=	a.ifirstdate4            ,
medi_days4		=	a.idays4                 ,
medi_tenki4		=	a.icourse4               ,
medi_fusho5		=	a.iname5                 ,
medi_shoken5	=	a.ifirstdate5            ,
medi_days5		=	a.idays5                 ,
medi_tenki5		=	a.icourse5               ,

from application a
where exportdata.medi_key=a.numbering

2.exportdataのレコードをすべてexcel出力する
