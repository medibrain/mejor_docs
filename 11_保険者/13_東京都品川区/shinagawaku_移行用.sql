PGDMP     -        
            y            shinagawaku    11.10    11.7 b    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    415297    shinagawaku    DATABASE     }   CREATE DATABASE shinagawaku WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
    DROP DATABASE shinagawaku;
             postgres    false            �           0    0    SCHEMA public    ACL     �   REVOKE ALL ON SCHEMA public FROM rdsadmin;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  postgres    false    3            �            1255    415298    addmonth(integer, integer)    FUNCTION     �  CREATE FUNCTION public.addmonth(ym integer, addm integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    res integer := ym;
    ay integer := addm/12;
    am integer := addm%12;
    m integer :=ym%100;
BEGIN
    res=res+(ay*100);
    res=res+am;
    IF 12<m+am THEN
        res=res+88;
    ELSIF m+am<1 THEN
        res=res-88;
    END IF;
    RETURN res;
END;
$$;
 9   DROP FUNCTION public.addmonth(ym integer, addm integer);
       public       postgres    false            �            1255    415299    appcounter_update()    FUNCTION     �  CREATE FUNCTION public.appcounter_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'INSERT') THEN
  IF (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
    UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
  ELSE
    INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
  END IF;
  RETURN NEW;
ELSIF (TG_OP = 'UPDATE') THEN
  IF NEW.cym=OLD.cym THEN
    RETURN NEW;
  ELSE
    UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
    SELECT cym FROM appcounter WHERE cym=NEW.cym;
    IF  (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
      UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
    ELSE
      INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
    END IF;
  END IF;
ELSIF (TG_OP = 'DELETE') THEN
  UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
  RETURN OLD;
END IF;
END;
$$;
 *   DROP FUNCTION public.appcounter_update();
       public       postgres    false            �            1259    415300 
   appcounter    TABLE     e   CREATE TABLE public.appcounter (
    cym integer NOT NULL,
    counter integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.appcounter;
       public         postgres    false            �            1259    415304    application    TABLE     �  CREATE TABLE public.application (
    aid integer NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    ayear integer DEFAULT 0 NOT NULL,
    amonth integer DEFAULT 0 NOT NULL,
    inum text DEFAULT ''::text NOT NULL,
    hnum text DEFAULT ''::text NOT NULL,
    hpref integer DEFAULT 0 NOT NULL,
    htype integer DEFAULT 0 NOT NULL,
    hname text DEFAULT ''::text NOT NULL,
    hzip text DEFAULT ''::text NOT NULL,
    haddress text DEFAULT ''::text NOT NULL,
    pname text DEFAULT ''::text NOT NULL,
    psex integer DEFAULT 0 NOT NULL,
    pbirthday date DEFAULT '0001-01-01'::date NOT NULL,
    asingle integer DEFAULT 0 NOT NULL,
    afamily integer DEFAULT 0 NOT NULL,
    aratio integer DEFAULT 0 NOT NULL,
    publcexpense text DEFAULT ''::text NOT NULL,
    emptytext1 text DEFAULT ''::text NOT NULL,
    emptyint1 integer DEFAULT 0 NOT NULL,
    emptytext2 text DEFAULT ''::text NOT NULL,
    ainspectdate date DEFAULT '0001-01-01'::date NOT NULL,
    emptyint2 integer DEFAULT 0 NOT NULL,
    emptyint3 integer DEFAULT 0 NOT NULL,
    aimagefile text DEFAULT ''::text NOT NULL,
    emptytext3 text DEFAULT ''::text NOT NULL,
    achargeyear integer DEFAULT 0 NOT NULL,
    achargemonth integer DEFAULT 0 NOT NULL,
    iname1 text DEFAULT ''::text NOT NULL,
    idate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    idays1 integer DEFAULT 0 NOT NULL,
    icourse1 integer DEFAULT 0 NOT NULL,
    ifee1 integer DEFAULT 0 NOT NULL,
    iname2 text DEFAULT ''::text NOT NULL,
    idate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    idays2 integer DEFAULT 0 NOT NULL,
    icourse2 integer DEFAULT 0 NOT NULL,
    ifee2 integer DEFAULT 0 NOT NULL,
    iname3 text DEFAULT ''::text NOT NULL,
    idate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    idays3 integer DEFAULT 0 NOT NULL,
    icourse3 integer DEFAULT 0 NOT NULL,
    ifee3 integer DEFAULT 0 NOT NULL,
    iname4 text DEFAULT ''::text NOT NULL,
    idate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    idays4 integer DEFAULT 0 NOT NULL,
    icourse4 integer DEFAULT 0 NOT NULL,
    ifee4 integer DEFAULT 0 NOT NULL,
    iname5 text DEFAULT ''::text NOT NULL,
    idate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    idays5 integer DEFAULT 0 NOT NULL,
    icourse5 integer DEFAULT 0 NOT NULL,
    ifee5 integer DEFAULT 0 NOT NULL,
    fchargetype integer DEFAULT 0 NOT NULL,
    fdistance integer DEFAULT 0 NOT NULL,
    fvisittimes integer DEFAULT 0 NOT NULL,
    fvisitfee integer DEFAULT 0 NOT NULL,
    fvisitadd integer DEFAULT 0 NOT NULL,
    sid text DEFAULT ''::text NOT NULL,
    sregnumber text DEFAULT ''::text NOT NULL,
    szip text DEFAULT ''::text NOT NULL,
    saddress text DEFAULT ''::text NOT NULL,
    sname text DEFAULT ''::text NOT NULL,
    stel text DEFAULT ''::text NOT NULL,
    sdoctor text DEFAULT ''::text NOT NULL,
    skana text DEFAULT ''::text NOT NULL,
    bacctype integer DEFAULT 0 NOT NULL,
    bname text DEFAULT ''::text NOT NULL,
    btype integer DEFAULT 0 NOT NULL,
    bbranch text DEFAULT ''::text NOT NULL,
    bbranchtype integer DEFAULT 0 NOT NULL,
    baccname text DEFAULT ''::text NOT NULL,
    bkana text DEFAULT ''::text NOT NULL,
    baccnumber text DEFAULT ''::text NOT NULL,
    atotal integer DEFAULT 0 NOT NULL,
    apartial integer DEFAULT 0 NOT NULL,
    acharge integer DEFAULT 0 NOT NULL,
    acounteddays integer DEFAULT 0 NOT NULL,
    numbering text DEFAULT ''::text NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    ufirst integer DEFAULT 0 NOT NULL,
    usecond integer DEFAULT 0 NOT NULL,
    uinquiry integer DEFAULT 0 NOT NULL,
    bui integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    statusflags integer DEFAULT 0 NOT NULL,
    shokaireason integer DEFAULT 0 NOT NULL,
    rrid integer DEFAULT 0 NOT NULL,
    inspectreasons integer DEFAULT 0 NOT NULL,
    additionaluid1 integer DEFAULT 0 NOT NULL,
    additionaluid2 integer DEFAULT 0 NOT NULL,
    memo_shokai text DEFAULT ''::text NOT NULL,
    memo_inspect text DEFAULT ''::text NOT NULL,
    memo text DEFAULT ''::text NOT NULL,
    paycode text DEFAULT ''::text NOT NULL,
    shokaicode text DEFAULT ''::text NOT NULL,
    ocrdata text DEFAULT ''::text NOT NULL,
    ufirstex integer DEFAULT 0 NOT NULL,
    usecondex integer DEFAULT 0 NOT NULL,
    kagoreasons integer DEFAULT 0 NOT NULL,
    saishinsareasons integer DEFAULT 0 NOT NULL,
    henreireasons integer DEFAULT 0 NOT NULL,
    taggeddatas text DEFAULT ''::text NOT NULL,
    comnum text DEFAULT ''::text NOT NULL,
    groupnum text DEFAULT ''::text NOT NULL,
    outmemo text DEFAULT ''::text NOT NULL,
    kagoreasons_xml xml
);
    DROP TABLE public.application;
       public         postgres    false            �            1259    416158 
   kokuhodata    TABLE     <  CREATE TABLE public.kokuhodata (
    f001_hihonum character varying DEFAULT ''::character varying NOT NULL,
    f002_pname character varying DEFAULT ''::character varying NOT NULL,
    f003_shinryoym character varying DEFAULT ''::character varying NOT NULL,
    f004_sid character varying DEFAULT ''::character varying NOT NULL,
    f005_sname character varying DEFAULT ''::character varying NOT NULL,
    f006_setainum character varying DEFAULT ''::character varying NOT NULL,
    f007_atenanum character varying DEFAULT ''::character varying NOT NULL,
    f008_pbirthday character varying DEFAULT ''::character varying NOT NULL,
    f009_pgender character varying DEFAULT ''::character varying NOT NULL,
    f010_ratio character varying DEFAULT ''::character varying NOT NULL,
    f011_counteddays character varying DEFAULT ''::character varying NOT NULL,
    f012_total character varying DEFAULT ''::character varying NOT NULL,
    f013_score_kbn character varying DEFAULT ''::character varying NOT NULL,
    f014_honke character varying DEFAULT ''::character varying NOT NULL,
    f015_nyugai character varying DEFAULT ''::character varying NOT NULL,
    f016_hihomark character varying DEFAULT ''::character varying NOT NULL,
    f017 character varying DEFAULT ''::character varying NOT NULL,
    f018 character varying DEFAULT ''::character varying NOT NULL,
    f019 character varying DEFAULT ''::character varying NOT NULL,
    f020_shinsaym character varying DEFAULT ''::character varying NOT NULL,
    f021_insnum character varying DEFAULT ''::character varying NOT NULL,
    f022_comnum character varying DEFAULT ''::character varying NOT NULL,
    f023_dcode character varying DEFAULT ''::character varying NOT NULL,
    f024_dname character varying DEFAULT ''::character varying NOT NULL,
    f025 character varying DEFAULT ''::character varying NOT NULL,
    f026 character varying DEFAULT ''::character varying NOT NULL,
    f027 character varying DEFAULT ''::character varying NOT NULL,
    f028_newest character varying DEFAULT ''::character varying NOT NULL,
    f029 character varying DEFAULT ''::character varying NOT NULL,
    f030_prefcorss character varying DEFAULT ''::character varying NOT NULL,
    f031 character varying DEFAULT ''::character varying NOT NULL,
    f032 character varying DEFAULT ''::character varying NOT NULL,
    f033 character varying DEFAULT ''::character varying NOT NULL,
    f034 character varying DEFAULT ''::character varying NOT NULL,
    f035_kyufu_limit character varying DEFAULT ''::character varying NOT NULL,
    f036_expensive character varying DEFAULT ''::character varying NOT NULL,
    f037 character varying DEFAULT ''::character varying NOT NULL,
    f038 character varying DEFAULT ''::character varying NOT NULL,
    f039 character varying DEFAULT ''::character varying NOT NULL,
    f040 character varying DEFAULT ''::character varying NOT NULL,
    f041 character varying DEFAULT ''::character varying NOT NULL,
    f042 character varying DEFAULT ''::character varying NOT NULL,
    f043 character varying DEFAULT ''::character varying NOT NULL,
    f044_fusen01 character varying DEFAULT ''::character varying NOT NULL,
    f045_fusen02 character varying DEFAULT ''::character varying NOT NULL,
    f046_fusen03 character varying DEFAULT ''::character varying NOT NULL,
    f047_fusen04 character varying DEFAULT ''::character varying NOT NULL,
    f048_fusen05 character varying DEFAULT ''::character varying NOT NULL,
    f049_fusen06 character varying DEFAULT ''::character varying NOT NULL,
    f050_fusen07 character varying DEFAULT ''::character varying NOT NULL,
    f051_fusen08 character varying DEFAULT ''::character varying NOT NULL,
    f052_fusen09 character varying DEFAULT ''::character varying NOT NULL,
    f053_fusen10 character varying DEFAULT ''::character varying NOT NULL,
    f054_fusen11 character varying DEFAULT ''::character varying NOT NULL,
    f055_fusen12 character varying DEFAULT ''::character varying NOT NULL,
    f056_fusen13 character varying DEFAULT ''::character varying NOT NULL,
    f057_fusen14 character varying DEFAULT ''::character varying NOT NULL,
    hihonum_mark character varying DEFAULT ''::character varying NOT NULL,
    shinryoymad integer DEFAULT 0 NOT NULL,
    shinsaymad integer DEFAULT 0 NOT NULL,
    birthdayad date DEFAULT '0001-01-01'::date NOT NULL,
    cym integer DEFAULT 0
);
    DROP TABLE public.kokuhodata;
       public         postgres    false            �           0    0    COLUMN kokuhodata.f001_hihonum    COMMENT     P   COMMENT ON COLUMN public.kokuhodata.f001_hihonum IS '証番号　全角想定';
            public       postgres    false    205            �           0    0    COLUMN kokuhodata.f002_pname    COMMENT     <   COMMENT ON COLUMN public.kokuhodata.f002_pname IS '氏名';
            public       postgres    false    205            �           0    0     COLUMN kokuhodata.f003_shinryoym    COMMENT     S   COMMENT ON COLUMN public.kokuhodata.f003_shinryoym IS '診療年月 和暦想定';
            public       postgres    false    205            �           0    0    COLUMN kokuhodata.f004_sid    COMMENT     C   COMMENT ON COLUMN public.kokuhodata.f004_sid IS '機関コード';
            public       postgres    false    205            �           0    0    COLUMN kokuhodata.f005_sname    COMMENT     E   COMMENT ON COLUMN public.kokuhodata.f005_sname IS '医療機関名';
            public       postgres    false    205            �           0    0    COLUMN kokuhodata.f006_setainum    COMMENT     E   COMMENT ON COLUMN public.kokuhodata.f006_setainum IS '世帯番号';
            public       postgres    false    205            �           0    0    COLUMN kokuhodata.f007_atenanum    COMMENT     E   COMMENT ON COLUMN public.kokuhodata.f007_atenanum IS '宛名番号';
            public       postgres    false    205            �           0    0     COLUMN kokuhodata.f008_pbirthday    COMMENT     S   COMMENT ON COLUMN public.kokuhodata.f008_pbirthday IS '生年月日 和暦想定';
            public       postgres    false    205            �           0    0    COLUMN kokuhodata.f009_pgender    COMMENT     >   COMMENT ON COLUMN public.kokuhodata.f009_pgender IS '性別';
            public       postgres    false    205            �           0    0    COLUMN kokuhodata.f010_ratio    COMMENT     B   COMMENT ON COLUMN public.kokuhodata.f010_ratio IS '給付割合';
            public       postgres    false    205            �           0    0 "   COLUMN kokuhodata.f011_counteddays    COMMENT     E   COMMENT ON COLUMN public.kokuhodata.f011_counteddays IS '実日数';
            public       postgres    false    205            �           0    0    COLUMN kokuhodata.f012_total    COMMENT     B   COMMENT ON COLUMN public.kokuhodata.f012_total IS '決定点数';
            public       postgres    false    205            �           0    0     COLUMN kokuhodata.f013_score_kbn    COMMENT     C   COMMENT ON COLUMN public.kokuhodata.f013_score_kbn IS '点数表';
            public       postgres    false    205            �           0    0    COLUMN kokuhodata.f014_honke    COMMENT     <   COMMENT ON COLUMN public.kokuhodata.f014_honke IS '本家';
            public       postgres    false    205            �           0    0    COLUMN kokuhodata.f015_nyugai    COMMENT     =   COMMENT ON COLUMN public.kokuhodata.f015_nyugai IS '入外';
            public       postgres    false    205            �           0    0    COLUMN kokuhodata.f016_hihomark    COMMENT     B   COMMENT ON COLUMN public.kokuhodata.f016_hihomark IS '証記号';
            public       postgres    false    205            �           0    0    COLUMN kokuhodata.f017    COMMENT     9   COMMENT ON COLUMN public.kokuhodata.f017 IS '種別１';
            public       postgres    false    205            �           0    0    COLUMN kokuhodata.f018    COMMENT     9   COMMENT ON COLUMN public.kokuhodata.f018 IS '種別２';
            public       postgres    false    205            �           0    0    COLUMN kokuhodata.f019    COMMENT     E   COMMENT ON COLUMN public.kokuhodata.f019 IS '診療・療養費別';
            public       postgres    false    205            �           0    0    COLUMN kokuhodata.f020_shinsaym    COMMENT     E   COMMENT ON COLUMN public.kokuhodata.f020_shinsaym IS '審査年月';
            public       postgres    false    205            �           0    0    COLUMN kokuhodata.f021_insnum    COMMENT     F   COMMENT ON COLUMN public.kokuhodata.f021_insnum IS '保険者番号';
            public       postgres    false    205            �           0    0    COLUMN kokuhodata.f022_comnum    COMMENT     U   COMMENT ON COLUMN public.kokuhodata.f022_comnum IS 'レセプト全国共通キー';
            public       postgres    false    205                        0    0    COLUMN kokuhodata.f023_dcode    COMMENT     K   COMMENT ON COLUMN public.kokuhodata.f023_dcode IS '処方機関コード';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f024_dname    COMMENT     E   COMMENT ON COLUMN public.kokuhodata.f024_dname IS '処方機関名';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f025    COMMENT     6   COMMENT ON COLUMN public.kokuhodata.f025 IS 'クリ';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f026    COMMENT     6   COMMENT ON COLUMN public.kokuhodata.f026 IS 'ケア';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f027    COMMENT     6   COMMENT ON COLUMN public.kokuhodata.f027 IS '容認';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f028_newest    COMMENT     C   COMMENT ON COLUMN public.kokuhodata.f028_newest IS '最新履歴';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f029    COMMENT     <   COMMENT ON COLUMN public.kokuhodata.f029 IS '原本所在';
            public       postgres    false    205                       0    0     COLUMN kokuhodata.f030_prefcorss    COMMENT     C   COMMENT ON COLUMN public.kokuhodata.f030_prefcorss IS '県内外';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f031    COMMENT     ?   COMMENT ON COLUMN public.kokuhodata.f031 IS '療養費種別';
            public       postgres    false    205            	           0    0    COLUMN kokuhodata.f032    COMMENT     ?   COMMENT ON COLUMN public.kokuhodata.f032 IS '食事基準額';
            public       postgres    false    205            
           0    0    COLUMN kokuhodata.f033    COMMENT     6   COMMENT ON COLUMN public.kokuhodata.f033 IS '不当';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f034    COMMENT     9   COMMENT ON COLUMN public.kokuhodata.f034 IS '第三者';
            public       postgres    false    205                       0    0 "   COLUMN kokuhodata.f035_kyufu_limit    COMMENT     H   COMMENT ON COLUMN public.kokuhodata.f035_kyufu_limit IS '給付制限';
            public       postgres    false    205                       0    0     COLUMN kokuhodata.f036_expensive    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f036_expensive IS '高額';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f037    COMMENT     6   COMMENT ON COLUMN public.kokuhodata.f037 IS '予約';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f038    COMMENT     6   COMMENT ON COLUMN public.kokuhodata.f038 IS '処理';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f039    COMMENT     <   COMMENT ON COLUMN public.kokuhodata.f039 IS '疑義種別';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f040    COMMENT     6   COMMENT ON COLUMN public.kokuhodata.f040 IS '参考';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f041    COMMENT     6   COMMENT ON COLUMN public.kokuhodata.f041 IS '状態';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f042    COMMENT     ?   COMMENT ON COLUMN public.kokuhodata.f042 IS 'コード情報';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f043    COMMENT     <   COMMENT ON COLUMN public.kokuhodata.f043 IS '原本区分';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f044_fusen01    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f044_fusen01 IS '付箋01';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f045_fusen02    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f045_fusen02 IS '付箋02';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f046_fusen03    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f046_fusen03 IS '付箋03';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f047_fusen04    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f047_fusen04 IS '付箋04';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f048_fusen05    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f048_fusen05 IS '付箋05';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f049_fusen06    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f049_fusen06 IS '付箋06';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f050_fusen07    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f050_fusen07 IS '付箋07';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f051_fusen08    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f051_fusen08 IS '付箋08';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f052_fusen09    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f052_fusen09 IS '付箋09';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f053_fusen10    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f053_fusen10 IS '付箋10';
            public       postgres    false    205                       0    0    COLUMN kokuhodata.f054_fusen11    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f054_fusen11 IS '付箋11';
            public       postgres    false    205                        0    0    COLUMN kokuhodata.f055_fusen12    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f055_fusen12 IS '付箋12';
            public       postgres    false    205            !           0    0    COLUMN kokuhodata.f056_fusen13    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f056_fusen13 IS '付箋13';
            public       postgres    false    205            "           0    0    COLUMN kokuhodata.f057_fusen14    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f057_fusen14 IS '付箋14';
            public       postgres    false    205            #           0    0    COLUMN kokuhodata.hihonum_mark    COMMENT     _   COMMENT ON COLUMN public.kokuhodata.hihonum_mark IS '被保険者記号番号メホール用';
            public       postgres    false    205            $           0    0    COLUMN kokuhodata.shinryoymad    COMMENT     X   COMMENT ON COLUMN public.kokuhodata.shinryoymad IS '診療年月西暦メホール用';
            public       postgres    false    205            %           0    0    COLUMN kokuhodata.shinsaymad    COMMENT     W   COMMENT ON COLUMN public.kokuhodata.shinsaymad IS '審査年月西暦メホール用';
            public       postgres    false    205            &           0    0    COLUMN kokuhodata.birthdayad    COMMENT     W   COMMENT ON COLUMN public.kokuhodata.birthdayad IS '生年月日西暦メホール用';
            public       postgres    false    205            '           0    0    COLUMN kokuhodata.cym    COMMENT     G   COMMENT ON COLUMN public.kokuhodata.cym IS 'メホール請求年月';
            public       postgres    false    205            �            1259    415433    refrece    TABLE     �  CREATE TABLE public.refrece (
    numbering text NOT NULL,
    recekey text,
    chargeym integer NOT NULL,
    mediym integer NOT NULL,
    hihonum text,
    sex integer,
    birth date,
    startdate date,
    finishdate date,
    days integer,
    total integer,
    partial integer,
    charge integer,
    ratio integer,
    newconttype integer,
    drnum text,
    drname text
);
    DROP TABLE public.refrece;
       public         postgres    false            �            1259    415439    scan    TABLE     �   CREATE TABLE public.scan (
    sid integer NOT NULL,
    scandate date,
    cyear integer,
    cmonth integer,
    note1 text,
    note2 text,
    status integer DEFAULT 0,
    apptype integer DEFAULT 0 NOT NULL,
    cym integer
);
    DROP TABLE public.scan;
       public         postgres    false            �            1259    415447    scan_sid_seq    SEQUENCE     u   CREATE SEQUENCE public.scan_sid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.scan_sid_seq;
       public       postgres    false    199            (           0    0    scan_sid_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.scan_sid_seq OWNED BY public.scan.sid;
            public       postgres    false    200            �            1259    415449 	   scangroup    TABLE       CREATE TABLE public.scangroup (
    groupid integer NOT NULL,
    status integer,
    scanid integer,
    scandate date,
    scanuser integer,
    checkdate date,
    checkuser integer,
    inquirydate date,
    inquiryuser integer,
    workingusers text DEFAULT ''::text NOT NULL
);
    DROP TABLE public.scangroup;
       public         postgres    false            �            1259    415456    shokaiexclude    TABLE     )  CREATE TABLE public.shokaiexclude (
    excludeid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    mediym integer DEFAULT 0 NOT NULL,
    chargeym integer DEFAULT 0 NOT NULL,
    hihonum text DEFAULT ''::text NOT NULL,
    birth date DEFAULT '0001-01-01'::date NOT NULL
);
 !   DROP TABLE public.shokaiexclude;
       public         postgres    false            �            1259    415468    shokaiimage    TABLE     �   CREATE TABLE public.shokaiimage (
    imageid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    filename text NOT NULL,
    code text NOT NULL,
    aid integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.shokaiimage;
       public         postgres    false            �            1259    415477    shokaiimageimport    TABLE     o   CREATE TABLE public.shokaiimageimport (
    importid integer NOT NULL,
    importdate date,
    uid integer
);
 %   DROP TABLE public.shokaiimageimport;
       public         postgres    false                       2604    415480    scan sid    DEFAULT     d   ALTER TABLE ONLY public.scan ALTER COLUMN sid SET DEFAULT nextval('public.scan_sid_seq'::regclass);
 7   ALTER TABLE public.scan ALTER COLUMN sid DROP DEFAULT;
       public       postgres    false    200    199            Q           2606    415482    appcounter appcounter_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.appcounter
    ADD CONSTRAINT appcounter_pkey PRIMARY KEY (cym);
 D   ALTER TABLE ONLY public.appcounter DROP CONSTRAINT appcounter_pkey;
       public         postgres    false    196            W           2606    415485    application application_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.application
    ADD CONSTRAINT application_pkey PRIMARY KEY (aid);
 F   ALTER TABLE ONLY public.application DROP CONSTRAINT application_pkey;
       public         postgres    false    197            `           2606    415487    scangroup group_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.scangroup
    ADD CONSTRAINT group_pkey PRIMARY KEY (groupid);
 >   ALTER TABLE ONLY public.scangroup DROP CONSTRAINT group_pkey;
       public         postgres    false    201            h           2606    416227    kokuhodata kokuhodata_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.kokuhodata
    ADD CONSTRAINT kokuhodata_pkey PRIMARY KEY (f022_comnum);
 D   ALTER TABLE ONLY public.kokuhodata DROP CONSTRAINT kokuhodata_pkey;
       public         postgres    false    205            \           2606    416251    refrece refrece_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY public.refrece
    ADD CONSTRAINT refrece_pkey PRIMARY KEY (numbering, chargeym);
 >   ALTER TABLE ONLY public.refrece DROP CONSTRAINT refrece_pkey;
       public         postgres    false    198    198            ^           2606    415491    scan scan_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY public.scan
    ADD CONSTRAINT scan_pkey PRIMARY KEY (sid);
 8   ALTER TABLE ONLY public.scan DROP CONSTRAINT scan_pkey;
       public         postgres    false    199            f           2606    415493 *   shokaiimageimport shokai_image_import_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.shokaiimageimport
    ADD CONSTRAINT shokai_image_import_pkey PRIMARY KEY (importid);
 T   ALTER TABLE ONLY public.shokaiimageimport DROP CONSTRAINT shokai_image_import_pkey;
       public         postgres    false    204            d           2606    415495    shokaiimage shokai_image_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.shokaiimage
    ADD CONSTRAINT shokai_image_pkey PRIMARY KEY (imageid);
 G   ALTER TABLE ONLY public.shokaiimage DROP CONSTRAINT shokai_image_pkey;
       public         postgres    false    203            b           2606    415497     shokaiexclude shokaiexclude_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY public.shokaiexclude
    ADD CONSTRAINT shokaiexclude_pkey PRIMARY KEY (excludeid);
 J   ALTER TABLE ONLY public.shokaiexclude DROP CONSTRAINT shokaiexclude_pkey;
       public         postgres    false    202            R           1259    415498    application_cym_idx    INDEX     J   CREATE INDEX application_cym_idx ON public.application USING btree (cym);
 '   DROP INDEX public.application_cym_idx;
       public         postgres    false    197            S           1259    415499    application_groupid_idx    INDEX     R   CREATE INDEX application_groupid_idx ON public.application USING btree (groupid);
 +   DROP INDEX public.application_groupid_idx;
       public         postgres    false    197            T           1259    415500    application_hnum_idx    INDEX     L   CREATE INDEX application_hnum_idx ON public.application USING btree (hnum);
 (   DROP INDEX public.application_hnum_idx;
       public         postgres    false    197            U           1259    415501    application_pbirthday_idx    INDEX     V   CREATE INDEX application_pbirthday_idx ON public.application USING btree (pbirthday);
 -   DROP INDEX public.application_pbirthday_idx;
       public         postgres    false    197            X           1259    415502    application_rrid_idx    INDEX     L   CREATE INDEX application_rrid_idx ON public.application USING btree (rrid);
 (   DROP INDEX public.application_rrid_idx;
       public         postgres    false    197            Y           1259    415503    application_shokaicode_idx    INDEX     X   CREATE INDEX application_shokaicode_idx ON public.application USING btree (shokaicode);
 .   DROP INDEX public.application_shokaicode_idx;
       public         postgres    false    197            Z           1259    415504    uidx_aimagefile    INDEX     T   CREATE UNIQUE INDEX uidx_aimagefile ON public.application USING btree (aimagefile);
 #   DROP INDEX public.uidx_aimagefile;
       public         postgres    false    197            i           2620    415505 %   application trigger_appcounter_update    TRIGGER     �   CREATE TRIGGER trigger_appcounter_update BEFORE INSERT OR DELETE OR UPDATE ON public.application FOR EACH ROW EXECUTE PROCEDURE public.appcounter_update();
 >   DROP TRIGGER trigger_appcounter_update ON public.application;
       public       postgres    false    207    197           