Attribute VB_Name = "Module1"
Option Explicit

Sub SHARP支払計算書マクロ()

'画面処理とイベント処理の停止
Application.ScreenUpdating = False
Application.EnableEvents = False



Dim makuro As Workbook


Dim filesheet As Worksheet
Dim csvsheet As Worksheet
Dim datasheet As Worksheet
Dim CPsheet As Worksheet
Dim syuukeisheet As Worksheet
Dim teisyutusheet As Worksheet
Dim insatusheet As Worksheet

'新規ファイルの作成→支払計算書・データ提出用の新規ファイルになる
Dim newbook As Workbook

'新規ファイルの名前を変更し、それに対してワークブックの変数をいれる。
Dim dateteisyutu As Workbook


Dim folderpath As String
Dim filepath As String
Dim csvpath As String
Dim teisyutu As String
Dim teisyutupath As String


'CSV取り込みのための変数
Dim querytb As QueryTable

Dim torikomi As Workbook


'「csv」シートの最終行を求める。
Dim csvRowCount As Long

'「CP列」シートの最終行を求める。
Dim CPRowCount As Long



Set makuro = ThisWorkbook



Set filesheet = makuro.Worksheets("ファイル指定用")
Set csvsheet = makuro.Worksheets("csv")
Set datasheet = makuro.Worksheets("データ")
Set CPsheet = makuro.Worksheets("CP列")
Set syuukeisheet = makuro.Worksheets("集計")
Set teisyutusheet = makuro.Worksheets("提出用")
Set insatusheet = makuro.Worksheets("印刷用")




'ポップアウトの変数
Dim kakunin As VbMsgBoxResult



'マクロ実行をする前のポップアウト
kakunin = MsgBox("マクロを実行してもよろしいでしょか？", vbYesNo + vbQuestion, "実行確認")

If kakunin = vbYes Then


'CSVファイルが作業フォルダにあるかどうかの確認。無ければエラーメッセージを
'表示させ、Excelにもそのメッセージを記入するようにする。
'あとで、CSVファイルの列数が137列か
'どうかで、判断するIf分岐を作る。



folderpath = filesheet.Range("B5") & "\"

filepath = filesheet.Range("B8")

csvpath = folderpath & filepath

teisyutu = filesheet.Range("B15")

teisyutupath = folderpath & teisyutu



    If Dir(folderpath & filepath) <> "" Then

        filesheet.Range("B12") = ""
        
            
            If InStr(filesheet.Range("B8"), "シャープ健保_UTF8") > 0 Then
            
                   If Dir(teisyutupath) = "" Then
                    
                         filesheet.Range("B12") = ""
                                         
                                                           
                                '「CSV」シートの中身をまず、全て削除する。
                                csvsheet.Cells.Delete
                                
                                
                                
                                
                                'CSVファイルを、「CSV」シートに取り込む。（タブ、カンマ　UTF8 文字列）
                                Workbooks.Open csvpath
                
                                
                                Set querytb = csvsheet.QueryTables.Add(Connection:="TEXT;" & csvpath, _
                                       Destination:=csvsheet.Range("A1"))
                        
                                With querytb
                                    .TextFilePlatform = 65001
                                    .TextFileColumnDataTypes = Array(2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2)
                                    .TextFileParseType = xlDelimited
                                    .TextFileCommaDelimiter = True
                                    .RefreshStyle = xlOverwriteCells
                                    .Refresh
                                    .Delete
                                
                                End With
                                
                                
                                Set torikomi = Workbooks(filepath)
                                
                                
                                torikomi.Activate
                                
                                ActiveWorkbook.Close savechanges:=False
                                         
                                    
                                '「CSV」シートの最終行を求める。
                                csvRowCount = csvsheet.UsedRange.Rows.Count
                
                           
                '                MsgBox csvRowCount
                                
                                 datasheet.Activate
                                 
                                 datasheet.Range("D2:XFD1048576").Clear
                                 
                                 csvsheet.Activate
                                               
                                 csvsheet.Range(Cells(2, 1), Cells(csvRowCount, 137)).Copy
                                 
                                 
                                 datasheet.Activate
                                 
                                 datasheet.Range("D2").PasteSpecial xlPasteValues
                                
                                 
                                 
                                 
                                 CPsheet.Activate
                                 
                                 CPsheet.Range("A2:A1048576").Delete
                                 
                                 datasheet.Activate
                                 datasheet.Range(Cells(2, 94), Cells(csvRowCount, 94)).Copy
                                 
                                 CPsheet.Activate
                                 CPsheet.Range("A2").PasteSpecial xlPasteValues
                                 
                                 '重複の削除
                                 CPsheet.Range("A2:A" & csvRowCount).RemoveDuplicates (Array(1))
                                 
                                 '昇順で並べ替え
                                 CPsheet.Sort.SortFields.Clear
                                 Call CPsheet.Range("A2:A" & csvRowCount).Sort(Key1:=Range("A1"), Order1:=xlAscending)
                
                                 
                                 CPsheet.Activate
                                 
                                 '「CP列」シートの最大行を求める。
                                 CPRowCount = CPsheet.UsedRange.Rows.Count
                                 
                '                 MsgBox CPRowCount
                                 
                                 CPsheet.Range(Cells(2, 1), Cells(CPRowCount, 1)).Copy
                                 
                                 syuukeisheet.Activate
                                 
                                 syuukeisheet.Range("A4").PasteSpecial xlPasteValues
                                 
                                 syuukeisheet.Range(Cells(4, 1), Cells(CPRowCount + 2, 17)).Copy
                                 
                                 insatusheet.Activate
                                 
                                 insatusheet.Range("A3").PasteSpecial xlPasteValues
                                 
                                 syuukeisheet.Activate
                                 
                                 syuukeisheet.Range("C504:Q504").Copy
                                 
                                 insatusheet.Activate
                                 
                                 insatusheet.Range("C503").PasteSpecial xlPasteValues
                                 
                                 
                                 '「印刷用」シートにて余分な行を削除する
                                 insatusheet.Rows(CPRowCount + 2 & ":" & 502).Delete
                                 
                                 
                                 syuukeisheet.Activate
                                 syuukeisheet.Range(Cells(4, 1), Cells(CPRowCount + 2, 14)).Copy
                                 
                                 teisyutusheet.Activate
                                 teisyutusheet.Range("A3").PasteSpecial xlPasteValues
                                 
                                 teisyutusheet.Rows(CPRowCount + 2 & ":" & 502).Delete
        
        
        
                                 '作業フォルダの中に新規ファイルを立ち上げ、名前を出力ファイル名に変える。
                                 Set newbook = Workbooks.Add
                                 
                                 newbook.SaveAs teisyutupath
                                 
                                 Set dateteisyutu = Workbooks(teisyutu)
                                 
                                                                 
                                 
                                 
                                 '「提出用」シートを新規に作成したファイルにシートごとコピーする。
                                 makuro.Activate
                                 
                                 teisyutusheet.Copy before:=dateteisyutu.Sheets(1)
                                 
                                 '不要なシートを削除する。
                                 Application.DisplayAlerts = False
                                 dateteisyutu.Worksheets(2).Delete
                                 Application.DisplayAlerts = True
                                 
                                 dateteisyutu.Worksheets(1).Cells.Font.Name = "MS　Pゴシック"
                                 
                                 
                                 
                                 dateteisyutu.Save

                                 makuro.Save
                                 
                                 
                                                              
                    
                    Else
                        
                        MsgBox "支払計算書・データ提出用ファイルがすでに作業フォルダ内にありますのでマクロの実行を停止します。"
                         
                        filesheet.Range("B12") = "支払計算書・データ提出用ファイルがすでに作業フォルダにあります。"
                         
                        End
                        
                    End If
                         

            
            Else
                
                MsgBox "CSVファイル名に「シャープ健保_UTF8」が含まれていません。"
                
                filesheet.Range("B12") = "CSVのファイル名に「シャープ健保_UTF8」が含まれていませんのでマクロ実行を中止しました。"
                    
                End
                    
            
            End If
            


    Else
        MsgBox "CSVファイルが作業フォルダにありません。マクロの実行を停止します"
        
        
        filesheet.Range("B12") = "CSVファイルが作業フォルダにありません"
        
        End

    
    End If
        
        
'マクロの実行を確認するどうかのポップアウトでキャンセルをした場合
Else
    MsgBox "マクロの実行をキャンセルしました"
    End
    
End If

'画面処理とイベント処理の開始
Application.ScreenUpdating = True
Application.EnableEvents = True




End Sub
