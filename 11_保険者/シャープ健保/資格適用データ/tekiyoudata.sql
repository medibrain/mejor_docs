﻿-- Table: public.tekiyoudata

-- DROP TABLE public.tekiyoudata;

CREATE TABLE public.tekiyoudata
(
  gno text NOT NULL,
  syono text NOT NULL,
  branch text NOT NULL,
  branch2 text NOT NULL,
  famno text NOT NULL,
  hkana text NOT NULL,
  hname text NOT NULL,
  hgender text NOT NULL,
  zokugara text,
  birthg text,
  birthymd text NOT NULL,
  getg text NOT NULL,
  getymd text,
  lossg text,
  lossymd text,
  raito text,
  issuedate text,
  zip text,
  add text,
  birthadymd date,
  getadymd date,
  lossadymd date,
  etc01 text
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tekiyoudata
  OWNER TO postgres;
COMMENT ON TABLE public.tekiyoudata
  IS '保険者からの資格適用データ格納用';

-- Index: public.tekiyoudata_syono_getadymd_idx

-- DROP INDEX public.tekiyoudata_syono_getadymd_idx;

CREATE INDEX tekiyoudata_syono_getadymd_idx
  ON public.tekiyoudata
  USING btree
  (syono COLLATE pg_catalog."default", getadymd);

