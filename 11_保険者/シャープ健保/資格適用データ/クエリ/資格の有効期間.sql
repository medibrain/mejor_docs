﻿-- select * from tekiyoudata 
-- order by hihonum,branch,branch2
-- limit 100

select hihonum,branch,
min(getadymd) as get,
max(
	case lossadymd 
		when '-infinity' then '2099-12-31' 
		else lossadymd end 
) as loss 

from tekiyoudata

--where branch='0'

group by hihonum,branch

order by hihonum,branch

limit 100