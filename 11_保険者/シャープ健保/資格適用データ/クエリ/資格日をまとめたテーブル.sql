﻿drop table tekiyoudata_getloss;
create table tekiyoudata_getloss as 
select split_part(hihonum,'-',2) as SyoNo,branch,branch2,zokugara,
min(getadymd) as get,
max(
	case lossadymd 
		when '-infinity' then '2099-12-31' 
		else lossadymd end 
) as loss 
 
from tekiyoudata
where branch='0'
group by 
split_part(hihonum,'-',2) ,branch,branch2,zokugara