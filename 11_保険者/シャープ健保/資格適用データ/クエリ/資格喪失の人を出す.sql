﻿select 
a.aid
,a.groupid
,a.ayear
,a.amonth
,a.hnum 被保番
,a.pbirthday 生年月日
,case a.psex when '1' then '男' else '女' end 性別
,a.istartdate1 開始
,a.ifinishdate1 終了
,t.gno 事業所No
,t.syono 証番号
 ,t.branch
 ,t.branch2
,t.getadymd 取得日
,max(t.lossadymd) 失効日
 from application a 
 inner join TekiyouData  t

 on a.hnum=t.gno || '-' || t.syono
 and t.branch='0'

 WHERE
 (NOT(A.ISTARTDATE1 BETWEEN T.GETADYMD  AND  T.LOSSADYMD )
 or 
 NOT(A.ifinishdate1 BETWEEN T.GETADYMD  AND  T.LOSSADYMD ))
 AND T.LOSSADYMD<>'-INFINITY'
-- 
 group by
 a.aid
,a.groupid
,a.ayear
,a.amonth
,a.hnum ,a.pbirthday 
,a.psex
,a.istartdate1 
,a.ifinishdate1 
,t.gno 
,t.syono 
 ,t.branch
 ,t.branch2

,t.getadymd 
--,t.lossadymd



order by a.aid,a.hnum,a.ayear,a.amonth,a.istartdate1