PGDMP         
                y         
   kashiwashi    11.10    11.7 >   �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    419048 
   kashiwashi    DATABASE     |   CREATE DATABASE kashiwashi WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
    DROP DATABASE kashiwashi;
             postgres    false            �           0    0    SCHEMA public    ACL     �   REVOKE ALL ON SCHEMA public FROM rdsadmin;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  postgres    false    3            �            1255    419049    addmonth(integer, integer)    FUNCTION     �  CREATE FUNCTION public.addmonth(ym integer, addm integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    res integer := ym;
    ay integer := addm/12;
    am integer := addm%12;
    m integer :=ym%100;
BEGIN
    res=res+(ay*100);
    res=res+am;
    IF 12<m+am THEN
        res=res+88;
    ELSIF m+am<1 THEN
        res=res-88;
    END IF;
    RETURN res;
END;
$$;
 9   DROP FUNCTION public.addmonth(ym integer, addm integer);
       public       postgres    false            �            1255    419050    appcounter_update()    FUNCTION     �  CREATE FUNCTION public.appcounter_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'INSERT') THEN
  IF (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
    UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
  ELSE
    INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
  END IF;
  RETURN NEW;
ELSIF (TG_OP = 'UPDATE') THEN
  IF NEW.cym=OLD.cym THEN
    RETURN NEW;
  ELSE
    UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
    SELECT cym FROM appcounter WHERE cym=NEW.cym;
    IF  (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
      UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
    ELSE
      INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
    END IF;
  END IF;
ELSIF (TG_OP = 'DELETE') THEN
  UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
  RETURN OLD;
END IF;
END;
$$;
 *   DROP FUNCTION public.appcounter_update();
       public       postgres    false            �            1259    419051 
   appcounter    TABLE     e   CREATE TABLE public.appcounter (
    cym integer NOT NULL,
    counter integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.appcounter;
       public         postgres    false            �            1259    419055    application    TABLE     �  CREATE TABLE public.application (
    aid integer NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    ayear integer DEFAULT 0 NOT NULL,
    amonth integer DEFAULT 0 NOT NULL,
    inum text DEFAULT ''::text NOT NULL,
    hnum text DEFAULT ''::text NOT NULL,
    hpref integer DEFAULT 0 NOT NULL,
    htype integer DEFAULT 0 NOT NULL,
    hname text DEFAULT ''::text NOT NULL,
    hzip text DEFAULT ''::text NOT NULL,
    haddress text DEFAULT ''::text NOT NULL,
    pname text DEFAULT ''::text NOT NULL,
    psex integer DEFAULT 0 NOT NULL,
    pbirthday date DEFAULT '0001-01-01'::date NOT NULL,
    asingle integer DEFAULT 0 NOT NULL,
    afamily integer DEFAULT 0 NOT NULL,
    aratio integer DEFAULT 0 NOT NULL,
    publcexpense text DEFAULT ''::text NOT NULL,
    emptytext1 text DEFAULT ''::text NOT NULL,
    emptyint1 integer DEFAULT 0 NOT NULL,
    emptytext2 text DEFAULT ''::text NOT NULL,
    ainspectdate date DEFAULT '0001-01-01'::date NOT NULL,
    emptyint2 integer DEFAULT 0 NOT NULL,
    emptyint3 integer DEFAULT 0 NOT NULL,
    aimagefile text DEFAULT ''::text NOT NULL,
    emptytext3 text DEFAULT ''::text NOT NULL,
    achargeyear integer DEFAULT 0 NOT NULL,
    achargemonth integer DEFAULT 0 NOT NULL,
    iname1 text DEFAULT ''::text NOT NULL,
    idate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    idays1 integer DEFAULT 0 NOT NULL,
    icourse1 integer DEFAULT 0 NOT NULL,
    ifee1 integer DEFAULT 0 NOT NULL,
    iname2 text DEFAULT ''::text NOT NULL,
    idate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    idays2 integer DEFAULT 0 NOT NULL,
    icourse2 integer DEFAULT 0 NOT NULL,
    ifee2 integer DEFAULT 0 NOT NULL,
    iname3 text DEFAULT ''::text NOT NULL,
    idate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    idays3 integer DEFAULT 0 NOT NULL,
    icourse3 integer DEFAULT 0 NOT NULL,
    ifee3 integer DEFAULT 0 NOT NULL,
    iname4 text DEFAULT ''::text NOT NULL,
    idate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    idays4 integer DEFAULT 0 NOT NULL,
    icourse4 integer DEFAULT 0 NOT NULL,
    ifee4 integer DEFAULT 0 NOT NULL,
    iname5 text DEFAULT ''::text NOT NULL,
    idate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    idays5 integer DEFAULT 0 NOT NULL,
    icourse5 integer DEFAULT 0 NOT NULL,
    ifee5 integer DEFAULT 0 NOT NULL,
    fchargetype integer DEFAULT 0 NOT NULL,
    fdistance integer DEFAULT 0 NOT NULL,
    fvisittimes integer DEFAULT 0 NOT NULL,
    fvisitfee integer DEFAULT 0 NOT NULL,
    fvisitadd integer DEFAULT 0 NOT NULL,
    sid text DEFAULT ''::text NOT NULL,
    sregnumber text DEFAULT ''::text NOT NULL,
    szip text DEFAULT ''::text NOT NULL,
    saddress text DEFAULT ''::text NOT NULL,
    sname text DEFAULT ''::text NOT NULL,
    stel text DEFAULT ''::text NOT NULL,
    sdoctor text DEFAULT ''::text NOT NULL,
    skana text DEFAULT ''::text NOT NULL,
    bacctype integer DEFAULT 0 NOT NULL,
    bname text DEFAULT ''::text NOT NULL,
    btype integer DEFAULT 0 NOT NULL,
    bbranch text DEFAULT ''::text NOT NULL,
    bbranchtype integer DEFAULT 0 NOT NULL,
    baccname text DEFAULT ''::text NOT NULL,
    bkana text DEFAULT ''::text NOT NULL,
    baccnumber text DEFAULT ''::text NOT NULL,
    atotal integer DEFAULT 0 NOT NULL,
    apartial integer DEFAULT 0 NOT NULL,
    acharge integer DEFAULT 0 NOT NULL,
    acounteddays integer DEFAULT 0 NOT NULL,
    numbering text DEFAULT ''::text NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    ufirst integer DEFAULT 0 NOT NULL,
    usecond integer DEFAULT 0 NOT NULL,
    uinquiry integer DEFAULT 0 NOT NULL,
    bui integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    statusflags integer DEFAULT 0 NOT NULL,
    shokaireason integer DEFAULT 0 NOT NULL,
    rrid integer DEFAULT 0 NOT NULL,
    inspectreasons integer DEFAULT 0 NOT NULL,
    additionaluid1 integer DEFAULT 0 NOT NULL,
    additionaluid2 integer DEFAULT 0 NOT NULL,
    memo_shokai text DEFAULT ''::text NOT NULL,
    memo_inspect text DEFAULT ''::text NOT NULL,
    memo text DEFAULT ''::text NOT NULL,
    paycode text DEFAULT ''::text NOT NULL,
    shokaicode text DEFAULT ''::text NOT NULL,
    ocrdata text DEFAULT ''::text NOT NULL,
    ufirstex integer DEFAULT 0 NOT NULL,
    usecondex integer DEFAULT 0 NOT NULL,
    kagoreasons integer DEFAULT 0 NOT NULL,
    saishinsareasons integer DEFAULT 0 NOT NULL,
    henreireasons integer DEFAULT 0 NOT NULL,
    taggeddatas text DEFAULT ''::text NOT NULL,
    comnum text DEFAULT ''::text NOT NULL,
    groupnum text DEFAULT ''::text NOT NULL,
    outmemo text DEFAULT ''::text NOT NULL,
    kagoreasons_xml xml
);
    DROP TABLE public.application;
       public         postgres    false            �            1259    419351    export    TABLE     '  CREATE TABLE public.export (
    cym integer DEFAULT 0 NOT NULL,
    f001insnum character varying DEFAULT ''::character varying NOT NULL,
    f002shinsaym character varying DEFAULT ''::character varying NOT NULL,
    f003comnum character varying(20) DEFAULT ''::character varying NOT NULL,
    f004rezenum character varying(20) DEFAULT ''::character varying NOT NULL,
    f005sejutsuym character varying DEFAULT ''::character varying NOT NULL,
    f006 character varying DEFAULT ''::character varying NOT NULL,
    f007 character varying DEFAULT ''::character varying NOT NULL,
    f008family character varying DEFAULT ''::character varying NOT NULL,
    f009hihomark character varying DEFAULT ''::character varying NOT NULL,
    f010hihonum character varying DEFAULT ''::character varying NOT NULL,
    f011pgender character varying DEFAULT ''::character varying NOT NULL,
    f012pbirthday character varying DEFAULT ''::character varying NOT NULL,
    f013ratio character varying DEFAULT ''::character varying NOT NULL,
    f014 character varying DEFAULT ''::character varying NOT NULL,
    f015 character varying DEFAULT ''::character varying NOT NULL,
    f016 character varying DEFAULT ''::character varying NOT NULL,
    f017 character varying DEFAULT ''::character varying NOT NULL,
    f018 character varying DEFAULT ''::character varying NOT NULL,
    f019 character varying DEFAULT ''::character varying NOT NULL,
    f020 character varying DEFAULT ''::character varying NOT NULL,
    f021 character varying DEFAULT ''::character varying NOT NULL,
    f022clinicnum character varying DEFAULT ''::character varying NOT NULL,
    f023 character varying DEFAULT ''::character varying NOT NULL,
    f024 character varying DEFAULT ''::character varying NOT NULL,
    f025total character varying DEFAULT ''::character varying NOT NULL,
    f026partial character varying DEFAULT ''::character varying NOT NULL,
    f027 character varying DEFAULT ''::character varying NOT NULL,
    f028charge character varying DEFAULT ''::character varying NOT NULL,
    f029 character varying DEFAULT ''::character varying NOT NULL,
    f030 character varying DEFAULT ''::character varying NOT NULL,
    f031 character varying DEFAULT ''::character varying NOT NULL,
    f033firstdate1 character varying DEFAULT ''::character varying NOT NULL,
    f034 character varying DEFAULT ''::character varying NOT NULL,
    f035 character varying DEFAULT ''::character varying NOT NULL,
    f036 character varying DEFAULT ''::character varying NOT NULL,
    f037 character varying DEFAULT ''::character varying NOT NULL,
    f038 character varying DEFAULT ''::character varying NOT NULL,
    f039 character varying DEFAULT ''::character varying NOT NULL,
    f040 character varying DEFAULT ''::character varying NOT NULL,
    f041 character varying DEFAULT ''::character varying NOT NULL,
    f042 character varying DEFAULT ''::character varying NOT NULL,
    f043 character varying DEFAULT ''::character varying NOT NULL,
    f044 character varying DEFAULT ''::character varying NOT NULL,
    f045 character varying DEFAULT ''::character varying NOT NULL,
    f046 character varying DEFAULT ''::character varying NOT NULL,
    f047 character varying DEFAULT ''::character varying NOT NULL,
    f048 character varying DEFAULT ''::character varying NOT NULL,
    f049 character varying DEFAULT ''::character varying NOT NULL,
    f050 character varying DEFAULT ''::character varying NOT NULL,
    f051 character varying DEFAULT ''::character varying NOT NULL,
    f052 character varying DEFAULT ''::character varying NOT NULL,
    f053 character varying DEFAULT ''::character varying NOT NULL,
    f054 character varying DEFAULT ''::character varying NOT NULL,
    f055 character varying DEFAULT ''::character varying NOT NULL,
    f056 character varying DEFAULT ''::character varying NOT NULL,
    f057 character varying DEFAULT ''::character varying NOT NULL,
    f058 character varying DEFAULT ''::character varying NOT NULL,
    f059 character varying DEFAULT ''::character varying NOT NULL,
    f060 character varying DEFAULT ''::character varying NOT NULL,
    f061 character varying DEFAULT ''::character varying NOT NULL,
    f062 character varying DEFAULT ''::character varying NOT NULL,
    f063 character varying DEFAULT ''::character varying NOT NULL,
    f064 character varying DEFAULT ''::character varying NOT NULL,
    f065 character varying DEFAULT ''::character varying NOT NULL,
    f066 character varying DEFAULT ''::character varying NOT NULL,
    f067 character varying DEFAULT ''::character varying NOT NULL,
    f068 character varying DEFAULT ''::character varying NOT NULL,
    f069 character varying DEFAULT ''::character varying NOT NULL,
    f070 character varying DEFAULT ''::character varying NOT NULL,
    f071 character varying DEFAULT ''::character varying NOT NULL,
    f072 character varying DEFAULT ''::character varying NOT NULL,
    f073 character varying DEFAULT ''::character varying NOT NULL,
    f074 character varying DEFAULT ''::character varying NOT NULL,
    f075 character varying DEFAULT ''::character varying NOT NULL,
    f076 character varying DEFAULT ''::character varying NOT NULL,
    f077 character varying DEFAULT ''::character varying NOT NULL,
    f078 character varying DEFAULT ''::character varying NOT NULL,
    f079 character varying DEFAULT ''::character varying NOT NULL,
    f080 character varying DEFAULT ''::character varying NOT NULL,
    f081pzip character varying DEFAULT ''::character varying NOT NULL,
    f082paddress character varying DEFAULT ''::character varying NOT NULL,
    f083pnamekanji character varying DEFAULT ''::character varying NOT NULL,
    f100shinsaymad integer DEFAULT 0,
    f101sejutsuymad integer DEFAULT 0,
    f102birthad date DEFAULT '0001-01-01'::date,
    f103firstdatead date DEFAULT '0001-01-01'::date,
    f104seikeishisnaymad1 integer DEFAULT 0,
    f105seikeishisnaymad2 integer DEFAULT 0,
    f106seikeishisnaymad3 integer DEFAULT 0,
    f107hihomark_narrow character varying DEFAULT ''::character varying NOT NULL,
    f108hihonum_narrow character varying DEFAULT ''::character varying NOT NULL
);
    DROP TABLE public.export;
       public         postgres    false            �           0    0    COLUMN export.cym    COMMENT     L   COMMENT ON COLUMN public.export.cym IS 'メホール請求年月管理用';
            public       postgres    false    205            �           0    0    COLUMN export.f001insnum    COMMENT     A   COMMENT ON COLUMN public.export.f001insnum IS '保険者番号';
            public       postgres    false    205            �           0    0    COLUMN export.f002shinsaym    COMMENT     @   COMMENT ON COLUMN public.export.f002shinsaym IS '審査年月';
            public       postgres    false    205            �           0    0    COLUMN export.f003comnum    COMMENT     P   COMMENT ON COLUMN public.export.f003comnum IS 'レセプト全国共通キー';
            public       postgres    false    205            �           0    0    COLUMN export.f004rezenum    COMMENT     N   COMMENT ON COLUMN public.export.f004rezenum IS '国保連レセプト番号';
            public       postgres    false    205            �           0    0    COLUMN export.f005sejutsuym    COMMENT     A   COMMENT ON COLUMN public.export.f005sejutsuym IS '施術年月';
            public       postgres    false    205            �           0    0    COLUMN export.f006    COMMENT     9   COMMENT ON COLUMN public.export.f006 IS '保険種別1';
            public       postgres    false    205            �           0    0    COLUMN export.f007    COMMENT     9   COMMENT ON COLUMN public.export.f007 IS '保険種別2';
            public       postgres    false    205            �           0    0    COLUMN export.f008family    COMMENT     J   COMMENT ON COLUMN public.export.f008family IS '本人家族入外区分';
            public       postgres    false    205            �           0    0    COLUMN export.f009hihomark    COMMENT     I   COMMENT ON COLUMN public.export.f009hihomark IS '被保険者証記号';
            public       postgres    false    205            �           0    0    COLUMN export.f010hihonum    COMMENT     <   COMMENT ON COLUMN public.export.f010hihonum IS '証番号';
            public       postgres    false    205            �           0    0    COLUMN export.f011pgender    COMMENT     9   COMMENT ON COLUMN public.export.f011pgender IS '性別';
            public       postgres    false    205            �           0    0    COLUMN export.f012pbirthday    COMMENT     A   COMMENT ON COLUMN public.export.f012pbirthday IS '生年月日';
            public       postgres    false    205            �           0    0    COLUMN export.f013ratio    COMMENT     =   COMMENT ON COLUMN public.export.f013ratio IS '給付割合';
            public       postgres    false    205            �           0    0    COLUMN export.f014    COMMENT     9   COMMENT ON COLUMN public.export.f014 IS '特記事項1';
            public       postgres    false    205            �           0    0    COLUMN export.f015    COMMENT     9   COMMENT ON COLUMN public.export.f015 IS '特記事項2';
            public       postgres    false    205            �           0    0    COLUMN export.f016    COMMENT     9   COMMENT ON COLUMN public.export.f016 IS '特記事項3';
            public       postgres    false    205            �           0    0    COLUMN export.f017    COMMENT     9   COMMENT ON COLUMN public.export.f017 IS '特記事項4';
            public       postgres    false    205            �           0    0    COLUMN export.f018    COMMENT     9   COMMENT ON COLUMN public.export.f018 IS '特記事項5';
            public       postgres    false    205            �           0    0    COLUMN export.f019    COMMENT     9   COMMENT ON COLUMN public.export.f019 IS '算定区分1';
            public       postgres    false    205                        0    0    COLUMN export.f020    COMMENT     9   COMMENT ON COLUMN public.export.f020 IS '算定区分2';
            public       postgres    false    205                       0    0    COLUMN export.f021    COMMENT     9   COMMENT ON COLUMN public.export.f021 IS '算定区分3';
            public       postgres    false    205                       0    0    COLUMN export.f022clinicnum    COMMENT     G   COMMENT ON COLUMN public.export.f022clinicnum IS '施術所コード';
            public       postgres    false    205                       0    0    COLUMN export.f023    COMMENT     A   COMMENT ON COLUMN public.export.f023 IS '柔整団体コード';
            public       postgres    false    205                       0    0    COLUMN export.f024    COMMENT     2   COMMENT ON COLUMN public.export.f024 IS '回数';
            public       postgres    false    205                       0    0    COLUMN export.f025total    COMMENT     =   COMMENT ON COLUMN public.export.f025total IS '決定金額';
            public       postgres    false    205                       0    0    COLUMN export.f026partial    COMMENT     H   COMMENT ON COLUMN public.export.f026partial IS '決定一部負担金';
            public       postgres    false    205                       0    0    COLUMN export.f027    COMMENT     5   COMMENT ON COLUMN public.export.f027 IS '費用額';
            public       postgres    false    205                       0    0    COLUMN export.f028charge    COMMENT     D   COMMENT ON COLUMN public.export.f028charge IS '保険者負担額';
            public       postgres    false    205            	           0    0    COLUMN export.f029    COMMENT     ;   COMMENT ON COLUMN public.export.f029 IS '高額療養費';
            public       postgres    false    205            
           0    0    COLUMN export.f030    COMMENT     ;   COMMENT ON COLUMN public.export.f030 IS '患者負担額';
            public       postgres    false    205                       0    0    COLUMN export.f031    COMMENT     >   COMMENT ON COLUMN public.export.f031 IS '国保優先公費';
            public       postgres    false    205                       0    0    COLUMN export.f033firstdate1    COMMENT     E   COMMENT ON COLUMN public.export.f033firstdate1 IS '初検年月日';
            public       postgres    false    205                       0    0    COLUMN export.f034    COMMENT     8   COMMENT ON COLUMN public.export.f034 IS '負傷名数';
            public       postgres    false    205                       0    0    COLUMN export.f035    COMMENT     2   COMMENT ON COLUMN public.export.f035 IS '転帰';
            public       postgres    false    205                       0    0    COLUMN export.f036    COMMENT     D   COMMENT ON COLUMN public.export.f036 IS '転帰レコード区分';
            public       postgres    false    205                       0    0    COLUMN export.f037    COMMENT     D   COMMENT ON COLUMN public.export.f037 IS '転帰グループ番号';
            public       postgres    false    205                       0    0    COLUMN export.f038    COMMENT     @   COMMENT ON COLUMN public.export.f038 IS '整形審査年月_1';
            public       postgres    false    205                       0    0    COLUMN export.f039    COMMENT     R   COMMENT ON COLUMN public.export.f039 IS '整形レセプト全国共通キー_1';
            public       postgres    false    205                       0    0    COLUMN export.f040    COMMENT     I   COMMENT ON COLUMN public.export.f040 IS '整形医療機関コード_1';
            public       postgres    false    205                       0    0    COLUMN export.f041    COMMENT     C   COMMENT ON COLUMN public.export.f041 IS '整形医療機関名_1';
            public       postgres    false    205                       0    0    COLUMN export.f042    COMMENT     D   COMMENT ON COLUMN public.export.f042 IS '整形診療開始日1_1';
            public       postgres    false    205                       0    0    COLUMN export.f043    COMMENT     D   COMMENT ON COLUMN public.export.f043 IS '整形診療開始日2_1';
            public       postgres    false    205                       0    0    COLUMN export.f044    COMMENT     D   COMMENT ON COLUMN public.export.f044 IS '整形診療開始日3_1';
            public       postgres    false    205                       0    0    COLUMN export.f045    COMMENT     D   COMMENT ON COLUMN public.export.f045 IS '整形疾病コード1_1';
            public       postgres    false    205                       0    0    COLUMN export.f046    COMMENT     D   COMMENT ON COLUMN public.export.f046 IS '整形疾病コード2_1';
            public       postgres    false    205                       0    0    COLUMN export.f047    COMMENT     D   COMMENT ON COLUMN public.export.f047 IS '整形疾病コード3_1';
            public       postgres    false    205                       0    0    COLUMN export.f048    COMMENT     D   COMMENT ON COLUMN public.export.f048 IS '整形疾病コード4_1';
            public       postgres    false    205                       0    0    COLUMN export.f049    COMMENT     D   COMMENT ON COLUMN public.export.f049 IS '整形疾病コード5_1';
            public       postgres    false    205                       0    0    COLUMN export.f050    COMMENT     @   COMMENT ON COLUMN public.export.f050 IS '整形診療日数_1';
            public       postgres    false    205                       0    0    COLUMN export.f051    COMMENT     =   COMMENT ON COLUMN public.export.f051 IS '整形費用額_1';
            public       postgres    false    205                       0    0    COLUMN export.f052    COMMENT     @   COMMENT ON COLUMN public.export.f052 IS '整形審査年月_2';
            public       postgres    false    205                        0    0    COLUMN export.f053    COMMENT     R   COMMENT ON COLUMN public.export.f053 IS '整形レセプト全国共通キー_2';
            public       postgres    false    205            !           0    0    COLUMN export.f054    COMMENT     I   COMMENT ON COLUMN public.export.f054 IS '整形医療機関コード_2';
            public       postgres    false    205            "           0    0    COLUMN export.f055    COMMENT     C   COMMENT ON COLUMN public.export.f055 IS '整形医療機関名_2';
            public       postgres    false    205            #           0    0    COLUMN export.f056    COMMENT     D   COMMENT ON COLUMN public.export.f056 IS '整形診療開始日1_2';
            public       postgres    false    205            $           0    0    COLUMN export.f057    COMMENT     D   COMMENT ON COLUMN public.export.f057 IS '整形診療開始日2_2';
            public       postgres    false    205            %           0    0    COLUMN export.f058    COMMENT     D   COMMENT ON COLUMN public.export.f058 IS '整形診療開始日3_2';
            public       postgres    false    205            &           0    0    COLUMN export.f059    COMMENT     D   COMMENT ON COLUMN public.export.f059 IS '整形疾病コード1_2';
            public       postgres    false    205            '           0    0    COLUMN export.f060    COMMENT     D   COMMENT ON COLUMN public.export.f060 IS '整形疾病コード2_2';
            public       postgres    false    205            (           0    0    COLUMN export.f061    COMMENT     D   COMMENT ON COLUMN public.export.f061 IS '整形疾病コード3_2';
            public       postgres    false    205            )           0    0    COLUMN export.f062    COMMENT     D   COMMENT ON COLUMN public.export.f062 IS '整形疾病コード4_2';
            public       postgres    false    205            *           0    0    COLUMN export.f063    COMMENT     D   COMMENT ON COLUMN public.export.f063 IS '整形疾病コード5_2';
            public       postgres    false    205            +           0    0    COLUMN export.f064    COMMENT     C   COMMENT ON COLUMN public.export.f064 IS '整形診診療日数_2';
            public       postgres    false    205            ,           0    0    COLUMN export.f065    COMMENT     =   COMMENT ON COLUMN public.export.f065 IS '整形費用額_2';
            public       postgres    false    205            -           0    0    COLUMN export.f066    COMMENT     @   COMMENT ON COLUMN public.export.f066 IS '整形審査年月_3';
            public       postgres    false    205            .           0    0    COLUMN export.f067    COMMENT     R   COMMENT ON COLUMN public.export.f067 IS '整形レセプト全国共通キー_3';
            public       postgres    false    205            /           0    0    COLUMN export.f068    COMMENT     I   COMMENT ON COLUMN public.export.f068 IS '整形医療機関コード_3';
            public       postgres    false    205            0           0    0    COLUMN export.f069    COMMENT     C   COMMENT ON COLUMN public.export.f069 IS '整形医療機関名_3';
            public       postgres    false    205            1           0    0    COLUMN export.f070    COMMENT     D   COMMENT ON COLUMN public.export.f070 IS '整形診療開始日1_3';
            public       postgres    false    205            2           0    0    COLUMN export.f071    COMMENT     D   COMMENT ON COLUMN public.export.f071 IS '整形診療開始日2_3';
            public       postgres    false    205            3           0    0    COLUMN export.f072    COMMENT     D   COMMENT ON COLUMN public.export.f072 IS '整形診療開始日3_3';
            public       postgres    false    205            4           0    0    COLUMN export.f073    COMMENT     D   COMMENT ON COLUMN public.export.f073 IS '整形疾病コード1_3';
            public       postgres    false    205            5           0    0    COLUMN export.f074    COMMENT     D   COMMENT ON COLUMN public.export.f074 IS '整形疾病コード2_3';
            public       postgres    false    205            6           0    0    COLUMN export.f075    COMMENT     D   COMMENT ON COLUMN public.export.f075 IS '整形疾病コード3_3';
            public       postgres    false    205            7           0    0    COLUMN export.f076    COMMENT     D   COMMENT ON COLUMN public.export.f076 IS '整形疾病コード4_3';
            public       postgres    false    205            8           0    0    COLUMN export.f077    COMMENT     D   COMMENT ON COLUMN public.export.f077 IS '整形疾病コード5_3';
            public       postgres    false    205            9           0    0    COLUMN export.f078    COMMENT     @   COMMENT ON COLUMN public.export.f078 IS '整形診療日数_3';
            public       postgres    false    205            :           0    0    COLUMN export.f079    COMMENT     =   COMMENT ON COLUMN public.export.f079 IS '整形費用額_3';
            public       postgres    false    205            ;           0    0    COLUMN export.f080    COMMENT     8   COMMENT ON COLUMN public.export.f080 IS '宛名番号';
            public       postgres    false    205            <           0    0    COLUMN export.f081pzip    COMMENT     <   COMMENT ON COLUMN public.export.f081pzip IS '郵便番号';
            public       postgres    false    205            =           0    0    COLUMN export.f082paddress    COMMENT     :   COMMENT ON COLUMN public.export.f082paddress IS '住所';
            public       postgres    false    205            >           0    0    COLUMN export.f083pnamekanji    COMMENT     B   COMMENT ON COLUMN public.export.f083pnamekanji IS '氏名漢字';
            public       postgres    false    205            ?           0    0    COLUMN export.f100shinsaymad    COMMENT     H   COMMENT ON COLUMN public.export.f100shinsaymad IS '審査年月西暦';
            public       postgres    false    205            @           0    0    COLUMN export.f101sejutsuymad    COMMENT     I   COMMENT ON COLUMN public.export.f101sejutsuymad IS '施術年月西暦';
            public       postgres    false    205            A           0    0    COLUMN export.f102birthad    COMMENT     E   COMMENT ON COLUMN public.export.f102birthad IS '生年月日西暦';
            public       postgres    false    205            B           0    0    COLUMN export.f103firstdatead    COMMENT     L   COMMENT ON COLUMN public.export.f103firstdatead IS '初検年月日西暦';
            public       postgres    false    205            C           0    0 #   COLUMN export.f104seikeishisnaymad1    COMMENT     W   COMMENT ON COLUMN public.export.f104seikeishisnaymad1 IS '整形審査年月_1西暦';
            public       postgres    false    205            D           0    0 #   COLUMN export.f105seikeishisnaymad2    COMMENT     W   COMMENT ON COLUMN public.export.f105seikeishisnaymad2 IS '整形審査年月_2西暦';
            public       postgres    false    205            E           0    0 #   COLUMN export.f106seikeishisnaymad3    COMMENT     W   COMMENT ON COLUMN public.export.f106seikeishisnaymad3 IS '整形審査年月_3西暦';
            public       postgres    false    205            F           0    0 !   COLUMN export.f107hihomark_narrow    COMMENT     V   COMMENT ON COLUMN public.export.f107hihomark_narrow IS '被保険者証記号半角';
            public       postgres    false    205            G           0    0     COLUMN export.f108hihonum_narrow    COMMENT     I   COMMENT ON COLUMN public.export.f108hihonum_narrow IS '証番号半角';
            public       postgres    false    205            �            1259    419449    export_id_seq    SEQUENCE     v   CREATE SEQUENCE public.export_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.export_id_seq;
       public       postgres    false            �            1259    419451    import_id_seq    SEQUENCE     v   CREATE SEQUENCE public.import_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.import_id_seq;
       public       postgres    false            �            1259    419453    importahk_id_seq    SEQUENCE     y   CREATE SEQUENCE public.importahk_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.importahk_id_seq;
       public       postgres    false            �            1259    420295    importdata1    TABLE     M  CREATE TABLE public.importdata1 (
    cym integer DEFAULT 0 NOT NULL,
    f001insnum character varying DEFAULT ''::character varying NOT NULL,
    f002shinsaym character varying DEFAULT ''::character varying NOT NULL,
    f003comnum character varying(20) DEFAULT ''::character varying NOT NULL,
    f004rezenum character varying(20) DEFAULT ''::character varying NOT NULL,
    f005sejutsuym character varying DEFAULT ''::character varying NOT NULL,
    f006 character varying DEFAULT ''::character varying NOT NULL,
    f007 character varying DEFAULT ''::character varying NOT NULL,
    f008family character varying DEFAULT ''::character varying NOT NULL,
    f009hihomark character varying DEFAULT ''::character varying NOT NULL,
    f010hihonum character varying DEFAULT ''::character varying NOT NULL,
    f011pgender character varying DEFAULT ''::character varying NOT NULL,
    f012pbirthday character varying DEFAULT ''::character varying NOT NULL,
    f013ratio character varying DEFAULT ''::character varying NOT NULL,
    f014 character varying DEFAULT ''::character varying NOT NULL,
    f015 character varying DEFAULT ''::character varying NOT NULL,
    f016 character varying DEFAULT ''::character varying NOT NULL,
    f017 character varying DEFAULT ''::character varying NOT NULL,
    f018 character varying DEFAULT ''::character varying NOT NULL,
    f019 character varying DEFAULT ''::character varying NOT NULL,
    f020 character varying DEFAULT ''::character varying NOT NULL,
    f021 character varying DEFAULT ''::character varying NOT NULL,
    f022clinicnum character varying DEFAULT ''::character varying NOT NULL,
    f023 character varying DEFAULT ''::character varying NOT NULL,
    f024 character varying DEFAULT ''::character varying NOT NULL,
    f025total character varying DEFAULT ''::character varying NOT NULL,
    f026partial character varying DEFAULT ''::character varying NOT NULL,
    f027 character varying DEFAULT ''::character varying NOT NULL,
    f028charge character varying DEFAULT ''::character varying NOT NULL,
    f029 character varying DEFAULT ''::character varying NOT NULL,
    f030 character varying DEFAULT ''::character varying NOT NULL,
    f031 character varying DEFAULT ''::character varying NOT NULL,
    f033firstdate1 character varying DEFAULT ''::character varying NOT NULL,
    f034 character varying DEFAULT ''::character varying NOT NULL,
    f035 character varying DEFAULT ''::character varying NOT NULL,
    f036 character varying DEFAULT ''::character varying NOT NULL,
    f037 character varying DEFAULT ''::character varying NOT NULL,
    f038 character varying DEFAULT ''::character varying NOT NULL,
    f039 character varying DEFAULT ''::character varying NOT NULL,
    f040 character varying DEFAULT ''::character varying NOT NULL,
    f041 character varying DEFAULT ''::character varying NOT NULL,
    f042 character varying DEFAULT ''::character varying NOT NULL,
    f043 character varying DEFAULT ''::character varying NOT NULL,
    f044 character varying DEFAULT ''::character varying NOT NULL,
    f045 character varying DEFAULT ''::character varying NOT NULL,
    f046 character varying DEFAULT ''::character varying NOT NULL,
    f047 character varying DEFAULT ''::character varying NOT NULL,
    f048 character varying DEFAULT ''::character varying NOT NULL,
    f049 character varying DEFAULT ''::character varying NOT NULL,
    f050 character varying DEFAULT ''::character varying NOT NULL,
    f051 character varying DEFAULT ''::character varying NOT NULL,
    f052 character varying DEFAULT ''::character varying NOT NULL,
    f053 character varying DEFAULT ''::character varying NOT NULL,
    f054 character varying DEFAULT ''::character varying NOT NULL,
    f055 character varying DEFAULT ''::character varying NOT NULL,
    f056 character varying DEFAULT ''::character varying NOT NULL,
    f057 character varying DEFAULT ''::character varying NOT NULL,
    f058 character varying DEFAULT ''::character varying NOT NULL,
    f059 character varying DEFAULT ''::character varying NOT NULL,
    f060 character varying DEFAULT ''::character varying NOT NULL,
    f061 character varying DEFAULT ''::character varying NOT NULL,
    f062 character varying DEFAULT ''::character varying NOT NULL,
    f063 character varying DEFAULT ''::character varying NOT NULL,
    f064 character varying DEFAULT ''::character varying NOT NULL,
    f065 character varying DEFAULT ''::character varying NOT NULL,
    f066 character varying DEFAULT ''::character varying NOT NULL,
    f067 character varying DEFAULT ''::character varying NOT NULL,
    f068 character varying DEFAULT ''::character varying NOT NULL,
    f069 character varying DEFAULT ''::character varying NOT NULL,
    f070 character varying DEFAULT ''::character varying NOT NULL,
    f071 character varying DEFAULT ''::character varying NOT NULL,
    f072 character varying DEFAULT ''::character varying NOT NULL,
    f073 character varying DEFAULT ''::character varying NOT NULL,
    f074 character varying DEFAULT ''::character varying NOT NULL,
    f075 character varying DEFAULT ''::character varying NOT NULL,
    f076 character varying DEFAULT ''::character varying NOT NULL,
    f077 character varying DEFAULT ''::character varying NOT NULL,
    f078 character varying DEFAULT ''::character varying NOT NULL,
    f079 character varying DEFAULT ''::character varying NOT NULL,
    f080 character varying DEFAULT ''::character varying NOT NULL,
    f100shinsaymad integer DEFAULT 0,
    f101sejutsuymad integer DEFAULT 0,
    f102birthad date DEFAULT '0001-01-01'::date,
    f103firstdatead date DEFAULT '0001-01-01'::date,
    f104seikeishisnaymad1 integer DEFAULT 0,
    f105seikeishisnaymad2 integer DEFAULT 0,
    f106seikeishisnaymad3 integer DEFAULT 0,
    f107hihomark_narrow character varying DEFAULT ''::character varying NOT NULL,
    f108hihonum_narrow character varying DEFAULT ''::character varying NOT NULL
);
    DROP TABLE public.importdata1;
       public         postgres    false            H           0    0    COLUMN importdata1.cym    COMMENT     Q   COMMENT ON COLUMN public.importdata1.cym IS 'メホール請求年月管理用';
            public       postgres    false    211            I           0    0    COLUMN importdata1.f001insnum    COMMENT     F   COMMENT ON COLUMN public.importdata1.f001insnum IS '保険者番号';
            public       postgres    false    211            J           0    0    COLUMN importdata1.f002shinsaym    COMMENT     E   COMMENT ON COLUMN public.importdata1.f002shinsaym IS '審査年月';
            public       postgres    false    211            K           0    0    COLUMN importdata1.f003comnum    COMMENT     U   COMMENT ON COLUMN public.importdata1.f003comnum IS 'レセプト全国共通キー';
            public       postgres    false    211            L           0    0    COLUMN importdata1.f004rezenum    COMMENT     S   COMMENT ON COLUMN public.importdata1.f004rezenum IS '国保連レセプト番号';
            public       postgres    false    211            M           0    0     COLUMN importdata1.f005sejutsuym    COMMENT     F   COMMENT ON COLUMN public.importdata1.f005sejutsuym IS '施術年月';
            public       postgres    false    211            N           0    0    COLUMN importdata1.f006    COMMENT     >   COMMENT ON COLUMN public.importdata1.f006 IS '保険種別1';
            public       postgres    false    211            O           0    0    COLUMN importdata1.f007    COMMENT     >   COMMENT ON COLUMN public.importdata1.f007 IS '保険種別2';
            public       postgres    false    211            P           0    0    COLUMN importdata1.f008family    COMMENT     O   COMMENT ON COLUMN public.importdata1.f008family IS '本人家族入外区分';
            public       postgres    false    211            Q           0    0    COLUMN importdata1.f009hihomark    COMMENT     N   COMMENT ON COLUMN public.importdata1.f009hihomark IS '被保険者証記号';
            public       postgres    false    211            R           0    0    COLUMN importdata1.f010hihonum    COMMENT     M   COMMENT ON COLUMN public.importdata1.f010hihonum IS '被保険者証番号';
            public       postgres    false    211            S           0    0    COLUMN importdata1.f011pgender    COMMENT     >   COMMENT ON COLUMN public.importdata1.f011pgender IS '性別';
            public       postgres    false    211            T           0    0     COLUMN importdata1.f012pbirthday    COMMENT     F   COMMENT ON COLUMN public.importdata1.f012pbirthday IS '生年月日';
            public       postgres    false    211            U           0    0    COLUMN importdata1.f013ratio    COMMENT     B   COMMENT ON COLUMN public.importdata1.f013ratio IS '給付割合';
            public       postgres    false    211            V           0    0    COLUMN importdata1.f014    COMMENT     >   COMMENT ON COLUMN public.importdata1.f014 IS '特記事項1';
            public       postgres    false    211            W           0    0    COLUMN importdata1.f015    COMMENT     >   COMMENT ON COLUMN public.importdata1.f015 IS '特記事項2';
            public       postgres    false    211            X           0    0    COLUMN importdata1.f016    COMMENT     >   COMMENT ON COLUMN public.importdata1.f016 IS '特記事項3';
            public       postgres    false    211            Y           0    0    COLUMN importdata1.f017    COMMENT     >   COMMENT ON COLUMN public.importdata1.f017 IS '特記事項4';
            public       postgres    false    211            Z           0    0    COLUMN importdata1.f018    COMMENT     >   COMMENT ON COLUMN public.importdata1.f018 IS '特記事項5';
            public       postgres    false    211            [           0    0    COLUMN importdata1.f019    COMMENT     >   COMMENT ON COLUMN public.importdata1.f019 IS '算定区分1';
            public       postgres    false    211            \           0    0    COLUMN importdata1.f020    COMMENT     >   COMMENT ON COLUMN public.importdata1.f020 IS '算定区分2';
            public       postgres    false    211            ]           0    0    COLUMN importdata1.f021    COMMENT     >   COMMENT ON COLUMN public.importdata1.f021 IS '算定区分3';
            public       postgres    false    211            ^           0    0     COLUMN importdata1.f022clinicnum    COMMENT     L   COMMENT ON COLUMN public.importdata1.f022clinicnum IS '施術所コード';
            public       postgres    false    211            _           0    0    COLUMN importdata1.f023    COMMENT     F   COMMENT ON COLUMN public.importdata1.f023 IS '柔整団体コード';
            public       postgres    false    211            `           0    0    COLUMN importdata1.f024    COMMENT     7   COMMENT ON COLUMN public.importdata1.f024 IS '回数';
            public       postgres    false    211            a           0    0    COLUMN importdata1.f025total    COMMENT     B   COMMENT ON COLUMN public.importdata1.f025total IS '決定金額';
            public       postgres    false    211            b           0    0    COLUMN importdata1.f026partial    COMMENT     M   COMMENT ON COLUMN public.importdata1.f026partial IS '決定一部負担金';
            public       postgres    false    211            c           0    0    COLUMN importdata1.f027    COMMENT     :   COMMENT ON COLUMN public.importdata1.f027 IS '費用額';
            public       postgres    false    211            d           0    0    COLUMN importdata1.f028charge    COMMENT     I   COMMENT ON COLUMN public.importdata1.f028charge IS '保険者負担額';
            public       postgres    false    211            e           0    0    COLUMN importdata1.f029    COMMENT     @   COMMENT ON COLUMN public.importdata1.f029 IS '高額療養費';
            public       postgres    false    211            f           0    0    COLUMN importdata1.f030    COMMENT     @   COMMENT ON COLUMN public.importdata1.f030 IS '患者負担額';
            public       postgres    false    211            g           0    0    COLUMN importdata1.f031    COMMENT     C   COMMENT ON COLUMN public.importdata1.f031 IS '国保優先公費';
            public       postgres    false    211            h           0    0 !   COLUMN importdata1.f033firstdate1    COMMENT     J   COMMENT ON COLUMN public.importdata1.f033firstdate1 IS '初検年月日';
            public       postgres    false    211            i           0    0    COLUMN importdata1.f034    COMMENT     =   COMMENT ON COLUMN public.importdata1.f034 IS '負傷名数';
            public       postgres    false    211            j           0    0    COLUMN importdata1.f035    COMMENT     7   COMMENT ON COLUMN public.importdata1.f035 IS '転帰';
            public       postgres    false    211            k           0    0    COLUMN importdata1.f036    COMMENT     I   COMMENT ON COLUMN public.importdata1.f036 IS '転帰レコード区分';
            public       postgres    false    211            l           0    0    COLUMN importdata1.f037    COMMENT     I   COMMENT ON COLUMN public.importdata1.f037 IS '転帰グループ番号';
            public       postgres    false    211            m           0    0    COLUMN importdata1.f038    COMMENT     F   COMMENT ON COLUMN public.importdata1.f038 IS '整形審査年月(1)';
            public       postgres    false    211            n           0    0    COLUMN importdata1.f039    COMMENT     X   COMMENT ON COLUMN public.importdata1.f039 IS '整形レセプト全国共通キー(1)';
            public       postgres    false    211            o           0    0    COLUMN importdata1.f040    COMMENT     O   COMMENT ON COLUMN public.importdata1.f040 IS '整形医療機関コード(1)';
            public       postgres    false    211            p           0    0    COLUMN importdata1.f041    COMMENT     I   COMMENT ON COLUMN public.importdata1.f041 IS '整形医療機関名(1)';
            public       postgres    false    211            q           0    0    COLUMN importdata1.f042    COMMENT     J   COMMENT ON COLUMN public.importdata1.f042 IS '整形診療開始日1(1)';
            public       postgres    false    211            r           0    0    COLUMN importdata1.f043    COMMENT     J   COMMENT ON COLUMN public.importdata1.f043 IS '整形診療開始日2(1)';
            public       postgres    false    211            s           0    0    COLUMN importdata1.f044    COMMENT     J   COMMENT ON COLUMN public.importdata1.f044 IS '整形診療開始日3(1)';
            public       postgres    false    211            t           0    0    COLUMN importdata1.f045    COMMENT     J   COMMENT ON COLUMN public.importdata1.f045 IS '整形疾病コード1(1)';
            public       postgres    false    211            u           0    0    COLUMN importdata1.f046    COMMENT     J   COMMENT ON COLUMN public.importdata1.f046 IS '整形疾病コード2(1)';
            public       postgres    false    211            v           0    0    COLUMN importdata1.f047    COMMENT     J   COMMENT ON COLUMN public.importdata1.f047 IS '整形疾病コード3(1)';
            public       postgres    false    211            w           0    0    COLUMN importdata1.f048    COMMENT     J   COMMENT ON COLUMN public.importdata1.f048 IS '整形疾病コード4(1)';
            public       postgres    false    211            x           0    0    COLUMN importdata1.f049    COMMENT     J   COMMENT ON COLUMN public.importdata1.f049 IS '整形疾病コード5(1)';
            public       postgres    false    211            y           0    0    COLUMN importdata1.f050    COMMENT     F   COMMENT ON COLUMN public.importdata1.f050 IS '整形診療日数(1)';
            public       postgres    false    211            z           0    0    COLUMN importdata1.f051    COMMENT     C   COMMENT ON COLUMN public.importdata1.f051 IS '整形費用額(1)';
            public       postgres    false    211            {           0    0    COLUMN importdata1.f052    COMMENT     F   COMMENT ON COLUMN public.importdata1.f052 IS '整形審査年月(2)';
            public       postgres    false    211            |           0    0    COLUMN importdata1.f053    COMMENT     X   COMMENT ON COLUMN public.importdata1.f053 IS '整形レセプト全国共通キー(2)';
            public       postgres    false    211            }           0    0    COLUMN importdata1.f054    COMMENT     O   COMMENT ON COLUMN public.importdata1.f054 IS '整形医療機関コード(2)';
            public       postgres    false    211            ~           0    0    COLUMN importdata1.f055    COMMENT     I   COMMENT ON COLUMN public.importdata1.f055 IS '整形医療機関名(2)';
            public       postgres    false    211                       0    0    COLUMN importdata1.f056    COMMENT     J   COMMENT ON COLUMN public.importdata1.f056 IS '整形診療開始日1(2)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f057    COMMENT     J   COMMENT ON COLUMN public.importdata1.f057 IS '整形診療開始日2(2)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f058    COMMENT     J   COMMENT ON COLUMN public.importdata1.f058 IS '整形診療開始日3(2)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f059    COMMENT     J   COMMENT ON COLUMN public.importdata1.f059 IS '整形疾病コード1(2)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f060    COMMENT     J   COMMENT ON COLUMN public.importdata1.f060 IS '整形疾病コード2(2)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f061    COMMENT     J   COMMENT ON COLUMN public.importdata1.f061 IS '整形疾病コード3(2)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f062    COMMENT     J   COMMENT ON COLUMN public.importdata1.f062 IS '整形疾病コード4(2)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f063    COMMENT     J   COMMENT ON COLUMN public.importdata1.f063 IS '整形疾病コード5(2)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f064    COMMENT     I   COMMENT ON COLUMN public.importdata1.f064 IS '整形診診療日数(2)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f065    COMMENT     C   COMMENT ON COLUMN public.importdata1.f065 IS '整形費用額(2)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f066    COMMENT     F   COMMENT ON COLUMN public.importdata1.f066 IS '整形審査年月(3)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f067    COMMENT     X   COMMENT ON COLUMN public.importdata1.f067 IS '整形レセプト全国共通キー(3)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f068    COMMENT     O   COMMENT ON COLUMN public.importdata1.f068 IS '整形医療機関コード(3)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f069    COMMENT     I   COMMENT ON COLUMN public.importdata1.f069 IS '整形医療機関名(3)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f070    COMMENT     J   COMMENT ON COLUMN public.importdata1.f070 IS '整形診療開始日1(3)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f071    COMMENT     J   COMMENT ON COLUMN public.importdata1.f071 IS '整形診療開始日2(3)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f072    COMMENT     J   COMMENT ON COLUMN public.importdata1.f072 IS '整形診療開始日3(3)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f073    COMMENT     J   COMMENT ON COLUMN public.importdata1.f073 IS '整形疾病コード1(3)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f074    COMMENT     J   COMMENT ON COLUMN public.importdata1.f074 IS '整形疾病コード2(3)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f075    COMMENT     J   COMMENT ON COLUMN public.importdata1.f075 IS '整形疾病コード3(3)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f076    COMMENT     J   COMMENT ON COLUMN public.importdata1.f076 IS '整形疾病コード4(3)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f077    COMMENT     J   COMMENT ON COLUMN public.importdata1.f077 IS '整形疾病コード5(3)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f078    COMMENT     F   COMMENT ON COLUMN public.importdata1.f078 IS '整形診療日数(3)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f079    COMMENT     C   COMMENT ON COLUMN public.importdata1.f079 IS '整形費用額(3)';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f080    COMMENT     =   COMMENT ON COLUMN public.importdata1.f080 IS '宛名番号';
            public       postgres    false    211            �           0    0 !   COLUMN importdata1.f100shinsaymad    COMMENT     M   COMMENT ON COLUMN public.importdata1.f100shinsaymad IS '審査年月西暦';
            public       postgres    false    211            �           0    0 "   COLUMN importdata1.f101sejutsuymad    COMMENT     N   COMMENT ON COLUMN public.importdata1.f101sejutsuymad IS '施術年月西暦';
            public       postgres    false    211            �           0    0    COLUMN importdata1.f102birthad    COMMENT     J   COMMENT ON COLUMN public.importdata1.f102birthad IS '生年月日西暦';
            public       postgres    false    211            �           0    0 "   COLUMN importdata1.f103firstdatead    COMMENT     Q   COMMENT ON COLUMN public.importdata1.f103firstdatead IS '初検年月日西暦';
            public       postgres    false    211            �           0    0 (   COLUMN importdata1.f104seikeishisnaymad1    COMMENT     \   COMMENT ON COLUMN public.importdata1.f104seikeishisnaymad1 IS '整形審査年月_1西暦';
            public       postgres    false    211            �           0    0 (   COLUMN importdata1.f105seikeishisnaymad2    COMMENT     \   COMMENT ON COLUMN public.importdata1.f105seikeishisnaymad2 IS '整形審査年月_2西暦';
            public       postgres    false    211            �           0    0 (   COLUMN importdata1.f106seikeishisnaymad3    COMMENT     \   COMMENT ON COLUMN public.importdata1.f106seikeishisnaymad3 IS '整形審査年月_3西暦';
            public       postgres    false    211            �           0    0 &   COLUMN importdata1.f107hihomark_narrow    COMMENT     [   COMMENT ON COLUMN public.importdata1.f107hihomark_narrow IS '被保険者証記号半角';
            public       postgres    false    211            �           0    0 %   COLUMN importdata1.f108hihonum_narrow    COMMENT     N   COMMENT ON COLUMN public.importdata1.f108hihonum_narrow IS '証番号半角';
            public       postgres    false    211            �            1259    420754    importdata2    TABLE     W  CREATE TABLE public.importdata2 (
    cym integer DEFAULT 0 NOT NULL,
    f001 character varying DEFAULT ''::character varying NOT NULL,
    f002 character varying DEFAULT ''::character varying NOT NULL,
    f003 character varying DEFAULT ''::character varying NOT NULL,
    f004 character varying DEFAULT ''::character varying NOT NULL,
    f005 character varying DEFAULT ''::character varying NOT NULL,
    f006 character varying DEFAULT ''::character varying NOT NULL,
    f007clinicnum character varying DEFAULT ''::character varying NOT NULL,
    f008clinicname character varying DEFAULT ''::character varying NOT NULL,
    f009 character varying DEFAULT ''::character varying NOT NULL,
    f010 character varying DEFAULT ''::character varying NOT NULL,
    f011 character varying DEFAULT ''::character varying NOT NULL,
    f012 character varying DEFAULT ''::character varying NOT NULL,
    f013 character varying DEFAULT ''::character varying NOT NULL,
    f014 character varying DEFAULT ''::character varying NOT NULL,
    f015 character varying DEFAULT ''::character varying NOT NULL,
    f016 character varying DEFAULT ''::character varying NOT NULL,
    f017 character varying DEFAULT ''::character varying NOT NULL,
    f018 character varying DEFAULT ''::character varying NOT NULL,
    f019 character varying DEFAULT ''::character varying NOT NULL,
    f020 character varying DEFAULT ''::character varying NOT NULL,
    f021 character varying DEFAULT ''::character varying NOT NULL,
    f022 character varying DEFAULT ''::character varying NOT NULL,
    f023 character varying DEFAULT ''::character varying NOT NULL,
    f024counteddays character varying DEFAULT ''::character varying NOT NULL,
    f025 character varying DEFAULT ''::character varying NOT NULL,
    f026comnum character varying DEFAULT ''::character varying NOT NULL,
    f027 character varying DEFAULT ''::character varying NOT NULL,
    f028 character varying DEFAULT ''::character varying NOT NULL,
    f029 character varying DEFAULT ''::character varying NOT NULL,
    f030 character varying DEFAULT ''::character varying NOT NULL,
    f031 character varying DEFAULT ''::character varying NOT NULL,
    f032 character varying DEFAULT ''::character varying NOT NULL,
    f033 character varying DEFAULT ''::character varying NOT NULL,
    f034 character varying DEFAULT ''::character varying NOT NULL,
    f035 character varying DEFAULT ''::character varying NOT NULL,
    f036 character varying DEFAULT ''::character varying NOT NULL,
    f037 character varying DEFAULT ''::character varying NOT NULL,
    f038 character varying DEFAULT ''::character varying NOT NULL,
    f039 character varying DEFAULT ''::character varying NOT NULL,
    f040 character varying DEFAULT ''::character varying NOT NULL,
    f041 character varying DEFAULT ''::character varying NOT NULL,
    f042 character varying DEFAULT ''::character varying NOT NULL,
    f043 character varying DEFAULT ''::character varying NOT NULL,
    f044 character varying DEFAULT ''::character varying NOT NULL,
    f045 character varying DEFAULT ''::character varying NOT NULL,
    f046 character varying DEFAULT ''::character varying NOT NULL,
    f047 character varying DEFAULT ''::character varying NOT NULL,
    f048 character varying DEFAULT ''::character varying NOT NULL,
    f049 character varying DEFAULT ''::character varying NOT NULL,
    f050 character varying DEFAULT ''::character varying NOT NULL,
    f051 character varying DEFAULT ''::character varying NOT NULL,
    f052 character varying DEFAULT ''::character varying NOT NULL,
    f053 character varying DEFAULT ''::character varying NOT NULL,
    f054 character varying DEFAULT ''::character varying NOT NULL,
    f055 character varying DEFAULT ''::character varying NOT NULL,
    f056 character varying DEFAULT ''::character varying NOT NULL,
    f057 character varying DEFAULT ''::character varying NOT NULL
);
    DROP TABLE public.importdata2;
       public         postgres    false            �           0    0    COLUMN importdata2.cym    COMMENT     Q   COMMENT ON COLUMN public.importdata2.cym IS 'メホール請求年月管理用';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f001    COMMENT     @   COMMENT ON COLUMN public.importdata2.f001 IS '保険者番号';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f002    COMMENT     :   COMMENT ON COLUMN public.importdata2.f002 IS '証記号';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f003    COMMENT     :   COMMENT ON COLUMN public.importdata2.f003 IS '証番号';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f004    COMMENT     7   COMMENT ON COLUMN public.importdata2.f004 IS '氏名';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f005    COMMENT     =   COMMENT ON COLUMN public.importdata2.f005 IS '診療年月';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f006    COMMENT     =   COMMENT ON COLUMN public.importdata2.f006 IS '決定点数';
            public       postgres    false    212            �           0    0     COLUMN importdata2.f007clinicnum    COMMENT     I   COMMENT ON COLUMN public.importdata2.f007clinicnum IS '機関コード';
            public       postgres    false    212            �           0    0 !   COLUMN importdata2.f008clinicname    COMMENT     J   COMMENT ON COLUMN public.importdata2.f008clinicname IS '医療機関名';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f009    COMMENT     @   COMMENT ON COLUMN public.importdata2.f009 IS '処方機関名';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f010    COMMENT     =   COMMENT ON COLUMN public.importdata2.f010 IS '最新履歴';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f011    COMMENT     =   COMMENT ON COLUMN public.importdata2.f011 IS '世帯番号';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f012    COMMENT     =   COMMENT ON COLUMN public.importdata2.f012 IS '宛名番号';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f013    COMMENT     =   COMMENT ON COLUMN public.importdata2.f013 IS '生年月日';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f014    COMMENT     7   COMMENT ON COLUMN public.importdata2.f014 IS '性別';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f015    COMMENT     :   COMMENT ON COLUMN public.importdata2.f015 IS '種別１';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f016    COMMENT     :   COMMENT ON COLUMN public.importdata2.f016 IS '種別２';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f017    COMMENT     7   COMMENT ON COLUMN public.importdata2.f017 IS '入外';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f018    COMMENT     7   COMMENT ON COLUMN public.importdata2.f018 IS '本家';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f019    COMMENT     =   COMMENT ON COLUMN public.importdata2.f019 IS '給付割合';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f020    COMMENT     F   COMMENT ON COLUMN public.importdata2.f020 IS '診療・療養費別';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f021    COMMENT     :   COMMENT ON COLUMN public.importdata2.f021 IS '点数表';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f022    COMMENT     @   COMMENT ON COLUMN public.importdata2.f022 IS '療養費種別';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f023    COMMENT     =   COMMENT ON COLUMN public.importdata2.f023 IS '審査年月';
            public       postgres    false    212            �           0    0 "   COLUMN importdata2.f024counteddays    COMMENT     E   COMMENT ON COLUMN public.importdata2.f024counteddays IS '実日数';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f025    COMMENT     @   COMMENT ON COLUMN public.importdata2.f025 IS '食事基準額';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f026comnum    COMMENT     U   COMMENT ON COLUMN public.importdata2.f026comnum IS 'レセプト全国共通キー';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f027    COMMENT     F   COMMENT ON COLUMN public.importdata2.f027 IS '処方機関コード';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f028    COMMENT     7   COMMENT ON COLUMN public.importdata2.f028 IS 'クリ';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f029    COMMENT     7   COMMENT ON COLUMN public.importdata2.f029 IS 'ケア';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f030    COMMENT     7   COMMENT ON COLUMN public.importdata2.f030 IS '容認';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f031    COMMENT     7   COMMENT ON COLUMN public.importdata2.f031 IS '不当';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f032    COMMENT     :   COMMENT ON COLUMN public.importdata2.f032 IS '第三者';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f033    COMMENT     =   COMMENT ON COLUMN public.importdata2.f033 IS '給付制限';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f034    COMMENT     7   COMMENT ON COLUMN public.importdata2.f034 IS '高額';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f035    COMMENT     7   COMMENT ON COLUMN public.importdata2.f035 IS '予約';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f036    COMMENT     7   COMMENT ON COLUMN public.importdata2.f036 IS '処理';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f037    COMMENT     =   COMMENT ON COLUMN public.importdata2.f037 IS '疑義種別';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f038    COMMENT     7   COMMENT ON COLUMN public.importdata2.f038 IS '参考';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f039    COMMENT     7   COMMENT ON COLUMN public.importdata2.f039 IS '状態';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f040    COMMENT     @   COMMENT ON COLUMN public.importdata2.f040 IS 'コード情報';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f041    COMMENT     =   COMMENT ON COLUMN public.importdata2.f041 IS '原本区分';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f042    COMMENT     =   COMMENT ON COLUMN public.importdata2.f042 IS '原本所在';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f043    COMMENT     :   COMMENT ON COLUMN public.importdata2.f043 IS '県内外';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f044    COMMENT     <   COMMENT ON COLUMN public.importdata2.f044 IS '付箋１  ';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f045    COMMENT     :   COMMENT ON COLUMN public.importdata2.f045 IS '付箋２';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f046    COMMENT     :   COMMENT ON COLUMN public.importdata2.f046 IS '付箋３';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f047    COMMENT     :   COMMENT ON COLUMN public.importdata2.f047 IS '付箋４';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f048    COMMENT     :   COMMENT ON COLUMN public.importdata2.f048 IS '付箋５';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f049    COMMENT     :   COMMENT ON COLUMN public.importdata2.f049 IS '付箋６';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f050    COMMENT     :   COMMENT ON COLUMN public.importdata2.f050 IS '付箋７';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f051    COMMENT     :   COMMENT ON COLUMN public.importdata2.f051 IS '付箋８';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f052    COMMENT     :   COMMENT ON COLUMN public.importdata2.f052 IS '付箋９';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f053    COMMENT     =   COMMENT ON COLUMN public.importdata2.f053 IS '付箋１０';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f054    COMMENT     =   COMMENT ON COLUMN public.importdata2.f054 IS '付箋１１';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f055    COMMENT     =   COMMENT ON COLUMN public.importdata2.f055 IS '付箋１２';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f056    COMMENT     =   COMMENT ON COLUMN public.importdata2.f056 IS '付箋１３';
            public       postgres    false    212            �           0    0    COLUMN importdata2.f057    COMMENT     =   COMMENT ON COLUMN public.importdata2.f057 IS '付箋１４';
            public       postgres    false    212            �            1259    420819    importdata3    TABLE     �K  CREATE TABLE public.importdata3 (
    cym integer DEFAULT 0 NOT NULL,
    f001shoriym character varying DEFAULT ''::character varying NOT NULL,
    f002comnum character varying DEFAULT ''::character varying NOT NULL,
    f003 character varying DEFAULT ''::character varying NOT NULL,
    f004rezeptnum character varying DEFAULT ''::character varying NOT NULL,
    f005 character varying DEFAULT ''::character varying NOT NULL,
    f006clinicnum character varying DEFAULT ''::character varying NOT NULL,
    f007 character varying DEFAULT ''::character varying NOT NULL,
    f008 character varying DEFAULT ''::character varying NOT NULL,
    f009insnum character varying DEFAULT ''::character varying NOT NULL,
    f010hmark character varying DEFAULT ''::character varying NOT NULL,
    f011hnum character varying DEFAULT ''::character varying NOT NULL,
    f012pbirthday character varying DEFAULT ''::character varying NOT NULL,
    f013pgender character varying DEFAULT ''::character varying NOT NULL,
    f014pname character varying DEFAULT ''::character varying NOT NULL,
    f015personalnum character varying DEFAULT ''::character varying NOT NULL,
    f016 character varying DEFAULT ''::character varying NOT NULL,
    f017 character varying DEFAULT ''::character varying NOT NULL,
    f018 character varying DEFAULT ''::character varying NOT NULL,
    f019 character varying DEFAULT ''::character varying NOT NULL,
    f020 character varying DEFAULT ''::character varying NOT NULL,
    f021 character varying DEFAULT ''::character varying NOT NULL,
    f022 character varying DEFAULT ''::character varying NOT NULL,
    f023 character varying DEFAULT ''::character varying NOT NULL,
    f024 character varying DEFAULT ''::character varying NOT NULL,
    f025 character varying DEFAULT ''::character varying NOT NULL,
    f026 character varying DEFAULT ''::character varying NOT NULL,
    f027 character varying DEFAULT ''::character varying NOT NULL,
    f028 character varying DEFAULT ''::character varying NOT NULL,
    f029 character varying DEFAULT ''::character varying NOT NULL,
    f030 character varying DEFAULT ''::character varying NOT NULL,
    f031 character varying DEFAULT ''::character varying NOT NULL,
    f032 character varying DEFAULT ''::character varying NOT NULL,
    f033 character varying DEFAULT ''::character varying NOT NULL,
    f034 character varying DEFAULT ''::character varying NOT NULL,
    f035 character varying DEFAULT ''::character varying NOT NULL,
    f036 character varying DEFAULT ''::character varying NOT NULL,
    f037 character varying DEFAULT ''::character varying NOT NULL,
    f038 character varying DEFAULT ''::character varying NOT NULL,
    f039 character varying DEFAULT ''::character varying NOT NULL,
    f040 character varying DEFAULT ''::character varying NOT NULL,
    f041 character varying DEFAULT ''::character varying NOT NULL,
    f042 character varying DEFAULT ''::character varying NOT NULL,
    f043 character varying DEFAULT ''::character varying NOT NULL,
    f044 character varying DEFAULT ''::character varying NOT NULL,
    f045 character varying DEFAULT ''::character varying NOT NULL,
    f046 character varying DEFAULT ''::character varying NOT NULL,
    f047 character varying DEFAULT ''::character varying NOT NULL,
    f048 character varying DEFAULT ''::character varying NOT NULL,
    f049 character varying DEFAULT ''::character varying NOT NULL,
    f050 character varying DEFAULT ''::character varying NOT NULL,
    f051 character varying DEFAULT ''::character varying NOT NULL,
    f052 character varying DEFAULT ''::character varying NOT NULL,
    f053 character varying DEFAULT ''::character varying NOT NULL,
    f054 character varying DEFAULT ''::character varying NOT NULL,
    f055 character varying DEFAULT ''::character varying NOT NULL,
    f056 character varying DEFAULT ''::character varying NOT NULL,
    f057 character varying DEFAULT ''::character varying NOT NULL,
    f058 character varying DEFAULT ''::character varying NOT NULL,
    f059 character varying DEFAULT ''::character varying NOT NULL,
    f060 character varying DEFAULT ''::character varying NOT NULL,
    f061 character varying DEFAULT ''::character varying NOT NULL,
    f062 character varying DEFAULT ''::character varying NOT NULL,
    f063 character varying DEFAULT ''::character varying NOT NULL,
    f064 character varying DEFAULT ''::character varying NOT NULL,
    f065 character varying DEFAULT ''::character varying NOT NULL,
    f066 character varying DEFAULT ''::character varying NOT NULL,
    f067 character varying DEFAULT ''::character varying NOT NULL,
    f068 character varying DEFAULT ''::character varying NOT NULL,
    f069 character varying DEFAULT ''::character varying NOT NULL,
    f070 character varying DEFAULT ''::character varying NOT NULL,
    f071 character varying DEFAULT ''::character varying NOT NULL,
    f072 character varying DEFAULT ''::character varying NOT NULL,
    f073 character varying DEFAULT ''::character varying NOT NULL,
    f074 character varying DEFAULT ''::character varying NOT NULL,
    f075 character varying DEFAULT ''::character varying NOT NULL,
    f076 character varying DEFAULT ''::character varying NOT NULL,
    f077 character varying DEFAULT ''::character varying NOT NULL,
    f078 character varying DEFAULT ''::character varying NOT NULL,
    f079 character varying DEFAULT ''::character varying NOT NULL,
    f080 character varying DEFAULT ''::character varying NOT NULL,
    f081 character varying DEFAULT ''::character varying NOT NULL,
    f082 character varying DEFAULT ''::character varying NOT NULL,
    f083 character varying DEFAULT ''::character varying NOT NULL,
    f084 character varying DEFAULT ''::character varying NOT NULL,
    f085 character varying DEFAULT ''::character varying NOT NULL,
    f086 character varying DEFAULT ''::character varying NOT NULL,
    f087 character varying DEFAULT ''::character varying NOT NULL,
    f088 character varying DEFAULT ''::character varying NOT NULL,
    f089 character varying DEFAULT ''::character varying NOT NULL,
    f090 character varying DEFAULT ''::character varying NOT NULL,
    f091 character varying DEFAULT ''::character varying NOT NULL,
    f092 character varying DEFAULT ''::character varying NOT NULL,
    f093 character varying DEFAULT ''::character varying NOT NULL,
    f094 character varying DEFAULT ''::character varying NOT NULL,
    f095 character varying DEFAULT ''::character varying NOT NULL,
    f096 character varying DEFAULT ''::character varying NOT NULL,
    f097 character varying DEFAULT ''::character varying NOT NULL,
    f098 character varying DEFAULT ''::character varying NOT NULL,
    f099 character varying DEFAULT ''::character varying NOT NULL,
    f100 character varying DEFAULT ''::character varying NOT NULL,
    f101 character varying DEFAULT ''::character varying NOT NULL,
    f102 character varying DEFAULT ''::character varying NOT NULL,
    f103 character varying DEFAULT ''::character varying NOT NULL,
    f104 character varying DEFAULT ''::character varying NOT NULL,
    f105 character varying DEFAULT ''::character varying NOT NULL,
    f106 character varying DEFAULT ''::character varying NOT NULL,
    f107 character varying DEFAULT ''::character varying NOT NULL,
    f108 character varying DEFAULT ''::character varying NOT NULL,
    f109 character varying DEFAULT ''::character varying NOT NULL,
    f110 character varying DEFAULT ''::character varying NOT NULL,
    f111 character varying DEFAULT ''::character varying NOT NULL,
    f112 character varying DEFAULT ''::character varying NOT NULL,
    f113 character varying DEFAULT ''::character varying NOT NULL,
    f114 character varying DEFAULT ''::character varying NOT NULL,
    f115 character varying DEFAULT ''::character varying NOT NULL,
    f116 character varying DEFAULT ''::character varying NOT NULL,
    f117 character varying DEFAULT ''::character varying NOT NULL,
    f118 character varying DEFAULT ''::character varying NOT NULL,
    f119 character varying DEFAULT ''::character varying NOT NULL,
    f120 character varying DEFAULT ''::character varying NOT NULL,
    f121 character varying DEFAULT ''::character varying NOT NULL,
    f122 character varying DEFAULT ''::character varying NOT NULL,
    f123 character varying DEFAULT ''::character varying NOT NULL,
    f124 character varying DEFAULT ''::character varying NOT NULL,
    f125 character varying DEFAULT ''::character varying NOT NULL,
    f126 character varying DEFAULT ''::character varying NOT NULL,
    f127 character varying DEFAULT ''::character varying NOT NULL,
    f128 character varying DEFAULT ''::character varying NOT NULL,
    f129 character varying DEFAULT ''::character varying NOT NULL,
    f130 character varying DEFAULT ''::character varying NOT NULL,
    f131 character varying DEFAULT ''::character varying NOT NULL,
    f132 character varying DEFAULT ''::character varying NOT NULL,
    f133 character varying DEFAULT ''::character varying NOT NULL,
    f134 character varying DEFAULT ''::character varying NOT NULL,
    f135 character varying DEFAULT ''::character varying NOT NULL,
    f136 character varying DEFAULT ''::character varying NOT NULL,
    f137 character varying DEFAULT ''::character varying NOT NULL,
    f138 character varying DEFAULT ''::character varying NOT NULL,
    f139 character varying DEFAULT ''::character varying NOT NULL,
    f140 character varying DEFAULT ''::character varying NOT NULL,
    f141 character varying DEFAULT ''::character varying NOT NULL,
    f142 character varying DEFAULT ''::character varying NOT NULL,
    f143 character varying DEFAULT ''::character varying NOT NULL,
    f144 character varying DEFAULT ''::character varying NOT NULL,
    f145 character varying DEFAULT ''::character varying NOT NULL,
    f146 character varying DEFAULT ''::character varying NOT NULL,
    f147 character varying DEFAULT ''::character varying NOT NULL,
    f148 character varying DEFAULT ''::character varying NOT NULL,
    f149 character varying DEFAULT ''::character varying NOT NULL,
    f150 character varying DEFAULT ''::character varying NOT NULL,
    f151 character varying DEFAULT ''::character varying NOT NULL,
    f152 character varying DEFAULT ''::character varying NOT NULL,
    f153 character varying DEFAULT ''::character varying NOT NULL,
    f154 character varying DEFAULT ''::character varying NOT NULL,
    f155 character varying DEFAULT ''::character varying NOT NULL,
    f156 character varying DEFAULT ''::character varying NOT NULL,
    f157 character varying DEFAULT ''::character varying NOT NULL,
    f158 character varying DEFAULT ''::character varying NOT NULL,
    f159 character varying DEFAULT ''::character varying NOT NULL,
    f160 character varying DEFAULT ''::character varying NOT NULL,
    f161 character varying DEFAULT ''::character varying NOT NULL,
    f162 character varying DEFAULT ''::character varying NOT NULL,
    f163 character varying DEFAULT ''::character varying NOT NULL,
    f164 character varying DEFAULT ''::character varying NOT NULL,
    f165 character varying DEFAULT ''::character varying NOT NULL,
    f166 character varying DEFAULT ''::character varying NOT NULL,
    f167 character varying DEFAULT ''::character varying NOT NULL,
    f168 character varying DEFAULT ''::character varying NOT NULL,
    f169 character varying DEFAULT ''::character varying NOT NULL,
    f170 character varying DEFAULT ''::character varying NOT NULL,
    f171 character varying DEFAULT ''::character varying NOT NULL,
    f172 character varying DEFAULT ''::character varying NOT NULL,
    f173 character varying DEFAULT ''::character varying NOT NULL,
    f174 character varying DEFAULT ''::character varying NOT NULL,
    f175 character varying DEFAULT ''::character varying NOT NULL,
    f176 character varying DEFAULT ''::character varying NOT NULL,
    f177 character varying DEFAULT ''::character varying NOT NULL,
    f178 character varying DEFAULT ''::character varying NOT NULL,
    f179 character varying DEFAULT ''::character varying NOT NULL,
    f180 character varying DEFAULT ''::character varying NOT NULL,
    f181 character varying DEFAULT ''::character varying NOT NULL,
    f182 character varying DEFAULT ''::character varying NOT NULL,
    f183 character varying DEFAULT ''::character varying NOT NULL,
    f184 character varying DEFAULT ''::character varying NOT NULL,
    f185 character varying DEFAULT ''::character varying NOT NULL,
    f186 character varying DEFAULT ''::character varying NOT NULL,
    f187 character varying DEFAULT ''::character varying NOT NULL,
    f188 character varying DEFAULT ''::character varying NOT NULL,
    f189 character varying DEFAULT ''::character varying NOT NULL,
    f190 character varying DEFAULT ''::character varying NOT NULL,
    f191 character varying DEFAULT ''::character varying NOT NULL,
    f192 character varying DEFAULT ''::character varying NOT NULL,
    f193 character varying DEFAULT ''::character varying NOT NULL,
    f194 character varying DEFAULT ''::character varying NOT NULL,
    f195 character varying DEFAULT ''::character varying NOT NULL,
    f196 character varying DEFAULT ''::character varying NOT NULL,
    f197 character varying DEFAULT ''::character varying NOT NULL,
    f198 character varying DEFAULT ''::character varying NOT NULL,
    f199 character varying DEFAULT ''::character varying NOT NULL,
    f200 character varying DEFAULT ''::character varying NOT NULL,
    f201 character varying DEFAULT ''::character varying NOT NULL,
    f202 character varying DEFAULT ''::character varying NOT NULL,
    f203 character varying DEFAULT ''::character varying NOT NULL,
    f204 character varying DEFAULT ''::character varying NOT NULL,
    f205 character varying DEFAULT ''::character varying NOT NULL,
    f206 character varying DEFAULT ''::character varying NOT NULL,
    f207 character varying DEFAULT ''::character varying NOT NULL,
    f208 character varying DEFAULT ''::character varying NOT NULL,
    f209 character varying DEFAULT ''::character varying NOT NULL,
    f210 character varying DEFAULT ''::character varying NOT NULL,
    f211 character varying DEFAULT ''::character varying NOT NULL,
    f212 character varying DEFAULT ''::character varying NOT NULL,
    f213 character varying DEFAULT ''::character varying NOT NULL,
    f214 character varying DEFAULT ''::character varying NOT NULL,
    f215 character varying DEFAULT ''::character varying NOT NULL,
    f216 character varying DEFAULT ''::character varying NOT NULL,
    f217 character varying DEFAULT ''::character varying NOT NULL,
    f218 character varying DEFAULT ''::character varying NOT NULL,
    f219 character varying DEFAULT ''::character varying NOT NULL,
    f220 character varying DEFAULT ''::character varying NOT NULL,
    f221 character varying DEFAULT ''::character varying NOT NULL,
    f222 character varying DEFAULT ''::character varying NOT NULL,
    f223 character varying DEFAULT ''::character varying NOT NULL,
    f224 character varying DEFAULT ''::character varying NOT NULL,
    f225 character varying DEFAULT ''::character varying NOT NULL,
    f226 character varying DEFAULT ''::character varying NOT NULL,
    f227 character varying DEFAULT ''::character varying NOT NULL,
    f228 character varying DEFAULT ''::character varying NOT NULL,
    f229 character varying DEFAULT ''::character varying NOT NULL,
    f230 character varying DEFAULT ''::character varying NOT NULL,
    f231 character varying DEFAULT ''::character varying NOT NULL,
    f232 character varying DEFAULT ''::character varying NOT NULL,
    f233 character varying DEFAULT ''::character varying NOT NULL,
    f234 character varying DEFAULT ''::character varying NOT NULL,
    f235 character varying DEFAULT ''::character varying NOT NULL,
    f236 character varying DEFAULT ''::character varying NOT NULL,
    f237 character varying DEFAULT ''::character varying NOT NULL,
    f238 character varying DEFAULT ''::character varying NOT NULL,
    f239 character varying DEFAULT ''::character varying NOT NULL,
    f240 character varying DEFAULT ''::character varying NOT NULL,
    f241 character varying DEFAULT ''::character varying NOT NULL,
    f242 character varying DEFAULT ''::character varying NOT NULL,
    f243 character varying DEFAULT ''::character varying NOT NULL,
    f244 character varying DEFAULT ''::character varying NOT NULL,
    f245 character varying DEFAULT ''::character varying NOT NULL,
    f246 character varying DEFAULT ''::character varying NOT NULL,
    f247 character varying DEFAULT ''::character varying NOT NULL,
    f248 character varying DEFAULT ''::character varying NOT NULL,
    f249 character varying DEFAULT ''::character varying NOT NULL,
    f250 character varying DEFAULT ''::character varying NOT NULL,
    f251 character varying DEFAULT ''::character varying NOT NULL,
    f252 character varying DEFAULT ''::character varying NOT NULL,
    f253 character varying DEFAULT ''::character varying NOT NULL,
    f254 character varying DEFAULT ''::character varying NOT NULL,
    f255 character varying DEFAULT ''::character varying NOT NULL,
    f256 character varying DEFAULT ''::character varying NOT NULL,
    f257 character varying DEFAULT ''::character varying NOT NULL,
    f258 character varying DEFAULT ''::character varying NOT NULL,
    f259 character varying DEFAULT ''::character varying NOT NULL,
    f260 character varying DEFAULT ''::character varying NOT NULL,
    f261 character varying DEFAULT ''::character varying NOT NULL,
    f262 character varying DEFAULT ''::character varying NOT NULL,
    f263 character varying DEFAULT ''::character varying NOT NULL,
    f264 character varying DEFAULT ''::character varying NOT NULL,
    f265 character varying DEFAULT ''::character varying NOT NULL,
    f266 character varying DEFAULT ''::character varying NOT NULL,
    f267 character varying DEFAULT ''::character varying NOT NULL,
    f268 character varying DEFAULT ''::character varying NOT NULL,
    f269 character varying DEFAULT ''::character varying NOT NULL,
    f270 character varying DEFAULT ''::character varying NOT NULL,
    f271 character varying DEFAULT ''::character varying NOT NULL,
    f272 character varying DEFAULT ''::character varying NOT NULL,
    f273 character varying DEFAULT ''::character varying NOT NULL,
    f274 character varying DEFAULT ''::character varying NOT NULL,
    f275 character varying DEFAULT ''::character varying NOT NULL,
    f276 character varying DEFAULT ''::character varying NOT NULL,
    f277 character varying DEFAULT ''::character varying NOT NULL,
    f278 character varying DEFAULT ''::character varying NOT NULL,
    f279 character varying DEFAULT ''::character varying NOT NULL,
    f280 character varying DEFAULT ''::character varying NOT NULL,
    f281 character varying DEFAULT ''::character varying NOT NULL,
    f282 character varying DEFAULT ''::character varying NOT NULL,
    f283 character varying DEFAULT ''::character varying NOT NULL,
    f284 character varying DEFAULT ''::character varying NOT NULL,
    f285 character varying DEFAULT ''::character varying NOT NULL,
    f286 character varying DEFAULT ''::character varying NOT NULL,
    f287 character varying DEFAULT ''::character varying NOT NULL
);
    DROP TABLE public.importdata3;
       public         postgres    false            �           0    0    COLUMN importdata3.cym    COMMENT     Q   COMMENT ON COLUMN public.importdata3.cym IS 'メホール請求年月管理用';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f001shoriym    COMMENT     D   COMMENT ON COLUMN public.importdata3.f001shoriym IS '処理年月';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f002comnum    COMMENT     U   COMMENT ON COLUMN public.importdata3.f002comnum IS 'レセプト全国共通キー';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f003    COMMENT     =   COMMENT ON COLUMN public.importdata3.f003 IS '履歴番号';
            public       postgres    false    213            �           0    0     COLUMN importdata3.f004rezeptnum    COMMENT     U   COMMENT ON COLUMN public.importdata3.f004rezeptnum IS '国保連レセプト番号';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f005    COMMENT     =   COMMENT ON COLUMN public.importdata3.f005 IS '事業区分';
            public       postgres    false    213            �           0    0     COLUMN importdata3.f006clinicnum    COMMENT     O   COMMENT ON COLUMN public.importdata3.f006clinicnum IS '医療機関コード';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f007    COMMENT     I   COMMENT ON COLUMN public.importdata3.f007 IS '旧総合病院診療科';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f008    COMMENT     @   COMMENT ON COLUMN public.importdata3.f008 IS '任意診療科';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f009insnum    COMMENT     F   COMMENT ON COLUMN public.importdata3.f009insnum IS '保険者番号';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f010hmark    COMMENT     K   COMMENT ON COLUMN public.importdata3.f010hmark IS '被保険者証記号';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f011hnum    COMMENT     V   COMMENT ON COLUMN public.importdata3.f011hnum IS '被保険者証番号（全角）';
            public       postgres    false    213            �           0    0     COLUMN importdata3.f012pbirthday    COMMENT     F   COMMENT ON COLUMN public.importdata3.f012pbirthday IS '生年月日';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f013pgender    COMMENT     >   COMMENT ON COLUMN public.importdata3.f013pgender IS '性別';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f014pname    COMMENT     <   COMMENT ON COLUMN public.importdata3.f014pname IS '氏名';
            public       postgres    false    213            �           0    0 "   COLUMN importdata3.f015personalnum    COMMENT     H   COMMENT ON COLUMN public.importdata3.f015personalnum IS '個人番号';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f016    COMMENT     =   COMMENT ON COLUMN public.importdata3.f016 IS '世帯番号';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f017    COMMENT     @   COMMENT ON COLUMN public.importdata3.f017 IS '市町村番号';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f018    COMMENT     @   COMMENT ON COLUMN public.importdata3.f018 IS '受給者番号';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f019    COMMENT     X   COMMENT ON COLUMN public.importdata3.f019 IS '制御・管理情報制御処理区分';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f020    COMMENT     U   COMMENT ON COLUMN public.importdata3.f020 IS '制御・管理情報制御DPC区分';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f021    COMMENT     d   COMMENT ON COLUMN public.importdata3.f021 IS '制御・管理情報柔整・療養費口座番号';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f022    COMMENT     @   COMMENT ON COLUMN public.importdata3.f022 IS '月診療年月';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f023    COMMENT     F   COMMENT ON COLUMN public.importdata3.f023 IS '月支給決定年月';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f024    COMMENT     C   COMMENT ON COLUMN public.importdata3.f024 IS '月自支給期間';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f025    COMMENT     C   COMMENT ON COLUMN public.importdata3.f025 IS '月至支給期間';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f026    COMMENT     a   COMMENT ON COLUMN public.importdata3.f026 IS '区分・種別保険制度（保険種別①）';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f027    COMMENT     a   COMMENT ON COLUMN public.importdata3.f027 IS '区分・種別保険種別（保険種別②）';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f028    COMMENT     R   COMMENT ON COLUMN public.importdata3.f028 IS '区分・種別本人家族入外';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f029    COMMENT     I   COMMENT ON COLUMN public.importdata3.f029 IS '区分・種別点数表';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f030    COMMENT     O   COMMENT ON COLUMN public.importdata3.f030 IS '区分・種別療養費種別';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f031    COMMENT     U   COMMENT ON COLUMN public.importdata3.f031 IS '区分・種別海外療養費区分';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f032    COMMENT     I   COMMENT ON COLUMN public.importdata3.f032 IS '給付割合給付割合';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f033    COMMENT     C   COMMENT ON COLUMN public.importdata3.f033 IS '給付割合割引';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f034    COMMENT     F   COMMENT ON COLUMN public.importdata3.f034 IS '日診療開始日１';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f035    COMMENT     =   COMMENT ON COLUMN public.importdata3.f035 IS '日転帰１';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f036    COMMENT     F   COMMENT ON COLUMN public.importdata3.f036 IS '日診療開始日２';
            public       postgres    false    213                        0    0    COLUMN importdata3.f037    COMMENT     =   COMMENT ON COLUMN public.importdata3.f037 IS '日転帰２';
            public       postgres    false    213                       0    0    COLUMN importdata3.f038    COMMENT     F   COMMENT ON COLUMN public.importdata3.f038 IS '日診療開始日３';
            public       postgres    false    213                       0    0    COLUMN importdata3.f039    COMMENT     =   COMMENT ON COLUMN public.importdata3.f039 IS '日転帰３';
            public       postgres    false    213                       0    0    COLUMN importdata3.f040    COMMENT     @   COMMENT ON COLUMN public.importdata3.f040 IS '日初診回数';
            public       postgres    false    213                       0    0    COLUMN importdata3.f041    COMMENT     @   COMMENT ON COLUMN public.importdata3.f041 IS '日再診回数';
            public       postgres    false    213                       0    0    COLUMN importdata3.f042    COMMENT     C   COMMENT ON COLUMN public.importdata3.f042 IS '日入院年月日';
            public       postgres    false    213                       0    0    COLUMN importdata3.f043    COMMENT     C   COMMENT ON COLUMN public.importdata3.f043 IS '柔整団体機関';
            public       postgres    false    213                       0    0    COLUMN importdata3.f044    COMMENT     L   COMMENT ON COLUMN public.importdata3.f044 IS '処方箋交付医療機関';
            public       postgres    false    213                       0    0    COLUMN importdata3.f045    COMMENT     F   COMMENT ON COLUMN public.importdata3.f045 IS '特記特記事項１';
            public       postgres    false    213            	           0    0    COLUMN importdata3.f046    COMMENT     F   COMMENT ON COLUMN public.importdata3.f046 IS '特記特記事項２';
            public       postgres    false    213            
           0    0    COLUMN importdata3.f047    COMMENT     F   COMMENT ON COLUMN public.importdata3.f047 IS '特記特記事項３';
            public       postgres    false    213                       0    0    COLUMN importdata3.f048    COMMENT     F   COMMENT ON COLUMN public.importdata3.f048 IS '特記特記事項４';
            public       postgres    false    213                       0    0    COLUMN importdata3.f049    COMMENT     F   COMMENT ON COLUMN public.importdata3.f049 IS '特記特記事項５';
            public       postgres    false    213                       0    0    COLUMN importdata3.f050    COMMENT     C   COMMENT ON COLUMN public.importdata3.f050 IS '特記原爆区分';
            public       postgres    false    213                       0    0    COLUMN importdata3.f051    COMMENT     L   COMMENT ON COLUMN public.importdata3.f051 IS '特記継続療養費区分';
            public       postgres    false    213                       0    0    COLUMN importdata3.f052    COMMENT     C   COMMENT ON COLUMN public.importdata3.f052 IS '減額減額割合';
            public       postgres    false    213                       0    0    COLUMN importdata3.f053    COMMENT     U   COMMENT ON COLUMN public.importdata3.f053 IS '減額減額・免除・猶予区分';
            public       postgres    false    213                       0    0    COLUMN importdata3.f054    COMMENT     C   COMMENT ON COLUMN public.importdata3.f054 IS '減額減額金額';
            public       postgres    false    213                       0    0    COLUMN importdata3.f055    COMMENT     @   COMMENT ON COLUMN public.importdata3.f055 IS '算定区分１';
            public       postgres    false    213                       0    0    COLUMN importdata3.f056    COMMENT     @   COMMENT ON COLUMN public.importdata3.f056 IS '算定区分２';
            public       postgres    false    213                       0    0    COLUMN importdata3.f057    COMMENT     @   COMMENT ON COLUMN public.importdata3.f057 IS '算定区分３';
            public       postgres    false    213                       0    0    COLUMN importdata3.f058    COMMENT     L   COMMENT ON COLUMN public.importdata3.f058 IS '給付点検初診料有無';
            public       postgres    false    213                       0    0    COLUMN importdata3.f059    COMMENT     R   COMMENT ON COLUMN public.importdata3.f059 IS '給付点検乳幼児加算区分';
            public       postgres    false    213                       0    0    COLUMN importdata3.f060    COMMENT     R   COMMENT ON COLUMN public.importdata3.f060 IS '給付点検調剤技術基本料';
            public       postgres    false    213                       0    0    COLUMN importdata3.f061    COMMENT     ^   COMMENT ON COLUMN public.importdata3.f061 IS '給付点検入院基本料（初期加算）';
            public       postgres    false    213                       0    0    COLUMN importdata3.f062    COMMENT     O   COMMENT ON COLUMN public.importdata3.f062 IS '給付点検補綴時診断料';
            public       postgres    false    213                       0    0    COLUMN importdata3.f063    COMMENT     X   COMMENT ON COLUMN public.importdata3.f063 IS '給付点検特定疾患療養指導料';
            public       postgres    false    213                       0    0    COLUMN importdata3.f064    COMMENT     ^   COMMENT ON COLUMN public.importdata3.f064 IS '給付点検老人慢性生活指導管理料';
            public       postgres    false    213                       0    0    COLUMN importdata3.f065    COMMENT     ^   COMMENT ON COLUMN public.importdata3.f065 IS '給付点検歯周疾患継続指導管理料';
            public       postgres    false    213                       0    0    COLUMN importdata3.f066    COMMENT     X   COMMENT ON COLUMN public.importdata3.f066 IS '給付点検特定薬剤治療管理料';
            public       postgres    false    213                       0    0    COLUMN importdata3.f067    COMMENT     d   COMMENT ON COLUMN public.importdata3.f067 IS '給付点検悪性腫瘍特異物質治療管理料';
            public       postgres    false    213                       0    0    COLUMN importdata3.f068    COMMENT     U   COMMENT ON COLUMN public.importdata3.f068 IS '給付点検小児科療養指導料';
            public       postgres    false    213                        0    0    COLUMN importdata3.f069    COMMENT     R   COMMENT ON COLUMN public.importdata3.f069 IS '給付点検てんかん指導料';
            public       postgres    false    213            !           0    0    COLUMN importdata3.f070    COMMENT     X   COMMENT ON COLUMN public.importdata3.f070 IS '給付点検難病外来指導管理料';
            public       postgres    false    213            "           0    0    COLUMN importdata3.f071    COMMENT     a   COMMENT ON COLUMN public.importdata3.f071 IS '給付点検皮膚科特定疾患指導管理料';
            public       postgres    false    213            #           0    0    COLUMN importdata3.f072    COMMENT     R   COMMENT ON COLUMN public.importdata3.f072 IS '給付点検在宅指導管理料';
            public       postgres    false    213            $           0    0    COLUMN importdata3.f073    COMMENT     ^   COMMENT ON COLUMN public.importdata3.f073 IS '給付点検歯科補綴関連検査（ChB）';
            public       postgres    false    213            %           0    0    COLUMN importdata3.f074    COMMENT     ^   COMMENT ON COLUMN public.importdata3.f074 IS '給付点検歯科補綴関連検査（GoA）';
            public       postgres    false    213            &           0    0    COLUMN importdata3.f075    COMMENT     ^   COMMENT ON COLUMN public.importdata3.f075 IS '給付点検歯科補綴関連検査（PTG）';
            public       postgres    false    213            '           0    0    COLUMN importdata3.f076    COMMENT     d   COMMENT ON COLUMN public.importdata3.f076 IS '給付点検寝たきり老人訪問指導管理料';
            public       postgres    false    213            (           0    0    COLUMN importdata3.f077    COMMENT     L   COMMENT ON COLUMN public.importdata3.f077 IS '給付点検退院指導料';
            public       postgres    false    213            )           0    0    COLUMN importdata3.f078    COMMENT     R   COMMENT ON COLUMN public.importdata3.f078 IS '給付点検薬剤管理指導料';
            public       postgres    false    213            *           0    0    COLUMN importdata3.f079    COMMENT     ^   COMMENT ON COLUMN public.importdata3.f079 IS '給付点検特定疾患療養指導料査定';
            public       postgres    false    213            +           0    0    COLUMN importdata3.f080    COMMENT     d   COMMENT ON COLUMN public.importdata3.f080 IS '給付点検老人慢性生活指導管理料査定';
            public       postgres    false    213            ,           0    0    COLUMN importdata3.f081    COMMENT     �   COMMENT ON COLUMN public.importdata3.f081 IS '給付点検在宅訪問ﾘﾊﾋﾞﾘﾃｰｼｮﾝ指導管理料（医科）';
            public       postgres    false    213            -           0    0    COLUMN importdata3.f082    COMMENT     p   COMMENT ON COLUMN public.importdata3.f082 IS '給付点検在宅患者訪問薬剤管理指導料（医科）';
            public       postgres    false    213            .           0    0    COLUMN importdata3.f083    COMMENT     p   COMMENT ON COLUMN public.importdata3.f083 IS '給付点検在宅患者訪問栄養食事指導料（医科）';
            public       postgres    false    213            /           0    0    COLUMN importdata3.f084    COMMENT     j   COMMENT ON COLUMN public.importdata3.f084 IS '給付点検老人訪問口腔指導管理料（歯科）';
            public       postgres    false    213            0           0    0    COLUMN importdata3.f085    COMMENT     d   COMMENT ON COLUMN public.importdata3.f085 IS '給付点検訪問歯科衛生指導料（歯科）';
            public       postgres    false    213            1           0    0    COLUMN importdata3.f086    COMMENT     p   COMMENT ON COLUMN public.importdata3.f086 IS '給付点検在宅患者訪問薬剤管理指導料（歯科）';
            public       postgres    false    213            2           0    0    COLUMN importdata3.f087    COMMENT     p   COMMENT ON COLUMN public.importdata3.f087 IS '給付点検在宅患者訪問薬剤管理指導料（調剤）';
            public       postgres    false    213            3           0    0    COLUMN importdata3.f088    COMMENT     j   COMMENT ON COLUMN public.importdata3.f088 IS '給付点検基本療養費Ⅰ（訪問看護療養費）';
            public       postgres    false    213            4           0    0    COLUMN importdata3.f089    COMMENT     g   COMMENT ON COLUMN public.importdata3.f089 IS '給付点検管理療養費（訪問看護療養費）';
            public       postgres    false    213            5           0    0    COLUMN importdata3.f090    COMMENT     d   COMMENT ON COLUMN public.importdata3.f090 IS '給付点検在宅時医学総合管理料算定者';
            public       postgres    false    213            6           0    0    COLUMN importdata3.f091    COMMENT     [   COMMENT ON COLUMN public.importdata3.f091 IS '給付点検処方箋料算定有無情報';
            public       postgres    false    213            7           0    0    COLUMN importdata3.f092    COMMENT     X   COMMENT ON COLUMN public.importdata3.f092 IS '連合会任意連合会任意項目１';
            public       postgres    false    213            8           0    0    COLUMN importdata3.f093    COMMENT     X   COMMENT ON COLUMN public.importdata3.f093 IS '連合会任意連合会任意項目２';
            public       postgres    false    213            9           0    0    COLUMN importdata3.f094    COMMENT     X   COMMENT ON COLUMN public.importdata3.f094 IS '連合会任意連合会任意項目３';
            public       postgres    false    213            :           0    0    COLUMN importdata3.f095    COMMENT     g   COMMENT ON COLUMN public.importdata3.f095 IS '資格設定情報個人特定情報被保険者氏名';
            public       postgres    false    213            ;           0    0    COLUMN importdata3.f096    COMMENT     a   COMMENT ON COLUMN public.importdata3.f096 IS '資格設定情報個人特定情報生年月日';
            public       postgres    false    213            <           0    0    COLUMN importdata3.f097    COMMENT     p   COMMENT ON COLUMN public.importdata3.f097 IS '資格設定情報個人特定情報被保険者住所コード';
            public       postgres    false    213            =           0    0    COLUMN importdata3.f098    COMMENT     p   COMMENT ON COLUMN public.importdata3.f098 IS '資格設定情報個人特定情報被保険者地区コード';
            public       postgres    false    213            >           0    0    COLUMN importdata3.f099    COMMENT     g   COMMENT ON COLUMN public.importdata3.f099 IS '資格設定情報個人特定情報世帯管理番号';
            public       postgres    false    213            ?           0    0    COLUMN importdata3.f100    COMMENT     g   COMMENT ON COLUMN public.importdata3.f100 IS '資格設定情報個人特定情報個人管理番号';
            public       postgres    false    213            @           0    0    COLUMN importdata3.f101    COMMENT     s   COMMENT ON COLUMN public.importdata3.f101 IS '資格設定情報個人特定情報被保険者氏名（カナ）';
            public       postgres    false    213            A           0    0    COLUMN importdata3.f102    COMMENT     t   COMMENT ON COLUMN public.importdata3.f102 IS '資格設定情報個人特定情報直近情報_国保喪失事由';
            public       postgres    false    213            B           0    0    COLUMN importdata3.f103    COMMENT     g   COMMENT ON COLUMN public.importdata3.f103 IS '資格設定情報個人特定情報退職続柄区分';
            public       postgres    false    213            C           0    0    COLUMN importdata3.f104    COMMENT     C   COMMENT ON COLUMN public.importdata3.f104 IS '件数論理件数';
            public       postgres    false    213            D           0    0    COLUMN importdata3.f105    COMMENT     C   COMMENT ON COLUMN public.importdata3.f105 IS '件数物理件数';
            public       postgres    false    213            E           0    0    COLUMN importdata3.f106    COMMENT     O   COMMENT ON COLUMN public.importdata3.f106 IS '給付割合保険給付割合';
            public       postgres    false    213            F           0    0    COLUMN importdata3.f107    COMMENT     S   COMMENT ON COLUMN public.importdata3.f107 IS '請求医療機関機関_点数表';
            public       postgres    false    213            G           0    0    COLUMN importdata3.f108    COMMENT     _   COMMENT ON COLUMN public.importdata3.f108 IS '請求医療機関機関_都道府県コード';
            public       postgres    false    213            H           0    0    COLUMN importdata3.f109    COMMENT     Y   COMMENT ON COLUMN public.importdata3.f109 IS '請求医療機関機関_県内外区分';
            public       postgres    false    213            I           0    0    COLUMN importdata3.f110    COMMENT     S   COMMENT ON COLUMN public.importdata3.f110 IS '請求医療機関機関_診療科';
            public       postgres    false    213            J           0    0    COLUMN importdata3.f111    COMMENT     P   COMMENT ON COLUMN public.importdata3.f111 IS '給付割合公１_給付割合';
            public       postgres    false    213            K           0    0    COLUMN importdata3.f112    COMMENT     P   COMMENT ON COLUMN public.importdata3.f112 IS '給付割合公１_任意給付';
            public       postgres    false    213            L           0    0    COLUMN importdata3.f113    COMMENT     P   COMMENT ON COLUMN public.importdata3.f113 IS '給付割合公２_給付割合';
            public       postgres    false    213            M           0    0    COLUMN importdata3.f114    COMMENT     P   COMMENT ON COLUMN public.importdata3.f114 IS '給付割合公２_任意給付';
            public       postgres    false    213            N           0    0    COLUMN importdata3.f115    COMMENT     P   COMMENT ON COLUMN public.importdata3.f115 IS '給付割合公３_給付割合';
            public       postgres    false    213            O           0    0    COLUMN importdata3.f116    COMMENT     P   COMMENT ON COLUMN public.importdata3.f116 IS '給付割合公３_任意給付';
            public       postgres    false    213            P           0    0    COLUMN importdata3.f117    COMMENT     P   COMMENT ON COLUMN public.importdata3.f117 IS '給付割合公４_給付割合';
            public       postgres    false    213            Q           0    0    COLUMN importdata3.f118    COMMENT     P   COMMENT ON COLUMN public.importdata3.f118 IS '給付割合公４_任意給付';
            public       postgres    false    213            R           0    0    COLUMN importdata3.f119    COMMENT     P   COMMENT ON COLUMN public.importdata3.f119 IS '給付割合公５_給付割合';
            public       postgres    false    213            S           0    0    COLUMN importdata3.f120    COMMENT     P   COMMENT ON COLUMN public.importdata3.f120 IS '給付割合公５_任意給付';
            public       postgres    false    213            T           0    0    COLUMN importdata3.f121    COMMENT     e   COMMENT ON COLUMN public.importdata3.f121 IS '療養の給付等（保険）保険_診療実日数';
            public       postgres    false    213            U           0    0    COLUMN importdata3.f122    COMMENT     b   COMMENT ON COLUMN public.importdata3.f122 IS '療養の給付等（保険）保険_決定点数';
            public       postgres    false    213            V           0    0    COLUMN importdata3.f123    COMMENT     g   COMMENT ON COLUMN public.importdata3.f123 IS '療養の給付等（保険）（公費負担金額）';
            public       postgres    false    213            W           0    0    COLUMN importdata3.f124    COMMENT     g   COMMENT ON COLUMN public.importdata3.f124 IS '療養の給付等（保険）（公２負担金額）';
            public       postgres    false    213            X           0    0    COLUMN importdata3.f125    COMMENT     g   COMMENT ON COLUMN public.importdata3.f125 IS '療養の給付等（保険）（公３負担金額）';
            public       postgres    false    213            Y           0    0    COLUMN importdata3.f126    COMMENT     g   COMMENT ON COLUMN public.importdata3.f126 IS '療養の給付等（保険）（公４負担金額）';
            public       postgres    false    213            Z           0    0    COLUMN importdata3.f127    COMMENT     g   COMMENT ON COLUMN public.importdata3.f127 IS '療養の給付等（保険）（公５負担金額）';
            public       postgres    false    213            [           0    0    COLUMN importdata3.f128    COMMENT     e   COMMENT ON COLUMN public.importdata3.f128 IS '療養の給付等（保険）保険_一部負担金';
            public       postgres    false    213            \           0    0    COLUMN importdata3.f129    COMMENT     h   COMMENT ON COLUMN public.importdata3.f129 IS '療養の給付等（公費１）公１_負担者番号';
            public       postgres    false    213            ]           0    0    COLUMN importdata3.f130    COMMENT     h   COMMENT ON COLUMN public.importdata3.f130 IS '療養の給付等（公費１）公１_受給者番号';
            public       postgres    false    213            ^           0    0    COLUMN importdata3.f131    COMMENT     h   COMMENT ON COLUMN public.importdata3.f131 IS '療養の給付等（公費１）公１_診療実日数';
            public       postgres    false    213            _           0    0    COLUMN importdata3.f132    COMMENT     e   COMMENT ON COLUMN public.importdata3.f132 IS '療養の給付等（公費１）公１_決定点数';
            public       postgres    false    213            `           0    0    COLUMN importdata3.f133    COMMENT     e   COMMENT ON COLUMN public.importdata3.f133 IS '療養の給付等（公費１）公１_増減点数';
            public       postgres    false    213            a           0    0    COLUMN importdata3.f134    COMMENT     h   COMMENT ON COLUMN public.importdata3.f134 IS '療養の給付等（公費１）公１_一部負担金';
            public       postgres    false    213            b           0    0    COLUMN importdata3.f135    COMMENT     h   COMMENT ON COLUMN public.importdata3.f135 IS '療養の給付等（公費２）公２_負担者番号';
            public       postgres    false    213            c           0    0    COLUMN importdata3.f136    COMMENT     h   COMMENT ON COLUMN public.importdata3.f136 IS '療養の給付等（公費２）公２_受給者番号';
            public       postgres    false    213            d           0    0    COLUMN importdata3.f137    COMMENT     h   COMMENT ON COLUMN public.importdata3.f137 IS '療養の給付等（公費２）公２_診療実日数';
            public       postgres    false    213            e           0    0    COLUMN importdata3.f138    COMMENT     e   COMMENT ON COLUMN public.importdata3.f138 IS '療養の給付等（公費２）公２_決定点数';
            public       postgres    false    213            f           0    0    COLUMN importdata3.f139    COMMENT     e   COMMENT ON COLUMN public.importdata3.f139 IS '療養の給付等（公費２）公２_増減点数';
            public       postgres    false    213            g           0    0    COLUMN importdata3.f140    COMMENT     h   COMMENT ON COLUMN public.importdata3.f140 IS '療養の給付等（公費２）公２_一部負担金';
            public       postgres    false    213            h           0    0    COLUMN importdata3.f141    COMMENT     h   COMMENT ON COLUMN public.importdata3.f141 IS '療養の給付等（公費３）公３_負担者番号';
            public       postgres    false    213            i           0    0    COLUMN importdata3.f142    COMMENT     h   COMMENT ON COLUMN public.importdata3.f142 IS '療養の給付等（公費３）公３_受給者番号';
            public       postgres    false    213            j           0    0    COLUMN importdata3.f143    COMMENT     h   COMMENT ON COLUMN public.importdata3.f143 IS '療養の給付等（公費３）公３_診療実日数';
            public       postgres    false    213            k           0    0    COLUMN importdata3.f144    COMMENT     e   COMMENT ON COLUMN public.importdata3.f144 IS '療養の給付等（公費３）公３_決定点数';
            public       postgres    false    213            l           0    0    COLUMN importdata3.f145    COMMENT     e   COMMENT ON COLUMN public.importdata3.f145 IS '療養の給付等（公費３）公３_増減点数';
            public       postgres    false    213            m           0    0    COLUMN importdata3.f146    COMMENT     h   COMMENT ON COLUMN public.importdata3.f146 IS '療養の給付等（公費３）公３_一部負担金';
            public       postgres    false    213            n           0    0    COLUMN importdata3.f147    COMMENT     h   COMMENT ON COLUMN public.importdata3.f147 IS '療養の給付等（公費４）公４_負担者番号';
            public       postgres    false    213            o           0    0    COLUMN importdata3.f148    COMMENT     h   COMMENT ON COLUMN public.importdata3.f148 IS '療養の給付等（公費４）公４_受給者番号';
            public       postgres    false    213            p           0    0    COLUMN importdata3.f149    COMMENT     h   COMMENT ON COLUMN public.importdata3.f149 IS '療養の給付等（公費４）公４_診療実日数';
            public       postgres    false    213            q           0    0    COLUMN importdata3.f150    COMMENT     e   COMMENT ON COLUMN public.importdata3.f150 IS '療養の給付等（公費４）公４_決定点数';
            public       postgres    false    213            r           0    0    COLUMN importdata3.f151    COMMENT     e   COMMENT ON COLUMN public.importdata3.f151 IS '療養の給付等（公費４）公４_増減点数';
            public       postgres    false    213            s           0    0    COLUMN importdata3.f152    COMMENT     h   COMMENT ON COLUMN public.importdata3.f152 IS '療養の給付等（公費４）公４_一部負担金';
            public       postgres    false    213            t           0    0    COLUMN importdata3.f153    COMMENT     h   COMMENT ON COLUMN public.importdata3.f153 IS '療養の給付等（公費５）公５_負担者番号';
            public       postgres    false    213            u           0    0    COLUMN importdata3.f154    COMMENT     h   COMMENT ON COLUMN public.importdata3.f154 IS '療養の給付等（公費５）公５_受給者番号';
            public       postgres    false    213            v           0    0    COLUMN importdata3.f155    COMMENT     h   COMMENT ON COLUMN public.importdata3.f155 IS '療養の給付等（公費５）公５_診療実日数';
            public       postgres    false    213            w           0    0    COLUMN importdata3.f156    COMMENT     e   COMMENT ON COLUMN public.importdata3.f156 IS '療養の給付等（公費５）公５_決定点数';
            public       postgres    false    213            x           0    0    COLUMN importdata3.f157    COMMENT     e   COMMENT ON COLUMN public.importdata3.f157 IS '療養の給付等（公費５）公５_増減点数';
            public       postgres    false    213            y           0    0    COLUMN importdata3.f158    COMMENT     h   COMMENT ON COLUMN public.importdata3.f158 IS '療養の給付等（公費５）公５_一部負担金';
            public       postgres    false    213            z           0    0    COLUMN importdata3.f159    COMMENT     V   COMMENT ON COLUMN public.importdata3.f159 IS '食事（保険）保険_食事回数';
            public       postgres    false    213            {           0    0    COLUMN importdata3.f160    COMMENT     _   COMMENT ON COLUMN public.importdata3.f160 IS '食事（保険）保険_食事決定基準額';
            public       postgres    false    213            |           0    0    COLUMN importdata3.f161    COMMENT     _   COMMENT ON COLUMN public.importdata3.f161 IS '食事（保険）保険_食事標準負担額';
            public       postgres    false    213            }           0    0    COLUMN importdata3.f162    COMMENT     Y   COMMENT ON COLUMN public.importdata3.f162 IS '食事（公費１）公１_食事回数';
            public       postgres    false    213            ~           0    0    COLUMN importdata3.f163    COMMENT     b   COMMENT ON COLUMN public.importdata3.f163 IS '食事（公費１）公１_食事決定基準額';
            public       postgres    false    213                       0    0    COLUMN importdata3.f164    COMMENT     b   COMMENT ON COLUMN public.importdata3.f164 IS '食事（公費１）公１_食事増減基準額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f165    COMMENT     b   COMMENT ON COLUMN public.importdata3.f165 IS '食事（公費１）公１_食事標準負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f166    COMMENT     h   COMMENT ON COLUMN public.importdata3.f166 IS '食事（公費１）公１_食事増減標準負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f167    COMMENT     Y   COMMENT ON COLUMN public.importdata3.f167 IS '食事（公費２）公２_食事回数';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f168    COMMENT     b   COMMENT ON COLUMN public.importdata3.f168 IS '食事（公費２）公２_食事決定基準額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f169    COMMENT     b   COMMENT ON COLUMN public.importdata3.f169 IS '食事（公費２）公２_食事増減基準額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f170    COMMENT     b   COMMENT ON COLUMN public.importdata3.f170 IS '食事（公費２）公２_食事標準負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f171    COMMENT     h   COMMENT ON COLUMN public.importdata3.f171 IS '食事（公費２）公２_食事増減標準負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f172    COMMENT     Y   COMMENT ON COLUMN public.importdata3.f172 IS '食事（公費３）公３_食事回数';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f173    COMMENT     b   COMMENT ON COLUMN public.importdata3.f173 IS '食事（公費３）公３_食事決定基準額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f174    COMMENT     b   COMMENT ON COLUMN public.importdata3.f174 IS '食事（公費３）公３_食事増減基準額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f175    COMMENT     b   COMMENT ON COLUMN public.importdata3.f175 IS '食事（公費３）公３_食事標準負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f176    COMMENT     h   COMMENT ON COLUMN public.importdata3.f176 IS '食事（公費３）公３_食事増減標準負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f177    COMMENT     Y   COMMENT ON COLUMN public.importdata3.f177 IS '食事（公費４）公４_食事回数';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f178    COMMENT     b   COMMENT ON COLUMN public.importdata3.f178 IS '食事（公費４）公４_食事決定基準額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f179    COMMENT     b   COMMENT ON COLUMN public.importdata3.f179 IS '食事（公費４）公４_食事増減基準額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f180    COMMENT     b   COMMENT ON COLUMN public.importdata3.f180 IS '食事（公費４）公４_食事標準負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f181    COMMENT     h   COMMENT ON COLUMN public.importdata3.f181 IS '食事（公費４）公４_食事増減標準負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f182    COMMENT     Y   COMMENT ON COLUMN public.importdata3.f182 IS '食事（公費５）公５_食事回数';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f183    COMMENT     b   COMMENT ON COLUMN public.importdata3.f183 IS '食事（公費５）公５_食事決定基準額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f184    COMMENT     b   COMMENT ON COLUMN public.importdata3.f184 IS '食事（公費５）公５_食事増減基準額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f185    COMMENT     b   COMMENT ON COLUMN public.importdata3.f185 IS '食事（公費５）公５_食事標準負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f186    COMMENT     h   COMMENT ON COLUMN public.importdata3.f186 IS '食事（公費５）公５_食事増減標準負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f187    COMMENT     F   COMMENT ON COLUMN public.importdata3.f187 IS 'その他所得区分';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f188    COMMENT     L   COMMENT ON COLUMN public.importdata3.f188 IS 'その他本人家族区分';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f189    COMMENT     L   COMMENT ON COLUMN public.importdata3.f189 IS 'その他入院外来区分';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f190    COMMENT     U   COMMENT ON COLUMN public.importdata3.f190 IS 'その他在総診・在医総区分';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f191    COMMENT     =   COMMENT ON COLUMN public.importdata3.f191 IS 'その他Ⅰ';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f192    COMMENT     =   COMMENT ON COLUMN public.importdata3.f192 IS 'その他Ⅱ';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f193    COMMENT     C   COMMENT ON COLUMN public.importdata3.f193 IS 'その他三月超';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f194    COMMENT     F   COMMENT ON COLUMN public.importdata3.f194 IS 'その他多数該当';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f195    COMMENT     L   COMMENT ON COLUMN public.importdata3.f195 IS 'その他経過措置区分';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f196    COMMENT     R   COMMENT ON COLUMN public.importdata3.f196 IS 'その他市町村保険者変更';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f197    COMMENT     I   COMMENT ON COLUMN public.importdata3.f197 IS 'その他特別療養費';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f198    COMMENT     I   COMMENT ON COLUMN public.importdata3.f198 IS 'その他マル公区分';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f199    COMMENT     F   COMMENT ON COLUMN public.importdata3.f199 IS 'その他長期区分';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f200    COMMENT     X   COMMENT ON COLUMN public.importdata3.f200 IS 'その他限度額適用認定証区分';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f201    COMMENT     L   COMMENT ON COLUMN public.importdata3.f201 IS 'その他前期該当区分';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f202    COMMENT     I   COMMENT ON COLUMN public.importdata3.f202 IS 'その他二割徴収者';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f203    COMMENT     L   COMMENT ON COLUMN public.importdata3.f203 IS 'その他月中該当区分';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f204    COMMENT     f   COMMENT ON COLUMN public.importdata3.f204 IS '費用算定結果値保険算定_保険_決定点数';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f205    COMMENT     c   COMMENT ON COLUMN public.importdata3.f205 IS '費用算定結果値保険算定_保険_費用額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f206    COMMENT     l   COMMENT ON COLUMN public.importdata3.f206 IS '費用算定結果値保険算定_保険_負担者負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f207    COMMENT     i   COMMENT ON COLUMN public.importdata3.f207 IS '費用算定結果値保険算定_保険_高額療養費';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f208    COMMENT     o   COMMENT ON COLUMN public.importdata3.f208 IS '費用算定結果値保険算定_保険_長期高額療養費';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f209    COMMENT     i   COMMENT ON COLUMN public.importdata3.f209 IS '費用算定結果値保険算定_保険_患者負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f210    COMMENT     u   COMMENT ON COLUMN public.importdata3.f210 IS '費用算定結果値保険算定_保険_他法優先公費負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f211    COMMENT     u   COMMENT ON COLUMN public.importdata3.f211 IS '費用算定結果値保険算定_保険_国保優先公費負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f212    COMMENT     i   COMMENT ON COLUMN public.importdata3.f212 IS '費用算定結果値保険算定_保険_任意給付額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f213    COMMENT     i   COMMENT ON COLUMN public.importdata3.f213 IS '費用算定結果値保険算定_保険_減免猶予額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f214    COMMENT     i   COMMENT ON COLUMN public.importdata3.f214 IS '費用算定結果値保険算定_保険_食事基準額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f215    COMMENT     r   COMMENT ON COLUMN public.importdata3.f215 IS '費用算定結果値保険算定_保険_食事負担者負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f216    COMMENT     o   COMMENT ON COLUMN public.importdata3.f216 IS '費用算定結果値保険算定_保険_食事患者負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f217    COMMENT     {   COMMENT ON COLUMN public.importdata3.f217 IS '費用算定結果値保険算定_保険_食事他法優先公費負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f218    COMMENT     {   COMMENT ON COLUMN public.importdata3.f218 IS '費用算定結果値保険算定_保険_食事国保優先公費負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f219    COMMENT     i   COMMENT ON COLUMN public.importdata3.f219 IS '費用算定結果値公費１算定_公１_決定点数';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f220    COMMENT     f   COMMENT ON COLUMN public.importdata3.f220 IS '費用算定結果値公費１算定_公１_費用額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f221    COMMENT     r   COMMENT ON COLUMN public.importdata3.f221 IS '費用算定結果値公費１算定_公１_負担者負担金額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f222    COMMENT     l   COMMENT ON COLUMN public.importdata3.f222 IS '費用算定結果値公費１算定_公１_高額療養費';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f223    COMMENT     r   COMMENT ON COLUMN public.importdata3.f223 IS '費用算定結果値公費１算定_公１_長期高額療養費';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f224    COMMENT     l   COMMENT ON COLUMN public.importdata3.f224 IS '費用算定結果値公費１算定_公１_患者負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f225    COMMENT     l   COMMENT ON COLUMN public.importdata3.f225 IS '費用算定結果値公費１算定_公１_任意給付額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f226    COMMENT     l   COMMENT ON COLUMN public.importdata3.f226 IS '費用算定結果値公費１算定_公１_食事基準額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f227    COMMENT     u   COMMENT ON COLUMN public.importdata3.f227 IS '費用算定結果値公費１算定_公１_食事負担者負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f228    COMMENT     r   COMMENT ON COLUMN public.importdata3.f228 IS '費用算定結果値公費１算定_公１_食事患者負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f229    COMMENT     i   COMMENT ON COLUMN public.importdata3.f229 IS '費用算定結果値公費２算定_公２_決定点数';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f230    COMMENT     f   COMMENT ON COLUMN public.importdata3.f230 IS '費用算定結果値公費２算定_公２_費用額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f231    COMMENT     r   COMMENT ON COLUMN public.importdata3.f231 IS '費用算定結果値公費２算定_公２_負担者負担金額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f232    COMMENT     l   COMMENT ON COLUMN public.importdata3.f232 IS '費用算定結果値公費２算定_公２_高額療養費';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f233    COMMENT     r   COMMENT ON COLUMN public.importdata3.f233 IS '費用算定結果値公費２算定_公２_長期高額療養費';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f234    COMMENT     l   COMMENT ON COLUMN public.importdata3.f234 IS '費用算定結果値公費２算定_公２_患者負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f235    COMMENT     l   COMMENT ON COLUMN public.importdata3.f235 IS '費用算定結果値公費２算定_公２_任意給付額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f236    COMMENT     l   COMMENT ON COLUMN public.importdata3.f236 IS '費用算定結果値公費２算定_公２_食事基準額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f237    COMMENT     u   COMMENT ON COLUMN public.importdata3.f237 IS '費用算定結果値公費２算定_公２_食事負担者負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f238    COMMENT     r   COMMENT ON COLUMN public.importdata3.f238 IS '費用算定結果値公費２算定_公２_食事患者負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f239    COMMENT     i   COMMENT ON COLUMN public.importdata3.f239 IS '費用算定結果値公費３算定_公３_決定点数';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f240    COMMENT     f   COMMENT ON COLUMN public.importdata3.f240 IS '費用算定結果値公費３算定_公３_費用額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f241    COMMENT     r   COMMENT ON COLUMN public.importdata3.f241 IS '費用算定結果値公費３算定_公３_負担者負担金額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f242    COMMENT     l   COMMENT ON COLUMN public.importdata3.f242 IS '費用算定結果値公費３算定_公３_高額療養費';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f243    COMMENT     r   COMMENT ON COLUMN public.importdata3.f243 IS '費用算定結果値公費３算定_公３_長期高額療養費';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f244    COMMENT     l   COMMENT ON COLUMN public.importdata3.f244 IS '費用算定結果値公費３算定_公３_患者負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f245    COMMENT     l   COMMENT ON COLUMN public.importdata3.f245 IS '費用算定結果値公費３算定_公３_任意給付額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f246    COMMENT     l   COMMENT ON COLUMN public.importdata3.f246 IS '費用算定結果値公費３算定_公３_食事基準額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f247    COMMENT     u   COMMENT ON COLUMN public.importdata3.f247 IS '費用算定結果値公費３算定_公３_食事負担者負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f248    COMMENT     r   COMMENT ON COLUMN public.importdata3.f248 IS '費用算定結果値公費３算定_公３_食事患者負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f249    COMMENT     i   COMMENT ON COLUMN public.importdata3.f249 IS '費用算定結果値公費４算定_公４_決定点数';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f250    COMMENT     f   COMMENT ON COLUMN public.importdata3.f250 IS '費用算定結果値公費４算定_公４_費用額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f251    COMMENT     r   COMMENT ON COLUMN public.importdata3.f251 IS '費用算定結果値公費４算定_公４_負担者負担金額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f252    COMMENT     l   COMMENT ON COLUMN public.importdata3.f252 IS '費用算定結果値公費４算定_公４_高額療養費';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f253    COMMENT     r   COMMENT ON COLUMN public.importdata3.f253 IS '費用算定結果値公費４算定_公４_長期高額療養費';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f254    COMMENT     l   COMMENT ON COLUMN public.importdata3.f254 IS '費用算定結果値公費４算定_公４_患者負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f255    COMMENT     l   COMMENT ON COLUMN public.importdata3.f255 IS '費用算定結果値公費４算定_公４_任意給付額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f256    COMMENT     l   COMMENT ON COLUMN public.importdata3.f256 IS '費用算定結果値公費４算定_公４_食事基準額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f257    COMMENT     u   COMMENT ON COLUMN public.importdata3.f257 IS '費用算定結果値公費４算定_公４_食事負担者負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f258    COMMENT     r   COMMENT ON COLUMN public.importdata3.f258 IS '費用算定結果値公費４算定_公４_食事患者負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f259    COMMENT     i   COMMENT ON COLUMN public.importdata3.f259 IS '費用算定結果値公費５算定_公５_決定点数';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f260    COMMENT     f   COMMENT ON COLUMN public.importdata3.f260 IS '費用算定結果値公費５算定_公５_費用額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f261    COMMENT     r   COMMENT ON COLUMN public.importdata3.f261 IS '費用算定結果値公費５算定_公５_負担者負担金額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f262    COMMENT     l   COMMENT ON COLUMN public.importdata3.f262 IS '費用算定結果値公費５算定_公５_高額療養費';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f263    COMMENT     r   COMMENT ON COLUMN public.importdata3.f263 IS '費用算定結果値公費５算定_公５_長期高額療養費';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f264    COMMENT     l   COMMENT ON COLUMN public.importdata3.f264 IS '費用算定結果値公費５算定_公５_患者負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f265    COMMENT     l   COMMENT ON COLUMN public.importdata3.f265 IS '費用算定結果値公費５算定_公５_任意給付額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f266    COMMENT     l   COMMENT ON COLUMN public.importdata3.f266 IS '費用算定結果値公費５算定_公５_食事基準額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f267    COMMENT     u   COMMENT ON COLUMN public.importdata3.f267 IS '費用算定結果値公費５算定_公５_食事負担者負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f268    COMMENT     r   COMMENT ON COLUMN public.importdata3.f268 IS '費用算定結果値公費５算定_公５_食事患者負担額';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f269    COMMENT     U   COMMENT ON COLUMN public.importdata3.f269 IS 'エラー制御重複エラー区分';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f270    COMMENT     g   COMMENT ON COLUMN public.importdata3.f270 IS 'エラー制御資格チェックエラー項目情報';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f271    COMMENT     d   COMMENT ON COLUMN public.importdata3.f271 IS 'エラー制御資格チェックエラーコード';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f272    COMMENT     d   COMMENT ON COLUMN public.importdata3.f272 IS 'エラー制御給付チェックエラーコード';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f273    COMMENT     v   COMMENT ON COLUMN public.importdata3.f273 IS '療養費窓口申請分管理項目領域療_医療機関名(漢字)';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f274    COMMENT     |   COMMENT ON COLUMN public.importdata3.f274 IS '療養費窓口申請分管理項目領域療_柔整団体機関名(漢字)';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f275    COMMENT     t   COMMENT ON COLUMN public.importdata3.f275 IS '療養費窓口申請分管理項目領域療_支給決定年月日';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f276    COMMENT     n   COMMENT ON COLUMN public.importdata3.f276 IS '療養費窓口申請分管理項目領域療_高額療養費';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f277    COMMENT     t   COMMENT ON COLUMN public.importdata3.f277 IS '療養費窓口申請分管理項目領域療_振込銀行コード';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f278    COMMENT     t   COMMENT ON COLUMN public.importdata3.f278 IS '療養費窓口申請分管理項目領域療_振込支店コード';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f279    COMMENT     k   COMMENT ON COLUMN public.importdata3.f279 IS '療養費窓口申請分管理項目領域療_預金種目';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f280    COMMENT     z   COMMENT ON COLUMN public.importdata3.f280 IS '療養費窓口申請分管理項目領域療_口座名義人（カナ）';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f281    COMMENT     z   COMMENT ON COLUMN public.importdata3.f281 IS '療養費窓口申請分管理項目領域療_口座名義人（漢字）';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f282    COMMENT     g   COMMENT ON COLUMN public.importdata3.f282 IS '過誤再審査情報過誤・再審査データ区分';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f283    COMMENT     d   COMMENT ON COLUMN public.importdata3.f283 IS '過誤再審査情報過誤・再審査管理年月';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f284    COMMENT     d   COMMENT ON COLUMN public.importdata3.f284 IS '過誤再審査情報過誤・再審査理由番号';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f285    COMMENT     d   COMMENT ON COLUMN public.importdata3.f285 IS '過誤再審査情報過誤・再審査審査結果';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f286    COMMENT     R   COMMENT ON COLUMN public.importdata3.f286 IS '過誤再審査情報過誤区分';
            public       postgres    false    213            �           0    0    COLUMN importdata3.f287    COMMENT     d   COMMENT ON COLUMN public.importdata3.f287 IS '過誤再審査情報過誤・再審査結果年月';
            public       postgres    false    213            �            1259    419455    importkyufu_id_seq    SEQUENCE     {   CREATE SEQUENCE public.importkyufu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.importkyufu_id_seq;
       public       postgres    false            �            1259    419184    ocr_ordered    TABLE     >   CREATE TABLE public.ocr_ordered (
    sid integer NOT NULL
);
    DROP TABLE public.ocr_ordered;
       public         postgres    false            �            1259    419457    refrece    TABLE     �  CREATE TABLE public.refrece (
    rrid integer NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    num text DEFAULT ''::text NOT NULL,
    name text DEFAULT ''::text NOT NULL,
    kana text DEFAULT ''::text NOT NULL,
    zip text DEFAULT ''::text NOT NULL,
    add text DEFAULT ''::text NOT NULL,
    destzip text DEFAULT ''::text NOT NULL,
    destadd text DEFAULT ''::text NOT NULL,
    destname text DEFAULT ''::text NOT NULL,
    clinicnum text DEFAULT ''::text NOT NULL,
    clinicname text DEFAULT ''::text NOT NULL,
    days integer DEFAULT 0 NOT NULL,
    total integer DEFAULT 0 NOT NULL,
    comnum text DEFAULT ''::text NOT NULL,
    aid integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.refrece;
       public         postgres    false            �            1259    419187    scan    TABLE     �   CREATE TABLE public.scan (
    sid integer NOT NULL,
    scandate date,
    cyear integer,
    cmonth integer,
    note1 text,
    note2 text,
    status integer DEFAULT 0,
    apptype integer DEFAULT 0 NOT NULL,
    cym integer
);
    DROP TABLE public.scan;
       public         postgres    false            �            1259    419195    scan_sid_seq    SEQUENCE     u   CREATE SEQUENCE public.scan_sid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.scan_sid_seq;
       public       postgres    false    199            �           0    0    scan_sid_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.scan_sid_seq OWNED BY public.scan.sid;
            public       postgres    false    200            �            1259    419197 	   scangroup    TABLE       CREATE TABLE public.scangroup (
    groupid integer NOT NULL,
    status integer,
    scanid integer,
    scandate date,
    scanuser integer,
    checkdate date,
    checkuser integer,
    inquirydate date,
    inquiryuser integer,
    workingusers text DEFAULT ''::text NOT NULL
);
    DROP TABLE public.scangroup;
       public         postgres    false            �            1259    419204    shokaiexclude    TABLE     )  CREATE TABLE public.shokaiexclude (
    excludeid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    mediym integer DEFAULT 0 NOT NULL,
    chargeym integer DEFAULT 0 NOT NULL,
    hihonum text DEFAULT ''::text NOT NULL,
    birth date DEFAULT '0001-01-01'::date NOT NULL
);
 !   DROP TABLE public.shokaiexclude;
       public         postgres    false            �            1259    419216    shokaiimage    TABLE     �   CREATE TABLE public.shokaiimage (
    imageid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    filename text NOT NULL,
    code text NOT NULL,
    aid integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.shokaiimage;
       public         postgres    false            �            1259    419225    shokaiimageimport    TABLE     o   CREATE TABLE public.shokaiimageimport (
    importid integer NOT NULL,
    importdate date,
    uid integer
);
 %   DROP TABLE public.shokaiimageimport;
       public         postgres    false                        2604    419480    scan sid    DEFAULT     d   ALTER TABLE ONLY public.scan ALTER COLUMN sid SET DEFAULT nextval('public.scan_sid_seq'::regclass);
 7   ALTER TABLE public.scan ALTER COLUMN sid DROP DEFAULT;
       public       postgres    false    200    199            N           2606    419230    appcounter appcounter_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.appcounter
    ADD CONSTRAINT appcounter_pkey PRIMARY KEY (cym);
 D   ALTER TABLE ONLY public.appcounter DROP CONSTRAINT appcounter_pkey;
       public         postgres    false    196            T           2606    419232    application application_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.application
    ADD CONSTRAINT application_pkey PRIMARY KEY (aid);
 F   ALTER TABLE ONLY public.application DROP CONSTRAINT application_pkey;
       public         postgres    false    197            e           2606    419482    export export_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.export
    ADD CONSTRAINT export_pkey PRIMARY KEY (cym);
 <   ALTER TABLE ONLY public.export DROP CONSTRAINT export_pkey;
       public         postgres    false    205            ]           2606    419234    scangroup group_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.scangroup
    ADD CONSTRAINT group_pkey PRIMARY KEY (groupid);
 >   ALTER TABLE ONLY public.scangroup DROP CONSTRAINT group_pkey;
       public         postgres    false    201            j           2606    421114    importdata3 importdata3_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.importdata3
    ADD CONSTRAINT importdata3_pkey PRIMARY KEY (f002comnum);
 F   ALTER TABLE ONLY public.importdata3 DROP CONSTRAINT importdata3_pkey;
       public         postgres    false    213            Y           2606    419236    ocr_ordered ocr_ordered_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.ocr_ordered
    ADD CONSTRAINT ocr_ordered_pkey PRIMARY KEY (sid);
 F   ALTER TABLE ONLY public.ocr_ordered DROP CONSTRAINT ocr_ordered_pkey;
       public         postgres    false    198            h           2606    419484    refrece refrece_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.refrece
    ADD CONSTRAINT refrece_pkey PRIMARY KEY (rrid);
 >   ALTER TABLE ONLY public.refrece DROP CONSTRAINT refrece_pkey;
       public         postgres    false    210            [           2606    419238    scan scan_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY public.scan
    ADD CONSTRAINT scan_pkey PRIMARY KEY (sid);
 8   ALTER TABLE ONLY public.scan DROP CONSTRAINT scan_pkey;
       public         postgres    false    199            c           2606    419240 *   shokaiimageimport shokai_image_import_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.shokaiimageimport
    ADD CONSTRAINT shokai_image_import_pkey PRIMARY KEY (importid);
 T   ALTER TABLE ONLY public.shokaiimageimport DROP CONSTRAINT shokai_image_import_pkey;
       public         postgres    false    204            a           2606    419242    shokaiimage shokai_image_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.shokaiimage
    ADD CONSTRAINT shokai_image_pkey PRIMARY KEY (imageid);
 G   ALTER TABLE ONLY public.shokaiimage DROP CONSTRAINT shokai_image_pkey;
       public         postgres    false    203            _           2606    419244     shokaiexclude shokaiexclude_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY public.shokaiexclude
    ADD CONSTRAINT shokaiexclude_pkey PRIMARY KEY (excludeid);
 J   ALTER TABLE ONLY public.shokaiexclude DROP CONSTRAINT shokaiexclude_pkey;
       public         postgres    false    202            O           1259    419245    application_cym_idx    INDEX     J   CREATE INDEX application_cym_idx ON public.application USING btree (cym);
 '   DROP INDEX public.application_cym_idx;
       public         postgres    false    197            P           1259    419246    application_groupid_idx    INDEX     R   CREATE INDEX application_groupid_idx ON public.application USING btree (groupid);
 +   DROP INDEX public.application_groupid_idx;
       public         postgres    false    197            Q           1259    419247    application_hnum_idx    INDEX     L   CREATE INDEX application_hnum_idx ON public.application USING btree (hnum);
 (   DROP INDEX public.application_hnum_idx;
       public         postgres    false    197            R           1259    419248    application_pbirthday_idx    INDEX     V   CREATE INDEX application_pbirthday_idx ON public.application USING btree (pbirthday);
 -   DROP INDEX public.application_pbirthday_idx;
       public         postgres    false    197            U           1259    419249    application_rrid_idx    INDEX     L   CREATE INDEX application_rrid_idx ON public.application USING btree (rrid);
 (   DROP INDEX public.application_rrid_idx;
       public         postgres    false    197            V           1259    419250    application_shokaicode_idx    INDEX     X   CREATE INDEX application_shokaicode_idx ON public.application USING btree (shokaicode);
 .   DROP INDEX public.application_shokaicode_idx;
       public         postgres    false    197            f           1259    419485    refrece_num_idx    INDEX     B   CREATE INDEX refrece_num_idx ON public.refrece USING btree (num);
 #   DROP INDEX public.refrece_num_idx;
       public         postgres    false    210            W           1259    419486    uidx_aimagefile    INDEX     T   CREATE UNIQUE INDEX uidx_aimagefile ON public.application USING btree (aimagefile);
 #   DROP INDEX public.uidx_aimagefile;
       public         postgres    false    197            k           2620    419251 %   application trigger_appcounter_update    TRIGGER     �   CREATE TRIGGER trigger_appcounter_update BEFORE INSERT OR DELETE OR UPDATE ON public.application FOR EACH ROW EXECUTE PROCEDURE public.appcounter_update();
 >   DROP TRIGGER trigger_appcounter_update ON public.application;
       public       postgres    false    215    197           