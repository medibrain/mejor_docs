PGDMP     
    9                y         
   ibarakishi    11.10    11.7 z               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                       1262    416285 
   ibarakishi    DATABASE     |   CREATE DATABASE ibarakishi WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
    DROP DATABASE ibarakishi;
             postgres    false                       0    0    SCHEMA public    ACL     ¢   REVOKE ALL ON SCHEMA public FROM rdsadmin;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  postgres    false    3            Ñ            1255    416286    addmonth(integer, integer)    FUNCTION       CREATE FUNCTION public.addmonth(ym integer, addm integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    res integer := ym;
    ay integer := addm/12;
    am integer := addm%12;
    m integer :=ym%100;
BEGIN
    res=res+(ay*100);
    res=res+am;
    IF 12<m+am THEN
        res=res+88;
    ELSIF m+am<1 THEN
        res=res-88;
    END IF;
    RETURN res;
END;
$$;
 9   DROP FUNCTION public.addmonth(ym integer, addm integer);
       public       postgres    false            Ò            1255    416287    appcounter_update()    FUNCTION       CREATE FUNCTION public.appcounter_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'INSERT') THEN
  IF (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
    UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
  ELSE
    INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
  END IF;
  RETURN NEW;
ELSIF (TG_OP = 'UPDATE') THEN
  IF NEW.cym=OLD.cym THEN
    RETURN NEW;
  ELSE
    UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
    SELECT cym FROM appcounter WHERE cym=NEW.cym;
    IF  (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
      UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
    ELSE
      INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
    END IF;
  END IF;
ELSIF (TG_OP = 'DELETE') THEN
  UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
  RETURN OLD;
END IF;
END;
$$;
 *   DROP FUNCTION public.appcounter_update();
       public       postgres    false            Ä            1259    416288 
   appcounter    TABLE     e   CREATE TABLE public.appcounter (
    cym integer NOT NULL,
    counter integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.appcounter;
       public         postgres    false            Å            1259    416292    application    TABLE     à  CREATE TABLE public.application (
    aid integer NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    ayear integer DEFAULT 0 NOT NULL,
    amonth integer DEFAULT 0 NOT NULL,
    inum text DEFAULT ''::text NOT NULL,
    hnum text DEFAULT ''::text NOT NULL,
    hpref integer DEFAULT 0 NOT NULL,
    htype integer DEFAULT 0 NOT NULL,
    hname text DEFAULT ''::text NOT NULL,
    hzip text DEFAULT ''::text NOT NULL,
    haddress text DEFAULT ''::text NOT NULL,
    pname text DEFAULT ''::text NOT NULL,
    psex integer DEFAULT 0 NOT NULL,
    pbirthday date DEFAULT '0001-01-01'::date NOT NULL,
    asingle integer DEFAULT 0 NOT NULL,
    afamily integer DEFAULT 0 NOT NULL,
    aratio integer DEFAULT 0 NOT NULL,
    publcexpense text DEFAULT ''::text NOT NULL,
    emptytext1 text DEFAULT ''::text NOT NULL,
    emptyint1 integer DEFAULT 0 NOT NULL,
    emptytext2 text DEFAULT ''::text NOT NULL,
    ainspectdate date DEFAULT '0001-01-01'::date NOT NULL,
    emptyint2 integer DEFAULT 0 NOT NULL,
    emptyint3 integer DEFAULT 0 NOT NULL,
    aimagefile text DEFAULT ''::text NOT NULL,
    emptytext3 text DEFAULT ''::text NOT NULL,
    achargeyear integer DEFAULT 0 NOT NULL,
    achargemonth integer DEFAULT 0 NOT NULL,
    iname1 text DEFAULT ''::text NOT NULL,
    idate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    idays1 integer DEFAULT 0 NOT NULL,
    icourse1 integer DEFAULT 0 NOT NULL,
    ifee1 integer DEFAULT 0 NOT NULL,
    iname2 text DEFAULT ''::text NOT NULL,
    idate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    idays2 integer DEFAULT 0 NOT NULL,
    icourse2 integer DEFAULT 0 NOT NULL,
    ifee2 integer DEFAULT 0 NOT NULL,
    iname3 text DEFAULT ''::text NOT NULL,
    idate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    idays3 integer DEFAULT 0 NOT NULL,
    icourse3 integer DEFAULT 0 NOT NULL,
    ifee3 integer DEFAULT 0 NOT NULL,
    iname4 text DEFAULT ''::text NOT NULL,
    idate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    idays4 integer DEFAULT 0 NOT NULL,
    icourse4 integer DEFAULT 0 NOT NULL,
    ifee4 integer DEFAULT 0 NOT NULL,
    iname5 text DEFAULT ''::text NOT NULL,
    idate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    idays5 integer DEFAULT 0 NOT NULL,
    icourse5 integer DEFAULT 0 NOT NULL,
    ifee5 integer DEFAULT 0 NOT NULL,
    fchargetype integer DEFAULT 0 NOT NULL,
    fdistance integer DEFAULT 0 NOT NULL,
    fvisittimes integer DEFAULT 0 NOT NULL,
    fvisitfee integer DEFAULT 0 NOT NULL,
    fvisitadd integer DEFAULT 0 NOT NULL,
    sid text DEFAULT ''::text NOT NULL,
    sregnumber text DEFAULT ''::text NOT NULL,
    szip text DEFAULT ''::text NOT NULL,
    saddress text DEFAULT ''::text NOT NULL,
    sname text DEFAULT ''::text NOT NULL,
    stel text DEFAULT ''::text NOT NULL,
    sdoctor text DEFAULT ''::text NOT NULL,
    skana text DEFAULT ''::text NOT NULL,
    bacctype integer DEFAULT 0 NOT NULL,
    bname text DEFAULT ''::text NOT NULL,
    btype integer DEFAULT 0 NOT NULL,
    bbranch text DEFAULT ''::text NOT NULL,
    bbranchtype integer DEFAULT 0 NOT NULL,
    baccname text DEFAULT ''::text NOT NULL,
    bkana text DEFAULT ''::text NOT NULL,
    baccnumber text DEFAULT ''::text NOT NULL,
    atotal integer DEFAULT 0 NOT NULL,
    apartial integer DEFAULT 0 NOT NULL,
    acharge integer DEFAULT 0 NOT NULL,
    acounteddays integer DEFAULT 0 NOT NULL,
    numbering text DEFAULT ''::text NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    ufirst integer DEFAULT 0 NOT NULL,
    usecond integer DEFAULT 0 NOT NULL,
    uinquiry integer DEFAULT 0 NOT NULL,
    bui integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    statusflags integer DEFAULT 0 NOT NULL,
    shokaireason integer DEFAULT 0 NOT NULL,
    rrid integer DEFAULT 0 NOT NULL,
    inspectreasons integer DEFAULT 0 NOT NULL,
    additionaluid1 integer DEFAULT 0 NOT NULL,
    additionaluid2 integer DEFAULT 0 NOT NULL,
    memo_shokai text DEFAULT ''::text NOT NULL,
    memo_inspect text DEFAULT ''::text NOT NULL,
    memo text DEFAULT ''::text NOT NULL,
    paycode text DEFAULT ''::text NOT NULL,
    shokaicode text DEFAULT ''::text NOT NULL,
    ocrdata text DEFAULT ''::text NOT NULL,
    ufirstex integer DEFAULT 0 NOT NULL,
    usecondex integer DEFAULT 0 NOT NULL,
    kagoreasons integer DEFAULT 0 NOT NULL,
    saishinsareasons integer DEFAULT 0 NOT NULL,
    henreireasons integer DEFAULT 0 NOT NULL,
    taggeddatas text DEFAULT ''::text NOT NULL,
    comnum text DEFAULT ''::text NOT NULL,
    groupnum text DEFAULT ''::text NOT NULL,
    outmemo text DEFAULT ''::text NOT NULL,
    kagoreasons_xml xml
);
    DROP TABLE public.application;
       public         postgres    false            Í            1259    416790    impahkid_seq    SEQUENCE     u   CREATE SEQUENCE public.impahkid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.impahkid_seq;
       public       postgres    false            Î            1259    416848    dataimport_ahk    TABLE     y  CREATE TABLE public.dataimport_ahk (
    f000importid integer DEFAULT nextval('public.impahkid_seq'::regclass) NOT NULL,
    f001renban integer DEFAULT 0 NOT NULL,
    f002insnum character varying(10) DEFAULT ''::character varying NOT NULL,
    f003kohifutan character varying(16) DEFAULT ''::character varying NOT NULL,
    f004hnum character varying(32) DEFAULT ''::character varying NOT NULL,
    f005kohijyukyu character varying(16) DEFAULT ''::character varying NOT NULL,
    f006pbirthday character varying(7) DEFAULT ''::character varying NOT NULL,
    f007pgender character varying(1) DEFAULT ''::character varying NOT NULL,
    f008pname character varying(64) DEFAULT ''::character varying NOT NULL,
    f009ym character varying(5) DEFAULT ''::character varying NOT NULL,
    f010sid character varying(16) DEFAULT ''::character varying NOT NULL,
    f011sregnumber character varying(16) DEFAULT ''::character varying NOT NULL,
    f012shinsaym character varying(5) DEFAULT ''::character varying NOT NULL,
    f013comnum character varying(20) DEFAULT ''::character varying NOT NULL,
    f014 character varying DEFAULT ''::character varying NOT NULL,
    f015 character varying DEFAULT ''::character varying NOT NULL,
    pbirthdayad date DEFAULT '0001-01-01'::date NOT NULL,
    ymad integer DEFAULT 0 NOT NULL,
    shinsaymad integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL
);
 "   DROP TABLE public.dataimport_ahk;
       public         postgres    false    205            	           0    0 "   COLUMN dataimport_ahk.f000importid    COMMENT     G   COMMENT ON COLUMN public.dataimport_ahk.f000importid IS 'ç®¡çç¨ID';
            public       postgres    false    206            
           0    0     COLUMN dataimport_ahk.f001renban    COMMENT     =   COMMENT ON COLUMN public.dataimport_ahk.f001renban IS 'No.';
            public       postgres    false    206                       0    0     COLUMN dataimport_ahk.f002insnum    COMMENT     I   COMMENT ON COLUMN public.dataimport_ahk.f002insnum IS 'ä¿éºèçªå·';
            public       postgres    false    206                       0    0 #   COLUMN dataimport_ahk.f003kohifutan    COMMENT     R   COMMENT ON COLUMN public.dataimport_ahk.f003kohifutan IS 'å¬è²»è² æèçªå·';
            public       postgres    false    206                       0    0    COLUMN dataimport_ahk.f004hnum    COMMENT     M   COMMENT ON COLUMN public.dataimport_ahk.f004hnum IS 'è¢«ä¿éºèè¨¼çªå·';
            public       postgres    false    206                       0    0 $   COLUMN dataimport_ahk.f005kohijyukyu    COMMENT     S   COMMENT ON COLUMN public.dataimport_ahk.f005kohijyukyu IS 'å¬è²»åçµ¦èçªå·';
            public       postgres    false    206                       0    0 #   COLUMN dataimport_ahk.f006pbirthday    COMMENT     I   COMMENT ON COLUMN public.dataimport_ahk.f006pbirthday IS 'çå¹´ææ¥';
            public       postgres    false    206                       0    0 !   COLUMN dataimport_ahk.f007pgender    COMMENT     A   COMMENT ON COLUMN public.dataimport_ahk.f007pgender IS 'æ§å¥';
            public       postgres    false    206                       0    0    COLUMN dataimport_ahk.f008pname    COMMENT     ?   COMMENT ON COLUMN public.dataimport_ahk.f008pname IS 'æ°å';
            public       postgres    false    206                       0    0    COLUMN dataimport_ahk.f009ym    COMMENT     B   COMMENT ON COLUMN public.dataimport_ahk.f009ym IS 'æ½è¡å¹´æ';
            public       postgres    false    206                       0    0    COLUMN dataimport_ahk.f010sid    COMMENT     L   COMMENT ON COLUMN public.dataimport_ahk.f010sid IS 'å»çæ©é¢ã³ã¼ã';
            public       postgres    false    206                       0    0 $   COLUMN dataimport_ahk.f011sregnumber    COMMENT     P   COMMENT ON COLUMN public.dataimport_ahk.f011sregnumber IS 'ç»é²è¨å·çªå·';
            public       postgres    false    206                       0    0 "   COLUMN dataimport_ahk.f012shinsaym    COMMENT     H   COMMENT ON COLUMN public.dataimport_ahk.f012shinsaym IS 'å¯©æ»å¹´æ';
            public       postgres    false    206                       0    0     COLUMN dataimport_ahk.f013comnum    COMMENT     X   COMMENT ON COLUMN public.dataimport_ahk.f013comnum IS 'ã¬ã»ããå¨å½å±éã­ã¼';
            public       postgres    false    206                       0    0    COLUMN dataimport_ahk.f014    COMMENT     @   COMMENT ON COLUMN public.dataimport_ahk.f014 IS 'æ¯çµ¦åºå';
            public       postgres    false    206                       0    0    COLUMN dataimport_ahk.f015    COMMENT     @   COMMENT ON COLUMN public.dataimport_ahk.f015 IS 'è¿æ»çç±';
            public       postgres    false    206                       0    0 !   COLUMN dataimport_ahk.pbirthdayad    COMMENT     M   COMMENT ON COLUMN public.dataimport_ahk.pbirthdayad IS 'çå¹´ææ¥è¥¿æ¦';
            public       postgres    false    206                       0    0    COLUMN dataimport_ahk.ymad    COMMENT     F   COMMENT ON COLUMN public.dataimport_ahk.ymad IS 'æ½è¡å¹´æè¥¿æ¦';
            public       postgres    false    206                       0    0     COLUMN dataimport_ahk.shinsaymad    COMMENT     L   COMMENT ON COLUMN public.dataimport_ahk.shinsaymad IS 'å¯©æ»å¹´æè¥¿æ¦';
            public       postgres    false    206                       0    0    COLUMN dataimport_ahk.cym    COMMENT     K   COMMENT ON COLUMN public.dataimport_ahk.cym IS 'ã¡ãã¼ã«è«æ±å¹´æ';
            public       postgres    false    206            Ï            1259    416951    impkokuhoid_seq    SEQUENCE     x   CREATE SEQUENCE public.impkokuhoid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.impkokuhoid_seq;
       public       postgres    false            Ð            1259    416953 
   kokuhodata    TABLE       CREATE TABLE public.kokuhodata (
    f000_kokuhoid integer DEFAULT nextval('public.impkokuhoid_seq'::regclass) NOT NULL,
    f001_hihonum character varying DEFAULT ''::character varying NOT NULL,
    f002_pname character varying DEFAULT ''::character varying NOT NULL,
    f003_shinryoym character varying DEFAULT ''::character varying NOT NULL,
    f004_sid character varying DEFAULT ''::character varying NOT NULL,
    f005_sname character varying DEFAULT ''::character varying NOT NULL,
    f006_setainum character varying DEFAULT ''::character varying NOT NULL,
    f007_atenanum character varying DEFAULT ''::character varying NOT NULL,
    f008_pbirthday character varying DEFAULT ''::character varying NOT NULL,
    f009_pgender character varying DEFAULT ''::character varying NOT NULL,
    f010_ratio character varying DEFAULT ''::character varying NOT NULL,
    f011_counteddays character varying DEFAULT ''::character varying NOT NULL,
    f012_total character varying DEFAULT ''::character varying NOT NULL,
    f013_score_kbn character varying DEFAULT ''::character varying NOT NULL,
    f014_honke character varying DEFAULT ''::character varying NOT NULL,
    f015_nyugai character varying DEFAULT ''::character varying NOT NULL,
    f016_hihomark character varying DEFAULT ''::character varying NOT NULL,
    f017 character varying DEFAULT ''::character varying NOT NULL,
    f018 character varying DEFAULT ''::character varying NOT NULL,
    f019 character varying DEFAULT ''::character varying NOT NULL,
    f020_shinsaym character varying DEFAULT ''::character varying NOT NULL,
    f021_insnum character varying DEFAULT ''::character varying NOT NULL,
    f022_comnum character varying DEFAULT ''::character varying NOT NULL,
    f023_dcode character varying DEFAULT ''::character varying NOT NULL,
    f024_dname character varying DEFAULT ''::character varying NOT NULL,
    f025 character varying DEFAULT ''::character varying NOT NULL,
    f026 character varying DEFAULT ''::character varying NOT NULL,
    f027 character varying DEFAULT ''::character varying NOT NULL,
    f028_newest character varying DEFAULT ''::character varying NOT NULL,
    f029 character varying DEFAULT ''::character varying NOT NULL,
    f030_prefcorss character varying DEFAULT ''::character varying NOT NULL,
    f031 character varying DEFAULT ''::character varying NOT NULL,
    f032 character varying DEFAULT ''::character varying NOT NULL,
    f033 character varying DEFAULT ''::character varying NOT NULL,
    f034 character varying DEFAULT ''::character varying NOT NULL,
    f035_kyufu_limit character varying DEFAULT ''::character varying NOT NULL,
    f036_expensive character varying DEFAULT ''::character varying NOT NULL,
    f037 character varying DEFAULT ''::character varying NOT NULL,
    f038 character varying DEFAULT ''::character varying NOT NULL,
    f039 character varying DEFAULT ''::character varying NOT NULL,
    f040 character varying DEFAULT ''::character varying NOT NULL,
    f041 character varying DEFAULT ''::character varying NOT NULL,
    f042 character varying DEFAULT ''::character varying NOT NULL,
    f043 character varying DEFAULT ''::character varying NOT NULL,
    f044_fusen01 character varying DEFAULT ''::character varying NOT NULL,
    f045_fusen02 character varying DEFAULT ''::character varying NOT NULL,
    f046_fusen03 character varying DEFAULT ''::character varying NOT NULL,
    f047_fusen04 character varying DEFAULT ''::character varying NOT NULL,
    f048_fusen05 character varying DEFAULT ''::character varying NOT NULL,
    f049_fusen06 character varying DEFAULT ''::character varying NOT NULL,
    f050_fusen07 character varying DEFAULT ''::character varying NOT NULL,
    f051_fusen08 character varying DEFAULT ''::character varying NOT NULL,
    f052_fusen09 character varying DEFAULT ''::character varying NOT NULL,
    f053_fusen10 character varying DEFAULT ''::character varying NOT NULL,
    f054_fusen11 character varying DEFAULT ''::character varying NOT NULL,
    f055_fusen12 character varying DEFAULT ''::character varying NOT NULL,
    f056_fusen13 character varying DEFAULT ''::character varying NOT NULL,
    f057_fusen14 character varying DEFAULT ''::character varying NOT NULL,
    hihonum_narrow character varying DEFAULT ''::character varying NOT NULL,
    shinryoymad integer DEFAULT 0 NOT NULL,
    shinsaymad integer DEFAULT 0 NOT NULL,
    birthdayad date DEFAULT '0001-01-01'::date NOT NULL,
    cym integer DEFAULT 0
);
    DROP TABLE public.kokuhodata;
       public         postgres    false    207                       0    0    COLUMN kokuhodata.f001_hihonum    COMMENT     P   COMMENT ON COLUMN public.kokuhodata.f001_hihonum IS 'è¨¼çªå·ãå¨è§æ³å®';
            public       postgres    false    208                       0    0    COLUMN kokuhodata.f002_pname    COMMENT     <   COMMENT ON COLUMN public.kokuhodata.f002_pname IS 'æ°å';
            public       postgres    false    208                       0    0     COLUMN kokuhodata.f003_shinryoym    COMMENT     S   COMMENT ON COLUMN public.kokuhodata.f003_shinryoym IS 'è¨ºçå¹´æ åæ¦æ³å®';
            public       postgres    false    208                        0    0    COLUMN kokuhodata.f004_sid    COMMENT     C   COMMENT ON COLUMN public.kokuhodata.f004_sid IS 'æ©é¢ã³ã¼ã';
            public       postgres    false    208            !           0    0    COLUMN kokuhodata.f005_sname    COMMENT     E   COMMENT ON COLUMN public.kokuhodata.f005_sname IS 'å»çæ©é¢å';
            public       postgres    false    208            "           0    0    COLUMN kokuhodata.f006_setainum    COMMENT     E   COMMENT ON COLUMN public.kokuhodata.f006_setainum IS 'ä¸å¸¯çªå·';
            public       postgres    false    208            #           0    0    COLUMN kokuhodata.f007_atenanum    COMMENT     E   COMMENT ON COLUMN public.kokuhodata.f007_atenanum IS 'å®åçªå·';
            public       postgres    false    208            $           0    0     COLUMN kokuhodata.f008_pbirthday    COMMENT     S   COMMENT ON COLUMN public.kokuhodata.f008_pbirthday IS 'çå¹´ææ¥ åæ¦æ³å®';
            public       postgres    false    208            %           0    0    COLUMN kokuhodata.f009_pgender    COMMENT     >   COMMENT ON COLUMN public.kokuhodata.f009_pgender IS 'æ§å¥';
            public       postgres    false    208            &           0    0    COLUMN kokuhodata.f010_ratio    COMMENT     B   COMMENT ON COLUMN public.kokuhodata.f010_ratio IS 'çµ¦ä»å²å';
            public       postgres    false    208            '           0    0 "   COLUMN kokuhodata.f011_counteddays    COMMENT     E   COMMENT ON COLUMN public.kokuhodata.f011_counteddays IS 'å®æ¥æ°';
            public       postgres    false    208            (           0    0    COLUMN kokuhodata.f012_total    COMMENT     B   COMMENT ON COLUMN public.kokuhodata.f012_total IS 'æ±ºå®ç¹æ°';
            public       postgres    false    208            )           0    0     COLUMN kokuhodata.f013_score_kbn    COMMENT     C   COMMENT ON COLUMN public.kokuhodata.f013_score_kbn IS 'ç¹æ°è¡¨';
            public       postgres    false    208            *           0    0    COLUMN kokuhodata.f014_honke    COMMENT     <   COMMENT ON COLUMN public.kokuhodata.f014_honke IS 'æ¬å®¶';
            public       postgres    false    208            +           0    0    COLUMN kokuhodata.f015_nyugai    COMMENT     =   COMMENT ON COLUMN public.kokuhodata.f015_nyugai IS 'å¥å¤';
            public       postgres    false    208            ,           0    0    COLUMN kokuhodata.f016_hihomark    COMMENT     B   COMMENT ON COLUMN public.kokuhodata.f016_hihomark IS 'è¨¼è¨å·';
            public       postgres    false    208            -           0    0    COLUMN kokuhodata.f017    COMMENT     9   COMMENT ON COLUMN public.kokuhodata.f017 IS 'ç¨®å¥ï¼';
            public       postgres    false    208            .           0    0    COLUMN kokuhodata.f018    COMMENT     9   COMMENT ON COLUMN public.kokuhodata.f018 IS 'ç¨®å¥ï¼';
            public       postgres    false    208            /           0    0    COLUMN kokuhodata.f019    COMMENT     E   COMMENT ON COLUMN public.kokuhodata.f019 IS 'è¨ºçã»çé¤è²»å¥';
            public       postgres    false    208            0           0    0    COLUMN kokuhodata.f020_shinsaym    COMMENT     E   COMMENT ON COLUMN public.kokuhodata.f020_shinsaym IS 'å¯©æ»å¹´æ';
            public       postgres    false    208            1           0    0    COLUMN kokuhodata.f021_insnum    COMMENT     F   COMMENT ON COLUMN public.kokuhodata.f021_insnum IS 'ä¿éºèçªå·';
            public       postgres    false    208            2           0    0    COLUMN kokuhodata.f022_comnum    COMMENT     U   COMMENT ON COLUMN public.kokuhodata.f022_comnum IS 'ã¬ã»ããå¨å½å±éã­ã¼';
            public       postgres    false    208            3           0    0    COLUMN kokuhodata.f023_dcode    COMMENT     K   COMMENT ON COLUMN public.kokuhodata.f023_dcode IS 'å¦æ¹æ©é¢ã³ã¼ã';
            public       postgres    false    208            4           0    0    COLUMN kokuhodata.f024_dname    COMMENT     E   COMMENT ON COLUMN public.kokuhodata.f024_dname IS 'å¦æ¹æ©é¢å';
            public       postgres    false    208            5           0    0    COLUMN kokuhodata.f025    COMMENT     6   COMMENT ON COLUMN public.kokuhodata.f025 IS 'ã¯ãª';
            public       postgres    false    208            6           0    0    COLUMN kokuhodata.f026    COMMENT     6   COMMENT ON COLUMN public.kokuhodata.f026 IS 'ã±ã¢';
            public       postgres    false    208            7           0    0    COLUMN kokuhodata.f027    COMMENT     6   COMMENT ON COLUMN public.kokuhodata.f027 IS 'å®¹èª';
            public       postgres    false    208            8           0    0    COLUMN kokuhodata.f028_newest    COMMENT     C   COMMENT ON COLUMN public.kokuhodata.f028_newest IS 'ææ°å±¥æ­´';
            public       postgres    false    208            9           0    0    COLUMN kokuhodata.f029    COMMENT     <   COMMENT ON COLUMN public.kokuhodata.f029 IS 'åæ¬æå¨';
            public       postgres    false    208            :           0    0     COLUMN kokuhodata.f030_prefcorss    COMMENT     C   COMMENT ON COLUMN public.kokuhodata.f030_prefcorss IS 'çåå¤';
            public       postgres    false    208            ;           0    0    COLUMN kokuhodata.f031    COMMENT     ?   COMMENT ON COLUMN public.kokuhodata.f031 IS 'çé¤è²»ç¨®å¥';
            public       postgres    false    208            <           0    0    COLUMN kokuhodata.f032    COMMENT     ?   COMMENT ON COLUMN public.kokuhodata.f032 IS 'é£äºåºæºé¡';
            public       postgres    false    208            =           0    0    COLUMN kokuhodata.f033    COMMENT     6   COMMENT ON COLUMN public.kokuhodata.f033 IS 'ä¸å½';
            public       postgres    false    208            >           0    0    COLUMN kokuhodata.f034    COMMENT     9   COMMENT ON COLUMN public.kokuhodata.f034 IS 'ç¬¬ä¸è';
            public       postgres    false    208            ?           0    0 "   COLUMN kokuhodata.f035_kyufu_limit    COMMENT     H   COMMENT ON COLUMN public.kokuhodata.f035_kyufu_limit IS 'çµ¦ä»å¶é';
            public       postgres    false    208            @           0    0     COLUMN kokuhodata.f036_expensive    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f036_expensive IS 'é«é¡';
            public       postgres    false    208            A           0    0    COLUMN kokuhodata.f037    COMMENT     6   COMMENT ON COLUMN public.kokuhodata.f037 IS 'äºç´';
            public       postgres    false    208            B           0    0    COLUMN kokuhodata.f038    COMMENT     6   COMMENT ON COLUMN public.kokuhodata.f038 IS 'å¦ç';
            public       postgres    false    208            C           0    0    COLUMN kokuhodata.f039    COMMENT     <   COMMENT ON COLUMN public.kokuhodata.f039 IS 'çç¾©ç¨®å¥';
            public       postgres    false    208            D           0    0    COLUMN kokuhodata.f040    COMMENT     6   COMMENT ON COLUMN public.kokuhodata.f040 IS 'åè';
            public       postgres    false    208            E           0    0    COLUMN kokuhodata.f041    COMMENT     6   COMMENT ON COLUMN public.kokuhodata.f041 IS 'ç¶æ';
            public       postgres    false    208            F           0    0    COLUMN kokuhodata.f042    COMMENT     ?   COMMENT ON COLUMN public.kokuhodata.f042 IS 'ã³ã¼ãæå ±';
            public       postgres    false    208            G           0    0    COLUMN kokuhodata.f043    COMMENT     <   COMMENT ON COLUMN public.kokuhodata.f043 IS 'åæ¬åºå';
            public       postgres    false    208            H           0    0    COLUMN kokuhodata.f044_fusen01    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f044_fusen01 IS 'ä»ç®01';
            public       postgres    false    208            I           0    0    COLUMN kokuhodata.f045_fusen02    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f045_fusen02 IS 'ä»ç®02';
            public       postgres    false    208            J           0    0    COLUMN kokuhodata.f046_fusen03    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f046_fusen03 IS 'ä»ç®03';
            public       postgres    false    208            K           0    0    COLUMN kokuhodata.f047_fusen04    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f047_fusen04 IS 'ä»ç®04';
            public       postgres    false    208            L           0    0    COLUMN kokuhodata.f048_fusen05    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f048_fusen05 IS 'ä»ç®05';
            public       postgres    false    208            M           0    0    COLUMN kokuhodata.f049_fusen06    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f049_fusen06 IS 'ä»ç®06';
            public       postgres    false    208            N           0    0    COLUMN kokuhodata.f050_fusen07    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f050_fusen07 IS 'ä»ç®07';
            public       postgres    false    208            O           0    0    COLUMN kokuhodata.f051_fusen08    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f051_fusen08 IS 'ä»ç®08';
            public       postgres    false    208            P           0    0    COLUMN kokuhodata.f052_fusen09    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f052_fusen09 IS 'ä»ç®09';
            public       postgres    false    208            Q           0    0    COLUMN kokuhodata.f053_fusen10    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f053_fusen10 IS 'ä»ç®10';
            public       postgres    false    208            R           0    0    COLUMN kokuhodata.f054_fusen11    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f054_fusen11 IS 'ä»ç®11';
            public       postgres    false    208            S           0    0    COLUMN kokuhodata.f055_fusen12    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f055_fusen12 IS 'ä»ç®12';
            public       postgres    false    208            T           0    0    COLUMN kokuhodata.f056_fusen13    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f056_fusen13 IS 'ä»ç®13';
            public       postgres    false    208            U           0    0    COLUMN kokuhodata.f057_fusen14    COMMENT     @   COMMENT ON COLUMN public.kokuhodata.f057_fusen14 IS 'ä»ç®14';
            public       postgres    false    208            V           0    0     COLUMN kokuhodata.hihonum_narrow    COMMENT     a   COMMENT ON COLUMN public.kokuhodata.hihonum_narrow IS 'è¢«ä¿éºèè¨å·çªå·ã¡ãã¼ã«ç¨';
            public       postgres    false    208            W           0    0    COLUMN kokuhodata.shinryoymad    COMMENT     X   COMMENT ON COLUMN public.kokuhodata.shinryoymad IS 'è¨ºçå¹´æè¥¿æ¦ã¡ãã¼ã«ç¨';
            public       postgres    false    208            X           0    0    COLUMN kokuhodata.shinsaymad    COMMENT     W   COMMENT ON COLUMN public.kokuhodata.shinsaymad IS 'å¯©æ»å¹´æè¥¿æ¦ã¡ãã¼ã«ç¨';
            public       postgres    false    208            Y           0    0    COLUMN kokuhodata.birthdayad    COMMENT     W   COMMENT ON COLUMN public.kokuhodata.birthdayad IS 'çå¹´ææ¥è¥¿æ¦ã¡ãã¼ã«ç¨';
            public       postgres    false    208            Z           0    0    COLUMN kokuhodata.cym    COMMENT     G   COMMENT ON COLUMN public.kokuhodata.cym IS 'ã¡ãã¼ã«è«æ±å¹´æ';
            public       postgres    false    208            Æ            1259    416421    ocr_ordered    TABLE     >   CREATE TABLE public.ocr_ordered (
    sid integer NOT NULL
);
    DROP TABLE public.ocr_ordered;
       public         postgres    false            Ç            1259    416424    scan    TABLE     è   CREATE TABLE public.scan (
    sid integer NOT NULL,
    scandate date,
    cyear integer,
    cmonth integer,
    note1 text,
    note2 text,
    status integer DEFAULT 0,
    apptype integer DEFAULT 0 NOT NULL,
    cym integer
);
    DROP TABLE public.scan;
       public         postgres    false            È            1259    416432    scan_sid_seq    SEQUENCE     u   CREATE SEQUENCE public.scan_sid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.scan_sid_seq;
       public       postgres    false    199            [           0    0    scan_sid_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.scan_sid_seq OWNED BY public.scan.sid;
            public       postgres    false    200            É            1259    416434 	   scangroup    TABLE       CREATE TABLE public.scangroup (
    groupid integer NOT NULL,
    status integer,
    scanid integer,
    scandate date,
    scanuser integer,
    checkdate date,
    checkuser integer,
    inquirydate date,
    inquiryuser integer,
    workingusers text DEFAULT ''::text NOT NULL
);
    DROP TABLE public.scangroup;
       public         postgres    false            Ê            1259    416441    shokaiexclude    TABLE     )  CREATE TABLE public.shokaiexclude (
    excludeid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    mediym integer DEFAULT 0 NOT NULL,
    chargeym integer DEFAULT 0 NOT NULL,
    hihonum text DEFAULT ''::text NOT NULL,
    birth date DEFAULT '0001-01-01'::date NOT NULL
);
 !   DROP TABLE public.shokaiexclude;
       public         postgres    false            Ë            1259    416453    shokaiimage    TABLE     Í   CREATE TABLE public.shokaiimage (
    imageid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    filename text NOT NULL,
    code text NOT NULL,
    aid integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.shokaiimage;
       public         postgres    false            Ì            1259    416462    shokaiimageimport    TABLE     o   CREATE TABLE public.shokaiimageimport (
    importid integer NOT NULL,
    importdate date,
    uid integer
);
 %   DROP TABLE public.shokaiimageimport;
       public         postgres    false                       2604    416465    scan sid    DEFAULT     d   ALTER TABLE ONLY public.scan ALTER COLUMN sid SET DEFAULT nextval('public.scan_sid_seq'::regclass);
 7   ALTER TABLE public.scan ALTER COLUMN sid DROP DEFAULT;
       public       postgres    false    200    199            n           2606    416467    appcounter appcounter_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.appcounter
    ADD CONSTRAINT appcounter_pkey PRIMARY KEY (cym);
 D   ALTER TABLE ONLY public.appcounter DROP CONSTRAINT appcounter_pkey;
       public         postgres    false    196            t           2606    416469    application application_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.application
    ADD CONSTRAINT application_pkey PRIMARY KEY (aid);
 F   ALTER TABLE ONLY public.application DROP CONSTRAINT application_pkey;
       public         postgres    false    197                       2606    416875 "   dataimport_ahk dataimport_ahk_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.dataimport_ahk
    ADD CONSTRAINT dataimport_ahk_pkey PRIMARY KEY (f000importid);
 L   ALTER TABLE ONLY public.dataimport_ahk DROP CONSTRAINT dataimport_ahk_pkey;
       public         postgres    false    206            |           2606    416471    scangroup group_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.scangroup
    ADD CONSTRAINT group_pkey PRIMARY KEY (groupid);
 >   ALTER TABLE ONLY public.scangroup DROP CONSTRAINT group_pkey;
       public         postgres    false    201                       2606    417023    kokuhodata kokuhodata_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.kokuhodata
    ADD CONSTRAINT kokuhodata_pkey PRIMARY KEY (f000_kokuhoid, f022_comnum);
 D   ALTER TABLE ONLY public.kokuhodata DROP CONSTRAINT kokuhodata_pkey;
       public         postgres    false    208    208            x           2606    416473    ocr_ordered ocr_ordered_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.ocr_ordered
    ADD CONSTRAINT ocr_ordered_pkey PRIMARY KEY (sid);
 F   ALTER TABLE ONLY public.ocr_ordered DROP CONSTRAINT ocr_ordered_pkey;
       public         postgres    false    198            z           2606    416475    scan scan_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY public.scan
    ADD CONSTRAINT scan_pkey PRIMARY KEY (sid);
 8   ALTER TABLE ONLY public.scan DROP CONSTRAINT scan_pkey;
       public         postgres    false    199                       2606    416477 *   shokaiimageimport shokai_image_import_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.shokaiimageimport
    ADD CONSTRAINT shokai_image_import_pkey PRIMARY KEY (importid);
 T   ALTER TABLE ONLY public.shokaiimageimport DROP CONSTRAINT shokai_image_import_pkey;
       public         postgres    false    204                       2606    416479    shokaiimage shokai_image_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.shokaiimage
    ADD CONSTRAINT shokai_image_pkey PRIMARY KEY (imageid);
 G   ALTER TABLE ONLY public.shokaiimage DROP CONSTRAINT shokai_image_pkey;
       public         postgres    false    203            ~           2606    416481     shokaiexclude shokaiexclude_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY public.shokaiexclude
    ADD CONSTRAINT shokaiexclude_pkey PRIMARY KEY (excludeid);
 J   ALTER TABLE ONLY public.shokaiexclude DROP CONSTRAINT shokaiexclude_pkey;
       public         postgres    false    202            o           1259    416482    application_cym_idx    INDEX     J   CREATE INDEX application_cym_idx ON public.application USING btree (cym);
 '   DROP INDEX public.application_cym_idx;
       public         postgres    false    197            p           1259    416483    application_groupid_idx    INDEX     R   CREATE INDEX application_groupid_idx ON public.application USING btree (groupid);
 +   DROP INDEX public.application_groupid_idx;
       public         postgres    false    197            q           1259    416484    application_hnum_idx    INDEX     L   CREATE INDEX application_hnum_idx ON public.application USING btree (hnum);
 (   DROP INDEX public.application_hnum_idx;
       public         postgres    false    197            r           1259    416485    application_pbirthday_idx    INDEX     V   CREATE INDEX application_pbirthday_idx ON public.application USING btree (pbirthday);
 -   DROP INDEX public.application_pbirthday_idx;
       public         postgres    false    197            u           1259    416486    application_rrid_idx    INDEX     L   CREATE INDEX application_rrid_idx ON public.application USING btree (rrid);
 (   DROP INDEX public.application_rrid_idx;
       public         postgres    false    197            v           1259    416487    application_shokaicode_idx    INDEX     X   CREATE INDEX application_shokaicode_idx ON public.application USING btree (shokaicode);
 .   DROP INDEX public.application_shokaicode_idx;
       public         postgres    false    197                       1259    417034    uidx_comnum_ym    INDEX     c   CREATE UNIQUE INDEX uidx_comnum_ym ON public.kokuhodata USING btree (f022_comnum, f003_shinryoym);
 "   DROP INDEX public.uidx_comnum_ym;
       public         postgres    false    208    208                       2620    416488 %   application trigger_appcounter_update    TRIGGER        CREATE TRIGGER trigger_appcounter_update BEFORE INSERT OR DELETE OR UPDATE ON public.application FOR EACH ROW EXECUTE PROCEDURE public.appcounter_update();
 >   DROP TRIGGER trigger_appcounter_update ON public.application;
       public       postgres    false    197    210           