Attribute VB_Name = "Module1"
'画像登録直後のチェック
Sub checkblank1()

Dim r As Long
Dim c As Integer
Dim sh As Worksheet
Set sh = ThisWorkbook.ActiveSheet
Dim flgng As Boolean
flgng = False


For r = 7 To 167
    
    If sh.Cells(r, 28) <> "" Then
        For c = 57 To 61
            '色クリア
            sh.Cells(r, c).Interior.Color = xlNone
            sh.Cells(r, c).Font.ColorIndex = 1
        
            '画像登録時の自動値があるはずなのに無い場合は色付け
            If sh.Cells(r, c) = "" Or sh.Cells(r, c) = "0" Then
                sh.Cells(r, c).Interior.Color = RGB(250, 50, 0)
                flgng = True
                
            Else
                sh.Cells(r, c).Interior.Color = RGB(0, 150, 220)
                sh.Cells(r, c).Font.Color = RGB(250, 250, 250)
                
                '未来日付
                If checkdate(sh.Cells(r, c)) = False Then flgng = True
                
                
            End If
            
        Next
    End If
    
Next

If flgng Then MsgBox "ng有り"

End Sub

Function checkdate(rng As Range)
On Error GoTo err

    If InStr(rng, "/") > 0 Then
    'If CDate(rng) Then
        Dim tmprng As Date
        tmprng = rng
        
        '未来
        If tmprng > Format(Now(), "yyyy/mm/dd") Then
            rng.Interior.Color = RGB(250, 0, 0)
            checkdate = False
            Exit Function
        '100年以上前
        ElseIf DateDiff("yyyy", tmprng, Format(Now(), "yyyy/mm/dd")) > 100 And tmprng <> "0001/01/01" Then
            rng.Interior.Color = RGB(250, 0, 0)
            checkdate = False
            Exit Function
            
        End If
    End If
    checkdate = True
    Exit Function
err:
    checkdate = True
    Exit Function
End Function

'初回入力後チェック
Sub checkblank2()
Dim r As Long
Dim c As Integer
Dim sh As Worksheet
Set sh = ThisWorkbook.ActiveSheet
Dim flgng As Boolean
flgng = False

For r = 7 To 167
    '入力項目の場合、何か値があるはず
    If sh.Cells(r, 28) <> "" Or sh.Cells(r, 29) <> "" Or sh.Cells(r, 42) <> "" Then
        For c = 63 To 67
            '色クリア
            sh.Cells(r, c).Interior.Color = xlNone
            sh.Cells(r, c).Font.ColorIndex = 1
            
            '登録前と1回目で、入力項目なのに値が入ってない場合警告
            If sh.Cells(r, c) = "" Or sh.Cells(r, c) = "0" Then
                sh.Cells(r, c).Interior.Color = RGB(250, 50, 0)
                flgng = True
            End If
            
              '登録前と1回目で、値が入っていて同じ場合OK
            If sh.Cells(r, c - 6) = sh.Cells(r, c) Then
                sh.Cells(r, c).Interior.Color = RGB(0, 150, 220)
                sh.Cells(r, c).Font.Color = RGB(250, 250, 250)
                
                '未来日付エラー
                If checkdate(sh.Cells(r, c)) = False Then flgng = True
                

            End If
            
            '登録前と1回目で、値が入っていて違う場合色変える
            If sh.Cells(r, c - 6) <> sh.Cells(r, c) Then
                sh.Cells(r, c).Interior.Color = RGB(0, 190, 70)
                sh.Cells(r, c).Font.Color = RGB(250, 250, 250)
                
                '未来日付エラー
                If checkdate(sh.Cells(r, c)) = False Then flgng = True
                
                
            End If
            
          
            
        Next
    End If
    
Next
If flgng Then MsgBox "ng有り"

End Sub


'2回入力後チェック
Sub checkblank3()
Dim r As Long
Dim c As Integer
Dim sh As Worksheet
Set sh = ThisWorkbook.ActiveSheet
Dim flgng As Boolean
flgng = False

For r = 7 To 167
    '入力項目の場合、何か値があるはず
    If sh.Cells(r, 28) <> "" Or sh.Cells(r, 29) <> "" Or sh.Cells(r, 42) <> "" Then
        For c = 69 To 73
            '色クリア
            sh.Cells(r, c).Interior.Color = xlNone
            sh.Cells(r, c).Font.ColorIndex = 1
            
            '2回目で、入力項目なのに値が入ってない場合警告
            If sh.Cells(r, c) = "" Or sh.Cells(r, c) = "0" Then
                sh.Cells(r, c).Interior.Color = RGB(250, 50, 0)
                flgng = True
            End If
            
            '1回目と2回目で、値が入っていて同じ場合OK
            If sh.Cells(r, c - 6) = sh.Cells(r, c) Then
                sh.Cells(r, c).Interior.Color = RGB(0, 150, 220)
                sh.Cells(r, c).Font.Color = RGB(250, 250, 250)
                
                If checkdate(sh.Cells(r, c)) = False Then flgng = True
                
            
            End If
            
            '1回目と2回目で、値が入っていて違う場合注意
            If sh.Cells(r, c - 6) <> sh.Cells(r, c) Then
                sh.Cells(r, c).Interior.Color = RGB(0, 200, 100)
                'sh.Cells(r, c).Font.Color = RGB(250, 250, 250)
                
                If checkdate(sh.Cells(r, c)) = False Then flgng = True
                
            
            End If
            
        Next
    End If
    
Next
If flgng Then MsgBox "ng有り"

End Sub

Sub clearcells()
Set sh = ThisWorkbook.ActiveSheet
sh.Range(sh.Cells(7, 57), sh.Cells(167, 73)).ClearContents
sh.Range(sh.Cells(7, 57), sh.Cells(167, 73)).Interior.Color = xlNone
sh.Range(sh.Cells(7, 57), sh.Cells(167, 73)).Font.ColorIndex = 1



End Sub
