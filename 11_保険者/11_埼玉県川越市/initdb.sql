PGDMP     0                
    y         
   kawagoeshi    11.12    13.3 `               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                       1262    648798 
   kawagoeshi    DATABASE     _   CREATE DATABASE kawagoeshi WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.UTF-8';
    DROP DATABASE kawagoeshi;
                postgres    false            	           0    0    SCHEMA public    ACL     ¢   REVOKE ALL ON SCHEMA public FROM rdsadmin;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                   postgres    false    3            Ñ            1255    648799    addmonth(integer, integer)    FUNCTION       CREATE FUNCTION public.addmonth(ym integer, addm integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    res integer := ym;
    ay integer := addm/12;
    am integer := addm%12;
    m integer :=ym%100;
BEGIN
    res=res+(ay*100);
    res=res+am;
    IF 12<m+am THEN
        res=res+88;
    ELSIF m+am<1 THEN
        res=res-88;
    END IF;
    RETURN res;
END;
$$;
 9   DROP FUNCTION public.addmonth(ym integer, addm integer);
       public          postgres    false            Ò            1255    648800    appcounter_update()    FUNCTION       CREATE FUNCTION public.appcounter_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'INSERT') THEN
  IF (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
    UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
  ELSE
    INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
  END IF;
  RETURN NEW;
ELSIF (TG_OP = 'UPDATE') THEN
  IF NEW.cym=OLD.cym THEN
    RETURN NEW;
  ELSE
    UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
    SELECT cym FROM appcounter WHERE cym=NEW.cym;
    IF  (SELECT EXISTS(SELECT cym FROM appcounter WHERE cym=NEW.cym)) THEN
      UPDATE appcounter SET counter=counter+1 WHERE cym=NEW.cym;
    ELSE
      INSERT INTO appcounter(cym,counter) VALUES(NEW.cym,1);
    END IF;
  END IF;
ELSIF (TG_OP = 'DELETE') THEN
  UPDATE appcounter SET counter=counter-1 WHERE cym=OLD.cym;
  RETURN OLD;
END IF;
END;
$$;
 *   DROP FUNCTION public.appcounter_update();
       public          postgres    false            Ä            1259    648801 
   appcounter    TABLE     e   CREATE TABLE public.appcounter (
    cym integer NOT NULL,
    counter integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.appcounter;
       public            postgres    false            Å            1259    648805    application    TABLE     à  CREATE TABLE public.application (
    aid integer NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    ayear integer DEFAULT 0 NOT NULL,
    amonth integer DEFAULT 0 NOT NULL,
    inum text DEFAULT ''::text NOT NULL,
    hnum text DEFAULT ''::text NOT NULL,
    hpref integer DEFAULT 0 NOT NULL,
    htype integer DEFAULT 0 NOT NULL,
    hname text DEFAULT ''::text NOT NULL,
    hzip text DEFAULT ''::text NOT NULL,
    haddress text DEFAULT ''::text NOT NULL,
    pname text DEFAULT ''::text NOT NULL,
    psex integer DEFAULT 0 NOT NULL,
    pbirthday date DEFAULT '0001-01-01'::date NOT NULL,
    asingle integer DEFAULT 0 NOT NULL,
    afamily integer DEFAULT 0 NOT NULL,
    aratio integer DEFAULT 0 NOT NULL,
    publcexpense text DEFAULT ''::text NOT NULL,
    emptytext1 text DEFAULT ''::text NOT NULL,
    emptyint1 integer DEFAULT 0 NOT NULL,
    emptytext2 text DEFAULT ''::text NOT NULL,
    ainspectdate date DEFAULT '0001-01-01'::date NOT NULL,
    emptyint2 integer DEFAULT 0 NOT NULL,
    emptyint3 integer DEFAULT 0 NOT NULL,
    aimagefile text DEFAULT ''::text NOT NULL,
    emptytext3 text DEFAULT ''::text NOT NULL,
    achargeyear integer DEFAULT 0 NOT NULL,
    achargemonth integer DEFAULT 0 NOT NULL,
    iname1 text DEFAULT ''::text NOT NULL,
    idate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate1 date DEFAULT '0001-01-01'::date NOT NULL,
    idays1 integer DEFAULT 0 NOT NULL,
    icourse1 integer DEFAULT 0 NOT NULL,
    ifee1 integer DEFAULT 0 NOT NULL,
    iname2 text DEFAULT ''::text NOT NULL,
    idate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate2 date DEFAULT '0001-01-01'::date NOT NULL,
    idays2 integer DEFAULT 0 NOT NULL,
    icourse2 integer DEFAULT 0 NOT NULL,
    ifee2 integer DEFAULT 0 NOT NULL,
    iname3 text DEFAULT ''::text NOT NULL,
    idate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate3 date DEFAULT '0001-01-01'::date NOT NULL,
    idays3 integer DEFAULT 0 NOT NULL,
    icourse3 integer DEFAULT 0 NOT NULL,
    ifee3 integer DEFAULT 0 NOT NULL,
    iname4 text DEFAULT ''::text NOT NULL,
    idate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate4 date DEFAULT '0001-01-01'::date NOT NULL,
    idays4 integer DEFAULT 0 NOT NULL,
    icourse4 integer DEFAULT 0 NOT NULL,
    ifee4 integer DEFAULT 0 NOT NULL,
    iname5 text DEFAULT ''::text NOT NULL,
    idate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifirstdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    istartdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    ifinishdate5 date DEFAULT '0001-01-01'::date NOT NULL,
    idays5 integer DEFAULT 0 NOT NULL,
    icourse5 integer DEFAULT 0 NOT NULL,
    ifee5 integer DEFAULT 0 NOT NULL,
    fchargetype integer DEFAULT 0 NOT NULL,
    fdistance integer DEFAULT 0 NOT NULL,
    fvisittimes integer DEFAULT 0 NOT NULL,
    fvisitfee integer DEFAULT 0 NOT NULL,
    fvisitadd integer DEFAULT 0 NOT NULL,
    sid text DEFAULT ''::text NOT NULL,
    sregnumber text DEFAULT ''::text NOT NULL,
    szip text DEFAULT ''::text NOT NULL,
    saddress text DEFAULT ''::text NOT NULL,
    sname text DEFAULT ''::text NOT NULL,
    stel text DEFAULT ''::text NOT NULL,
    sdoctor text DEFAULT ''::text NOT NULL,
    skana text DEFAULT ''::text NOT NULL,
    bacctype integer DEFAULT 0 NOT NULL,
    bname text DEFAULT ''::text NOT NULL,
    btype integer DEFAULT 0 NOT NULL,
    bbranch text DEFAULT ''::text NOT NULL,
    bbranchtype integer DEFAULT 0 NOT NULL,
    baccname text DEFAULT ''::text NOT NULL,
    bkana text DEFAULT ''::text NOT NULL,
    baccnumber text DEFAULT ''::text NOT NULL,
    atotal integer DEFAULT 0 NOT NULL,
    apartial integer DEFAULT 0 NOT NULL,
    acharge integer DEFAULT 0 NOT NULL,
    acounteddays integer DEFAULT 0 NOT NULL,
    numbering text DEFAULT ''::text NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    note text DEFAULT ''::text NOT NULL,
    ufirst integer DEFAULT 0 NOT NULL,
    usecond integer DEFAULT 0 NOT NULL,
    uinquiry integer DEFAULT 0 NOT NULL,
    bui integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    statusflags integer DEFAULT 0 NOT NULL,
    shokaireason integer DEFAULT 0 NOT NULL,
    rrid integer DEFAULT 0 NOT NULL,
    inspectreasons integer DEFAULT 0 NOT NULL,
    additionaluid1 integer DEFAULT 0 NOT NULL,
    additionaluid2 integer DEFAULT 0 NOT NULL,
    memo_shokai text DEFAULT ''::text NOT NULL,
    memo_inspect text DEFAULT ''::text NOT NULL,
    memo text DEFAULT ''::text NOT NULL,
    paycode text DEFAULT ''::text NOT NULL,
    shokaicode text DEFAULT ''::text NOT NULL,
    ocrdata text DEFAULT ''::text NOT NULL,
    ufirstex integer DEFAULT 0 NOT NULL,
    usecondex integer DEFAULT 0 NOT NULL,
    kagoreasons integer DEFAULT 0 NOT NULL,
    saishinsareasons integer DEFAULT 0 NOT NULL,
    henreireasons integer DEFAULT 0 NOT NULL,
    taggeddatas text DEFAULT ''::text NOT NULL,
    comnum text DEFAULT ''::text NOT NULL,
    groupnum text DEFAULT ''::text NOT NULL,
    outmemo text DEFAULT ''::text NOT NULL,
    kagoreasons_xml xml
);
    DROP TABLE public.application;
       public            postgres    false            Æ            1259    648934    application_aux    TABLE     ®  CREATE TABLE public.application_aux (
    aid integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    scanid integer DEFAULT 0 NOT NULL,
    groupid integer DEFAULT 0 NOT NULL,
    aimagefile character varying DEFAULT ''::character varying NOT NULL,
    origfile character varying DEFAULT ''::character varying NOT NULL,
    multitiff_pageno integer DEFAULT 0 NOT NULL,
    aapptype integer DEFAULT 0 NOT NULL,
    parentaid integer DEFAULT 0 NOT NULL,
    batchaid integer DEFAULT 0 NOT NULL,
    matchingid01 character varying DEFAULT ''::character varying NOT NULL,
    matchingid02 character varying DEFAULT ''::character varying NOT NULL,
    matchingid03 character varying DEFAULT ''::character varying NOT NULL,
    matchingid04 character varying DEFAULT ''::character varying NOT NULL,
    matchingid05 character varying DEFAULT ''::character varying NOT NULL,
    matchingid01date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid02date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid03date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid04date date DEFAULT '0001-01-01'::date NOT NULL,
    matchingid05date date DEFAULT '0001-01-01'::date NOT NULL
);
 #   DROP TABLE public.application_aux;
       public            postgres    false            
           0    0    COLUMN application_aux.aid    COMMENT     7   COMMENT ON COLUMN public.application_aux.aid IS 'aid';
          public          postgres    false    198                       0    0    COLUMN application_aux.cym    COMMENT     L   COMMENT ON COLUMN public.application_aux.cym IS 'ã¡ãã¼ã«è«æ±å¹´æ';
          public          postgres    false    198                       0    0    COLUMN application_aux.scanid    COMMENT     E   COMMENT ON COLUMN public.application_aux.scanid IS 'ã¹ã­ã£ã³ID';
          public          postgres    false    198                       0    0    COLUMN application_aux.groupid    COMMENT     F   COMMENT ON COLUMN public.application_aux.groupid IS 'ã°ã«ã¼ãID';
          public          postgres    false    198                       0    0 !   COLUMN application_aux.aimagefile    COMMENT     Y   COMMENT ON COLUMN public.application_aux.aimagefile IS 'å¤æå¾ç»åãã¡ã¤ã«å';
          public          postgres    false    198                       0    0    COLUMN application_aux.origfile    COMMENT     Q   COMMENT ON COLUMN public.application_aux.origfile IS 'åç»åãã¡ã¤ã«å';
          public          postgres    false    198                       0    0 '   COLUMN application_aux.multitiff_pageno    COMMENT     c   COMMENT ON COLUMN public.application_aux.multitiff_pageno IS 'ãã«ãtiffç»åãã¼ã¸çªå·';
          public          postgres    false    198                       0    0    COLUMN application_aux.aapptype    COMMENT     G   COMMENT ON COLUMN public.application_aux.aapptype IS 'AIDã®aapptype';
          public          postgres    false    198                       0    0     COLUMN application_aux.parentaid    COMMENT     R   COMMENT ON COLUMN public.application_aux.parentaid IS 'ç¶ç´ã®è¦ªç³è«æ¸AID';
          public          postgres    false    198                       0    0    COLUMN application_aux.batchaid    COMMENT     K   COMMENT ON COLUMN public.application_aux.batchaid IS 'æå±ãããAID';
          public          postgres    false    198                       0    0 #   COLUMN application_aux.matchingid01    COMMENT     a   COMMENT ON COLUMN public.application_aux.matchingid01 IS 'ãããã³ã°ãããã¼ã¿ã®ID1';
          public          postgres    false    198                       0    0 #   COLUMN application_aux.matchingid02    COMMENT     a   COMMENT ON COLUMN public.application_aux.matchingid02 IS 'ãããã³ã°ãããã¼ã¿ã®ID2';
          public          postgres    false    198                       0    0 #   COLUMN application_aux.matchingid03    COMMENT     a   COMMENT ON COLUMN public.application_aux.matchingid03 IS 'ãããã³ã°ãããã¼ã¿ã®ID3';
          public          postgres    false    198                       0    0 #   COLUMN application_aux.matchingid04    COMMENT     a   COMMENT ON COLUMN public.application_aux.matchingid04 IS 'ãããã³ã°ãããã¼ã¿ã®ID4';
          public          postgres    false    198                       0    0 #   COLUMN application_aux.matchingid05    COMMENT     a   COMMENT ON COLUMN public.application_aux.matchingid05 IS 'ãããã³ã°ãããã¼ã¿ã®ID5';
          public          postgres    false    198                       0    0 '   COLUMN application_aux.matchingid01date    COMMENT     W   COMMENT ON COLUMN public.application_aux.matchingid01date IS 'ãããã³ã°æå»1';
          public          postgres    false    198                       0    0 '   COLUMN application_aux.matchingid02date    COMMENT     W   COMMENT ON COLUMN public.application_aux.matchingid02date IS 'ãããã³ã°æå»2';
          public          postgres    false    198                       0    0 '   COLUMN application_aux.matchingid03date    COMMENT     W   COMMENT ON COLUMN public.application_aux.matchingid03date IS 'ãããã³ã°æå»3';
          public          postgres    false    198                       0    0 '   COLUMN application_aux.matchingid04date    COMMENT     W   COMMENT ON COLUMN public.application_aux.matchingid04date IS 'ãããã³ã°æå»4';
          public          postgres    false    198                       0    0 '   COLUMN application_aux.matchingid05date    COMMENT     W   COMMENT ON COLUMN public.application_aux.matchingid05date IS 'ãããã³ã°æå»5';
          public          postgres    false    198            Ï            1259    650136    import_id_seq    SEQUENCE     v   CREATE SEQUENCE public.import_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.import_id_seq;
       public          postgres    false            Ð            1259    650342 
   dataimport    TABLE     9	  CREATE TABLE public.dataimport (
    f000importid integer DEFAULT nextval('public.import_id_seq'::regclass) NOT NULL,
    f001shinsaym character varying DEFAULT ''::character varying NOT NULL,
    f002comnum character varying DEFAULT ''::character varying NOT NULL,
    f006clinicnum character varying DEFAULT ''::character varying NOT NULL,
    f008clinicname character varying DEFAULT ''::character varying NOT NULL,
    f011accountname character varying DEFAULT ''::character varying NOT NULL,
    f012insnum character varying DEFAULT ''::character varying NOT NULL,
    f014hmark character varying DEFAULT ''::character varying NOT NULL,
    f015hnum character varying DEFAULT ''::character varying NOT NULL,
    f016hmarknum character varying DEFAULT ''::character varying NOT NULL,
    f018pkana character varying DEFAULT ''::character varying NOT NULL,
    f019pname character varying DEFAULT ''::character varying NOT NULL,
    f020pgender character varying DEFAULT ''::character varying NOT NULL,
    f025atenano character varying DEFAULT ''::character varying NOT NULL,
    f028pbirthday character varying DEFAULT ''::character varying NOT NULL,
    f029ym character varying DEFAULT ''::character varying NOT NULL,
    f033counteddays character varying DEFAULT ''::character varying NOT NULL,
    f045ratio character varying DEFAULT ''::character varying NOT NULL,
    f046family character varying DEFAULT ''::character varying NOT NULL,
    f079startdate1 character varying DEFAULT ''::character varying NOT NULL,
    f080finishdate1 character varying DEFAULT ''::character varying NOT NULL,
    f090total character varying DEFAULT ''::character varying NOT NULL,
    f091charge character varying DEFAULT ''::character varying NOT NULL,
    f093partial character varying DEFAULT ''::character varying NOT NULL,
    f103clinickana character varying DEFAULT ''::character varying NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ymad integer DEFAULT 0,
    shinsaymad integer DEFAULT 0,
    birthad date DEFAULT '0001-01-01'::date,
    startdate1ad date DEFAULT '0001-01-01'::date,
    finishdate1ad date DEFAULT '0001-01-01'::date,
    hmark_nr character varying DEFAULT ''::character varying NOT NULL,
    hnum_nr character varying DEFAULT ''::character varying NOT NULL,
    hmarkhnum_nr character varying DEFAULT ''::character varying NOT NULL
);
    DROP TABLE public.dataimport;
       public            postgres    false    207                       0    0    COLUMN dataimport.f000importid    COMMENT     U   COMMENT ON COLUMN public.dataimport.f000importid IS 'ã¤ã³ãã¼ãIDãç®¡çç¨';
          public          postgres    false    208                       0    0    COLUMN dataimport.f001shinsaym    COMMENT     G   COMMENT ON COLUMN public.dataimport.f001shinsaym IS 'å¯©æ»æåæ¦';
          public          postgres    false    208                        0    0    COLUMN dataimport.f002comnum    COMMENT     T   COMMENT ON COLUMN public.dataimport.f002comnum IS 'ã¬ã»ããå¨å½å±éã­ã¼';
          public          postgres    false    208            !           0    0    COLUMN dataimport.f006clinicnum    COMMENT     N   COMMENT ON COLUMN public.dataimport.f006clinicnum IS 'å»çæ©é¢ã³ã¼ã';
          public          postgres    false    208            "           0    0     COLUMN dataimport.f008clinicname    COMMENT     L   COMMENT ON COLUMN public.dataimport.f008clinicname IS 'æ½è¡æåæ¼¢å­';
          public          postgres    false    208            #           0    0 !   COLUMN dataimport.f011accountname    COMMENT     G   COMMENT ON COLUMN public.dataimport.f011accountname IS 'å£åº§åç§°';
          public          postgres    false    208            $           0    0    COLUMN dataimport.f012insnum    COMMENT     E   COMMENT ON COLUMN public.dataimport.f012insnum IS 'ä¿éºèçªå·';
          public          postgres    false    208            %           0    0    COLUMN dataimport.f014hmark    COMMENT     P   COMMENT ON COLUMN public.dataimport.f014hmark IS 'è¢«ä¿éºèè¨¼è¨å·å¨è§';
          public          postgres    false    208            &           0    0    COLUMN dataimport.f015hnum    COMMENT     O   COMMENT ON COLUMN public.dataimport.f015hnum IS 'è¢«ä¿éºèè¨¼çªå·å¨è§';
          public          postgres    false    208            '           0    0    COLUMN dataimport.f016hmarknum    COMMENT     P   COMMENT ON COLUMN public.dataimport.f016hmarknum IS 'è¢«ä¿éºèè¨å·çªå·';
          public          postgres    false    208            (           0    0    COLUMN dataimport.f018pkana    COMMENT     k   COMMENT ON COLUMN public.dataimport.f018pkana IS 'è¢«ä¿éºèè¨¼æ°åã«ãï¼åçèã¨ãã¦æ±ã';
          public          postgres    false    208            )           0    0    COLUMN dataimport.f019pname    COMMENT     k   COMMENT ON COLUMN public.dataimport.f019pname IS 'è¢«ä¿éºèè¨¼æ°åæ¼¢å­ï¼åçèã¨ãã¦æ±ã';
          public          postgres    false    208            *           0    0    COLUMN dataimport.f020pgender    COMMENT     =   COMMENT ON COLUMN public.dataimport.f020pgender IS 'æ§å¥';
          public          postgres    false    208            +           0    0    COLUMN dataimport.f025atenano    COMMENT     C   COMMENT ON COLUMN public.dataimport.f025atenano IS 'å®åçªå·';
          public          postgres    false    208            ,           0    0    COLUMN dataimport.f028pbirthday    COMMENT     K   COMMENT ON COLUMN public.dataimport.f028pbirthday IS 'çå¹´ææ¥åæ¦';
          public          postgres    false    208            -           0    0    COLUMN dataimport.f029ym    COMMENT     D   COMMENT ON COLUMN public.dataimport.f029ym IS 'æ½è¡å¹´æåæ¦';
          public          postgres    false    208            .           0    0 !   COLUMN dataimport.f033counteddays    COMMENT     D   COMMENT ON COLUMN public.dataimport.f033counteddays IS 'å®æ¥æ°';
          public          postgres    false    208            /           0    0    COLUMN dataimport.f045ratio    COMMENT     B   COMMENT ON COLUMN public.dataimport.f045ratio IS 'çµ¦ä»å²å%';
          public          postgres    false    208            0           0    0    COLUMN dataimport.f046family    COMMENT     H   COMMENT ON COLUMN public.dataimport.f046family IS 'æ¬äººå®¶æå¥å¤';
          public          postgres    false    208            1           0    0     COLUMN dataimport.f079startdate1    COMMENT     O   COMMENT ON COLUMN public.dataimport.f079startdate1 IS 'æ½è¡éå§æ¥åæ¦';
          public          postgres    false    208            2           0    0 !   COLUMN dataimport.f080finishdate1    COMMENT     P   COMMENT ON COLUMN public.dataimport.f080finishdate1 IS 'æ½è¡çµäºæ¥åæ¦';
          public          postgres    false    208            3           0    0    COLUMN dataimport.f090total    COMMENT     >   COMMENT ON COLUMN public.dataimport.f090total IS 'åè¨é¡';
          public          postgres    false    208            4           0    0    COLUMN dataimport.f091charge    COMMENT     ?   COMMENT ON COLUMN public.dataimport.f091charge IS 'è«æ±é¡';
          public          postgres    false    208            5           0    0    COLUMN dataimport.f093partial    COMMENT     F   COMMENT ON COLUMN public.dataimport.f093partial IS 'ä¸é¨è² æé';
          public          postgres    false    208            6           0    0     COLUMN dataimport.f103clinickana    COMMENT     I   COMMENT ON COLUMN public.dataimport.f103clinickana IS 'æ½è¡æã«ã';
          public          postgres    false    208            7           0    0    COLUMN dataimport.cym    COMMENT     G   COMMENT ON COLUMN public.dataimport.cym IS 'ã¡ãã¼ã«è«æ±å¹´æ';
          public          postgres    false    208            8           0    0    COLUMN dataimport.ymad    COMMENT     B   COMMENT ON COLUMN public.dataimport.ymad IS 'æ½è¡å¹´æè¥¿æ¦';
          public          postgres    false    208            9           0    0    COLUMN dataimport.shinsaymad    COMMENT     E   COMMENT ON COLUMN public.dataimport.shinsaymad IS 'å¯©æ»æè¥¿æ¦';
          public          postgres    false    208            :           0    0    COLUMN dataimport.birthad    COMMENT     E   COMMENT ON COLUMN public.dataimport.birthad IS 'çå¹´ææ¥è¥¿æ¦';
          public          postgres    false    208            ;           0    0    COLUMN dataimport.startdate1ad    COMMENT     M   COMMENT ON COLUMN public.dataimport.startdate1ad IS 'æ½è¡éå§æ¥è¥¿æ¦';
          public          postgres    false    208            <           0    0    COLUMN dataimport.finishdate1ad    COMMENT     N   COMMENT ON COLUMN public.dataimport.finishdate1ad IS 'æ½è¡çµäºæ¥è¥¿æ¦';
          public          postgres    false    208            =           0    0    COLUMN dataimport.hmark_nr    COMMENT     O   COMMENT ON COLUMN public.dataimport.hmark_nr IS 'è¢«ä¿éºèè¨¼è¨å·åè§';
          public          postgres    false    208            >           0    0    COLUMN dataimport.hnum_nr    COMMENT     N   COMMENT ON COLUMN public.dataimport.hnum_nr IS 'è¢«ä¿éºèè¨¼çªå·åè§';
          public          postgres    false    208            ?           0    0    COLUMN dataimport.hmarkhnum_nr    COMMENT     S   COMMENT ON COLUMN public.dataimport.hmarkhnum_nr IS 'è¨å·åè§ï¼çªå·åè§';
          public          postgres    false    208            Ç            1259    648960    ocr_ordered    TABLE     >   CREATE TABLE public.ocr_ordered (
    sid integer NOT NULL
);
    DROP TABLE public.ocr_ordered;
       public            postgres    false            È            1259    648963    refrece    TABLE     @  CREATE TABLE public.refrece (
    rrid integer NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    cym integer DEFAULT 0 NOT NULL,
    ym integer DEFAULT 0 NOT NULL,
    num text DEFAULT ''::text NOT NULL,
    name text DEFAULT ''::text NOT NULL,
    kana text DEFAULT ''::text NOT NULL,
    zip text DEFAULT ''::text NOT NULL,
    add text DEFAULT ''::text NOT NULL,
    destzip text DEFAULT ''::text NOT NULL,
    destadd text DEFAULT ''::text NOT NULL,
    sex integer DEFAULT 0 NOT NULL,
    birth date DEFAULT '0001-01-01'::date NOT NULL,
    drnum text DEFAULT ''::text NOT NULL,
    drname text DEFAULT ''::text NOT NULL,
    clinicnum text DEFAULT ''::text NOT NULL,
    clinicname text DEFAULT ''::text NOT NULL,
    days integer DEFAULT 0 NOT NULL,
    total integer DEFAULT 0 NOT NULL,
    charge integer DEFAULT 0 NOT NULL,
    partial integer DEFAULT 0 NOT NULL,
    apptype integer DEFAULT 0 NOT NULL,
    aid integer DEFAULT 0 NOT NULL,
    insnum text DEFAULT ''::text NOT NULL,
    insname text DEFAULT ''::text NOT NULL,
    distance100 integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.refrece;
       public            postgres    false            @           0    0    COLUMN refrece.distance100    COMMENT     H   COMMENT ON COLUMN public.refrece.distance100 IS 'å¾çè·é¢km x 100';
          public          postgres    false    200            É            1259    648994    scan    TABLE     è   CREATE TABLE public.scan (
    sid integer NOT NULL,
    scandate date,
    cyear integer,
    cmonth integer,
    note1 text,
    note2 text,
    status integer DEFAULT 0,
    apptype integer DEFAULT 0 NOT NULL,
    cym integer
);
    DROP TABLE public.scan;
       public            postgres    false            Ê            1259    649002    scan_sid_seq    SEQUENCE     u   CREATE SEQUENCE public.scan_sid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.scan_sid_seq;
       public          postgres    false    201            A           0    0    scan_sid_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.scan_sid_seq OWNED BY public.scan.sid;
          public          postgres    false    202            Ë            1259    649004 	   scangroup    TABLE       CREATE TABLE public.scangroup (
    groupid integer NOT NULL,
    status integer,
    scanid integer,
    scandate date,
    scanuser integer,
    checkdate date,
    checkuser integer,
    inquirydate date,
    inquiryuser integer,
    workingusers text DEFAULT ''::text NOT NULL
);
    DROP TABLE public.scangroup;
       public            postgres    false            Ì            1259    649011    shokaiexclude    TABLE     )  CREATE TABLE public.shokaiexclude (
    excludeid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    mediym integer DEFAULT 0 NOT NULL,
    chargeym integer DEFAULT 0 NOT NULL,
    hihonum text DEFAULT ''::text NOT NULL,
    birth date DEFAULT '0001-01-01'::date NOT NULL
);
 !   DROP TABLE public.shokaiexclude;
       public            postgres    false            Í            1259    649023    shokaiimage    TABLE     Í   CREATE TABLE public.shokaiimage (
    imageid integer DEFAULT 0 NOT NULL,
    importid integer DEFAULT 0 NOT NULL,
    filename text NOT NULL,
    code text NOT NULL,
    aid integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.shokaiimage;
       public            postgres    false            Î            1259    649032    shokaiimageimport    TABLE     o   CREATE TABLE public.shokaiimageimport (
    importid integer NOT NULL,
    importdate date,
    uid integer
);
 %   DROP TABLE public.shokaiimageimport;
       public            postgres    false            ?           2604    649035    scan sid    DEFAULT     d   ALTER TABLE ONLY public.scan ALTER COLUMN sid SET DEFAULT nextval('public.scan_sid_seq'::regclass);
 7   ALTER TABLE public.scan ALTER COLUMN sid DROP DEFAULT;
       public          postgres    false    202    201            m           2606    649037    appcounter appcounter_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.appcounter
    ADD CONSTRAINT appcounter_pkey PRIMARY KEY (cym);
 D   ALTER TABLE ONLY public.appcounter DROP CONSTRAINT appcounter_pkey;
       public            postgres    false    196            w           2606    649039 %   application_aux application_aux_pkey1 
   CONSTRAINT     d   ALTER TABLE ONLY public.application_aux
    ADD CONSTRAINT application_aux_pkey1 PRIMARY KEY (aid);
 O   ALTER TABLE ONLY public.application_aux DROP CONSTRAINT application_aux_pkey1;
       public            postgres    false    198            s           2606    649041    application application_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.application
    ADD CONSTRAINT application_pkey PRIMARY KEY (aid);
 F   ALTER TABLE ONLY public.application DROP CONSTRAINT application_pkey;
       public            postgres    false    197                       2606    650383    dataimport dataimport_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.dataimport
    ADD CONSTRAINT dataimport_pkey PRIMARY KEY (f000importid, cym);
 D   ALTER TABLE ONLY public.dataimport DROP CONSTRAINT dataimport_pkey;
       public            postgres    false    208    208                       2606    649043    scangroup group_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.scangroup
    ADD CONSTRAINT group_pkey PRIMARY KEY (groupid);
 >   ALTER TABLE ONLY public.scangroup DROP CONSTRAINT group_pkey;
       public            postgres    false    203            y           2606    649045    ocr_ordered ocr_ordered_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.ocr_ordered
    ADD CONSTRAINT ocr_ordered_pkey PRIMARY KEY (sid);
 F   ALTER TABLE ONLY public.ocr_ordered DROP CONSTRAINT ocr_ordered_pkey;
       public            postgres    false    199            |           2606    649047    refrece refrece_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.refrece
    ADD CONSTRAINT refrece_pkey PRIMARY KEY (rrid);
 >   ALTER TABLE ONLY public.refrece DROP CONSTRAINT refrece_pkey;
       public            postgres    false    200            ~           2606    649049    scan scan_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY public.scan
    ADD CONSTRAINT scan_pkey PRIMARY KEY (sid);
 8   ALTER TABLE ONLY public.scan DROP CONSTRAINT scan_pkey;
       public            postgres    false    201                       2606    649051 *   shokaiimageimport shokai_image_import_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.shokaiimageimport
    ADD CONSTRAINT shokai_image_import_pkey PRIMARY KEY (importid);
 T   ALTER TABLE ONLY public.shokaiimageimport DROP CONSTRAINT shokai_image_import_pkey;
       public            postgres    false    206                       2606    649053    shokaiimage shokai_image_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.shokaiimage
    ADD CONSTRAINT shokai_image_pkey PRIMARY KEY (imageid);
 G   ALTER TABLE ONLY public.shokaiimage DROP CONSTRAINT shokai_image_pkey;
       public            postgres    false    205                       2606    649055     shokaiexclude shokaiexclude_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY public.shokaiexclude
    ADD CONSTRAINT shokaiexclude_pkey PRIMARY KEY (excludeid);
 J   ALTER TABLE ONLY public.shokaiexclude DROP CONSTRAINT shokaiexclude_pkey;
       public            postgres    false    204            n           1259    649056    application_cym_idx    INDEX     J   CREATE INDEX application_cym_idx ON public.application USING btree (cym);
 '   DROP INDEX public.application_cym_idx;
       public            postgres    false    197            o           1259    649057    application_groupid_idx    INDEX     R   CREATE INDEX application_groupid_idx ON public.application USING btree (groupid);
 +   DROP INDEX public.application_groupid_idx;
       public            postgres    false    197            p           1259    649058    application_hnum_idx    INDEX     L   CREATE INDEX application_hnum_idx ON public.application USING btree (hnum);
 (   DROP INDEX public.application_hnum_idx;
       public            postgres    false    197            q           1259    649059    application_pbirthday_idx    INDEX     V   CREATE INDEX application_pbirthday_idx ON public.application USING btree (pbirthday);
 -   DROP INDEX public.application_pbirthday_idx;
       public            postgres    false    197            t           1259    649060    application_rrid_idx    INDEX     L   CREATE INDEX application_rrid_idx ON public.application USING btree (rrid);
 (   DROP INDEX public.application_rrid_idx;
       public            postgres    false    197            u           1259    649061    application_shokaicode_idx    INDEX     X   CREATE INDEX application_shokaicode_idx ON public.application USING btree (shokaicode);
 .   DROP INDEX public.application_shokaicode_idx;
       public            postgres    false    197            z           1259    649062    refrece_num_idx    INDEX     B   CREATE INDEX refrece_num_idx ON public.refrece USING btree (num);
 #   DROP INDEX public.refrece_num_idx;
       public            postgres    false    200                       2620    649063 %   application trigger_appcounter_update    TRIGGER        CREATE TRIGGER trigger_appcounter_update BEFORE INSERT OR DELETE OR UPDATE ON public.application FOR EACH ROW EXECUTE PROCEDURE public.appcounter_update();
 >   DROP TRIGGER trigger_appcounter_update ON public.application;
       public          postgres    false    197    210           